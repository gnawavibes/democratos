import shutil

# Copy/paste Ckeditor pluggins
PLUGIN_TO_COPY_FOLDER = "./ckeditors pluggin to add/"
CKEDITOR_FOLDER_PLUGINS = "./venv/lib/python3.10/site-packages/ckeditor/static/ckeditor/ckeditor/plugins/"
shutil.copytree(PLUGIN_TO_COPY_FOLDER, CKEDITOR_FOLDER_PLUGINS, dirs_exist_ok=True)



Print('\n\nINFO: This script assest you already have a Postgresql Database and elasticsearch server runing.\n\n')

"""
from django.core.management.utils import get_random_secret_key
SECRET_KEY=get_random_secret_key()
DB_NAME=input("Database name?")
USER=input("Database owner username?")
PWD=input("Database owner password?")
ElasticHost=("Elasticsearch host address (i.e. 'https://{IP}:{PORT}')?")

# META PROGRAMMING HERE CREATE PYTHON FILE local_settings.py

"""


# migrate
from django.core.management import execute_from_command_line
import django
import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
print(BASE_DIR)
# On charge l'ORM django
sys.path.append(BASE_DIR+"/democratos/")
os.environ['DJANGO_SETTINGS_MODULE'] = 'democratos.settings'
django.setup()

try:
	execute_from_command_line(["democratos/manage.py", "migrate"])
except:
	raise EnvironmentError("Impossible to migrate DB. Do you have a running Postgres instance?")


# update  Elasticsearch
try:
	execute_from_command_line(["democratos/manage.py", "search_index --rebuild"])
except:
	raise EnvironmentError("Impossible to rebuild elasticsearch index. Do you have a running elasticsearch server instance?")
