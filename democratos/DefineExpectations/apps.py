from django.apps import AppConfig


class DefineexpectationsConfig(AppConfig):
    name = 'DefineExpectations'
