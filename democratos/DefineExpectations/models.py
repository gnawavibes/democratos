from django.db import models
from django.contrib.contenttypes.fields import GenericRelation
from CreateYourLaws.models import (
    Explaination, Question, Posopinion, Negopinion,
    SourceLink, Reflection
)
import sys


class Expectation(Reflection):
    questions = GenericRelation(Question)
    explainations = GenericRelation(Explaination)
    posopinions = GenericRelation(Posopinion)
    negopinions = GenericRelation(Negopinion)
    nb_exp = models.IntegerField(default=0)
    nb_q = models.IntegerField(default=0)
    nb_posop = models.IntegerField(default=0)
    nb_negop = models.IntegerField(default=0)


class PublicAdministrativeField(models.Model):
    name = models.CharField(max_length=50)
    expectations = GenericRelation(Expectation)


def Check_PublicAdministrativeField_exist_or_create():
    listPAfield = [
        "Action et des comptes publics",
        "Agriculture et Alimentation",
        "Armée",
        "Cohésion et Relations des territoires",
        "Culture",
        "Ecologie",
        "Économie et Finances",
        "Éducation",
        "Enseignement supérieur, Recherche et Innovation",
        "Industrie",
        "Justice",
        "Politiques étrangères",
        "Sécurité Intérieur",
        "Santé",
        "Sports",
        "Travail",
    ]
    for el in listPAfield:
        PAF, created = PublicAdministrativeField.objects.get_or_create(
            name=el,
        )
        PAF.save()


if any(i in sys.argv[0] for i in ['runserver', "up_the_db"]):
    Check_PublicAdministrativeField_exist_or_create()
