from django.http import (
    HttpResponseRedirect, JsonResponse, HttpResponse, Http404
)
from django.shortcuts import render
from django.template.loader import render_to_string
from render_block import render_block_to_string
from django.views.decorators.http import require_POST, require_GET
from democratos import log
# Create your views here.


@require_GET
def TheExpectations(request):
    print(request.headers.get('x-requested-with') == 'XMLHttpRequest')
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        nav = render_block_to_string(
            "TheExpectations.html",
            "nav",
            locals()
        )
        main = render_block_to_string(
            "TheExpectations.html",
            "Main",
            locals(),
        )
        script2 = render_block_to_string(
            "TheExpectations.html",
            "script2",
            locals()
        )
        ctx = {'nav': nav,
               'main': main,
               'script2': script2}
        return JsonResponse(ctx)
    else:
        return render(request, "TheExpectations.html")
