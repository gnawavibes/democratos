from django.urls import reverse
from django.test import RequestFactory, TestCase
from django.test.client import Client
from django.http import (
    JsonResponse, Http404, HttpResponseNotFound,
    HttpResponseNotAllowed
)
from mixer.backend.django import mixer

import pytest
from unittest import mock

from CommitApp.models import Commit
from CommitApp.views import (
    create_commit, CleanCommit, CleanText, GetHistory,
    GetTimeLine, GetCommitInteract
)
from CreateYourLaws.models import (
    LawCode, CodeBlock, Proposition, LawArticle
)
from UserManager.models import CYL_user


@pytest.mark.django_db
class TestCreateCommitView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestCreateCommitView, cls).setUpClass()
        cls.user = mixer.blend(
            CYL_user,
        )
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode)
        cls.codebloc = mixer.blend(
            CodeBlock,
            rank=1,
            law_code=cls.lawcode,
        )
        cls.law_art = mixer.blend(
            LawArticle,
            block=cls.codebloc,
            text_law="Some law text",
            title="Law title",
        )
        cls.prp = mixer.blend(
            Proposition,
            law_article=cls.law_art,
            text_prp="law proposition text",
            title="law proposition title",
        )

    def setUp(self):
        self.client.force_login(self.user)

    @mock.patch(
        "CommitApp.models.Commit.save",
        mock.MagicMock(name="save"),
    )
    def test_new_law_art_just_space_different(self):
        create_commit(
            self.law_art,
            "Some law text ",
            " Law  title",
            "",
            "",
        )
        self.assertTrue(not Commit.save.called)

    @mock.patch(
        "CommitApp.models.Commit.save",
        mock.MagicMock(name="save"),
    )
    def test_new_proposition_just_space_different(self):
        create_commit(
            self.prp,
            "law proposition text ",
            "law proposition title",
            "",
            "",
        )
        self.assertTrue(not Commit.save.called)

    @mock.patch(
        "CommitApp.models.Commit.save",
        mock.MagicMock(name="save"),
    )
    def test_new_law_art_different(self):
        create_commit(
            self.law_art,
            "Some new stuff in the law text",
            "Law title modified",
            "",
            "Changes with the previous Law",
        )
        self.assertTrue(Commit.save.called)

    @mock.patch(
        "CommitApp.models.Commit.save",
        mock.MagicMock(name="save"),
    )
    def test_new_proposition_different(self):
        create_commit(
            self.prp,
            "New law proposition modified text",
            "New law proposition title",
            "the proposition is different from the law because...",
            "Changes with the previous Proposition",
        )
        self.assertTrue(Commit.save.called)

    @classmethod
    def tearDownClass(cls):
        super(TestCreateCommitView, cls).tearDownClass()


@pytest.mark.django_db
class TestGetHistoryView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGetHistoryView, cls).setUpClass()
        cls.path = reverse('GetHistory')
        cls.user = mixer.blend(
            CYL_user,
        )
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode)
        cls.codebloc = mixer.blend(
            CodeBlock,
            rank=1,
            law_code=cls.lawcode,
        )
        cls.law_art = mixer.blend(
            LawArticle,
            block=cls.codebloc,
        )

    def setUp(self):
        self.client.force_login(self.user)

    def test_GetHistory_GET_request(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_GetHistory_request_is_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetHistory(request)

    @classmethod
    def tearDownClass(cls):
        super(TestGetHistoryView, cls).tearDownClass()


@pytest.mark.django_db
class TestGetTimeLineView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGetTimeLineView, cls).setUpClass()
        cls.path = reverse('GetTimeLine')
        cls.user = mixer.blend(
            CYL_user,
        )
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode)
        cls.codebloc = mixer.blend(
            CodeBlock,
            rank=1,
            law_code=cls.lawcode,
        )
        cls.lawart = mixer.blend(
            LawArticle,
            block=cls.codebloc,
        )
        cls.commits = []
        for i in range(1, 3):
            setattr(cls, 'cmt'+str(i), mixer.blend(
                Commit,
                content_object=cls.lawart,
            )
            )

    def setUp(self):
        self.client.force_login(self.user)

    def test_GetTimeLine_POST_request(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_GetTimeLine_request_is_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetHistory(request)

    def test_GetTimeLine_request_is_ajax_working(self):
        response = self.client.get(
            self.path,
            {
                'idref': str(self.lawart.id),
                'typeref': 'law',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertTemplateUsed("TimeLine.html")
        self.assertContains(response, "ref")
        self.assertContains(response, "commits")
        self.assertContains(response, "template")

    def test_GetTimeLine_unknown_ref_type_case(self):
        response = self.client.get(
            self.path,
            {
                'typeref': 'grd',
                'idref': '1',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_GetTimeLine_unknown_ref_case(self):
        response = self.client.get(
            self.path,
            {
                'typeref': 'law',
                'idref': '9999585951',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @classmethod
    def tearDownClass(cls):
        super(TestGetTimeLineView, cls).tearDownClass()


@pytest.mark.django_db
class TestGetCommitInteractView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGetCommitInteractView, cls).setUpClass()
        cls.path = reverse('GetCommitInteract')
        cls.user = mixer.blend(
            CYL_user,
        )
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode)
        cls.codebloc = mixer.blend(
            CodeBlock,
            rank=1,
            law_code=cls.lawcode,
        )
        cls.lawart = mixer.blend(
            LawArticle,
            block=cls.codebloc,
        )
        cls.commits = []
        for i in range(1, 3):
            setattr(cls, 'cmt'+str(i), mixer.blend(
                Commit,
                content_object=cls.lawart,
            )
            )

    def setUp(self):
        self.client.force_login(self.user)

    def test_GetCommitInteract_POST_request(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_GetCommitInteract_request_is_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetHistory(request)

    def test_GetCommitInteract_request_is_ajax_working(self):
        response = self.client.get(
            self.path,
            {
                'cmt_id': str(self.cmt2.id),
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertTemplateUsed("Interact.html")
        self.assertContains(response, "div")

    def test_GetCommitInteract_unknown_ref_case(self):
        response = self.client.get(
            self.path,
            {
                'cmt_id': '9999585951',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @classmethod
    def tearDownClass(cls):
        super(TestGetCommitInteractView, cls).tearDownClass()
