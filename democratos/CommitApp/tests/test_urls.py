from django.urls import reverse, resolve


class TestUrls:

    def test_gethistory_url(self):
        path = reverse('GetHistory')
        assert resolve(path).view_name == "GetHistory"

    def test_gettimeline_url(self):
        path = reverse('GetTimeLine')
        assert resolve(path).view_name == "GetTimeLine"

    def test_getcommitinteract_url(self):
        path = reverse('GetCommitInteract')
        assert resolve(path).view_name == "GetCommitInteract"
