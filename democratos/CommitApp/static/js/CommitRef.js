// note: htmldiff from jsdiff.js

var timeline;
var commits;
var ref;
var currentVersion;
var nb_versions;

function GoCommitDetails(){
    console.log("must be written");
}

function SetVisibleCommit(){
    $(".CommitStyle").css("display","none");
    $(".ActiveCommit").css("display","block");
}

function OnceLoadCommitDetails(){
    console.log("OnceLoadCommitDetails start.");
    $(".cmtInteract").each(function(){
        var id = $(this).attr("id");
        $.ajax({
            type: "GET",
            url: '/Commit/GetCommitInteract',
            data: {'cmt_id':id.substring(14) ,csrfmiddlewaretoken: csrftoken},
            dataType: "json",
            success: function(rs) {
                $("#"+id).html(rs.div);
            },
            error: function(rs, e) {
               alert(rs.responseText);
            }
        });
    });
    $(".cmtInteract").first().css("display","inline-block");
    console.log("OnceLoadCommitDetails end.");
}

function DisplayCommit(version){
    if (typeof version !== 'undefined' && version.length > 0) {
        console.log("Call for version " + version);
        $(".CommitStyle").removeClass("ActiveCommit");
        $("#FieldVersion"+version).addClass("ActiveCommit");
        timeline.setSelection(version);
        SetVisibleCommit();
    }
}

function GetTitleTextDetails(version){
    var title, text, details;
    if (version == nb_versions){
        title = ref.fields.title;
        if (ref.fields.hasOwnProperty("text_prp")){
            text = ref.fields.text_prp;
            details = ref.fields.details_prp;
        }
        else if (ref.fields.hasOwnProperty("text_law")){
            text = ref.fields.text_law;
            details = ref.fields.details_law;
        }
    }
    else{
        title = commits[version-1].fields.commit_title
        text = commits[version-1].fields.commit_txt
        details = commits[version-1].fields.commit_details
    }
    if (title==null){
        title = "";
    }
    if (text==null){
        text = "";
    }
    if (details==null){
        details = "";
    }
    return [title,text,details]
}

function CompareVersions(idversion, idcompare){
    console.log("we gonna compare version " + idversion + " with version " + idcompare);
    var Difftext,Difftitle,Diffdetails,title_old,text_old,details_old,title_new,text_new,details_new;
    if(idcompare==0){
        [Difftitle,Difftext,Diffdetails] = GetTitleTextDetails(idversion);
    }
    else{
        if(idversion<idcompare){
            [title_old,text_old,details_old] = GetTitleTextDetails(idversion);
            [title_new,text_new,details_new] = GetTitleTextDetails(idcompare);
        }
        else{
            [title_old,text_old,details_old] = GetTitleTextDetails(idcompare);
            [title_new,text_new,details_new] = GetTitleTextDetails(idversion);
        }
        Difftext = htmldiff(text_old, text_new);
        Difftitle = htmldiff(title_old, title_new);
        Diffdetails = htmldiff(details_old, details_new);
    }
    $("#titleV"+idversion).html(Difftitle);
    $("#txtV"+idversion).html(Difftext);
    $("#detailsV"+idversion).html(Diffdetails);
}

function AskHistory(elmt){
    var data =  $(elmt).attr('name').split(":");
    var items;
    console.log(data);
    $.ajax({
        type: "GET",
        url: '/Commit/GetTimeLine',
        data: {'typeref': data[1] ,'idref': data[2] ,csrfmiddlewaretoken: csrftoken},
        dataType: "json",
        success: function(rs) {
            // Load template for Timeline
            $("#AddTimeLine").html(rs.template);
            $("#AddTimeLine").removeClass("GetTimeLine clickable");
            // DOM element where the Timeline will be attached
            ref = JSON.parse(rs.ref)
            ref = ref[0];
            commits = JSON.parse(rs.commits)
            var container = document.getElementById('Timeline');
            // Configuration for the Timeline
            var minDate = new Date(ref.fields.posted);
            var maxDate = new Date().setHours(23,59,59,999);
            var options = {
                start:minDate,
                end:maxDate,
                min:minDate, 
                max:maxDate,
                showCurrentTime: false,
                margin: {
                    item : {
                        horizontal : 0
                    }
                },
                "zoomMin":864000000 // on zoom au jour près max
            };
            var dataset = [];
            items = new vis.DataSet();
            var start;
            for (var i=0; i < commits.length; i++){
                console.log("" + i +  " " + commits[i].fields.posted);
                var end = new Date(commits[i].fields.posted);
                if (i==0){
                    start = new Date(ref.fields.posted);
                }
                else{
                    start = new Date(commits[i-1].fields.posted);
                }
                dataset.push({
                    id:(i+1),
                    content:"version "+(i+1),
                    start:start,
                    end:end,
                    subgroup:1,
                });
            }
            start = end;
            end = new Date().setHours(23,59,59,999);
            if (ref.fields.abrogated){
                end = new Date(ref.fields.abrogated)
            }
            dataset.push({
                id:(i+1),
                content:"version "+(i+1),
                start:start,
                end:end,
            })
            nb_versions = i+1;
            currentVersion = i+1;
            items = new vis.DataSet(dataset);
            // Create a Timeline
            timeline = new vis.Timeline(container, items, options);
            timeline.setSelection(i+1);
            timeline.on('select', function (properties) {
                DisplayCommit(properties.items);
            });
            $('.CompareListCommitSelect').change(function(){
                console.log($(this).attr("id").substring(20, $(this).attr("id").length))
                var theVersion = Number($(this).attr("id").substring(20, $(this).attr("id").length));
                var comparedVersion = Number($(this).find("option:selected").attr("value"));
                CompareVersions(theVersion, comparedVersion);
            });
            console.log("TimeLineLoaded");
        },
        error: function(rs, e) {
            alert(rs.responseText);
        },
    });
}

$(document).ready(function() {
    //-------------  Get Ref History (commits) -------------------
    AskHistory("#AddTimeLine");
    $("#AddTimeLine").on('click', '.GoCommitVersion', function() {
        var data =  $(this).attr('name').split(":");
        currentVersion = data[1]; 
        DisplayCommit(currentVersion);
        $('#CompareSelectVersion'+currentVersion).val(0);
        CompareVersions(currentVersion,0);
    });
    console.log('CommitRef.js loaded');
});

