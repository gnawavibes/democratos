from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import (
    GenericForeignKey)
from ckeditor.fields import RichTextField


class Commit(models.Model):
    commit_title = models.TextField()
    commit_txt = models.TextField()
    commit_details = models.TextField(null=True)
    posted = models.DateTimeField(auto_now_add=True)
    content_type = models.ForeignKey(ContentType,
                                     null=True,
                                     on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    comments = RichTextField(null=True)
    url = models.URLField(null=True)
    legifranceID = models.CharField(
        max_length=20,
        blank=True,
        null=True
    )
