from django.urls import path
from . import views


urlpatterns = [
    path('GetHistory',
         views.GetHistory,
         name='GetHistory'),
    path('GetTimeLine',
         views.GetTimeLine,
         name='GetTimeLine'),
    path('GetCommitInteract',
         views.GetCommitInteract,
         name='GetCommitInteract',
         ),
]
