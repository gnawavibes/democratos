from django.http import JsonResponse, Http404
from CommitApp.models import Commit
from difflib import SequenceMatcher
from django.core import serializers
from django.template.loader import render_to_string
from CreateYourLaws.models import (
    LawArticle, Proposition,)
from CreateYourLaws.views_functions import (
    get_the_instance,
)
from django.views.decorators.http import require_POST, require_GET
import operator


def create_commit(ref, newtxt, newtitle, details, comments):
    """ 
    OBSOLETE! ùaybe reused and improver for law prop and prop
    Create A Commit model from A Ref Modif
    """
    if type(ref) is LawArticle:
        ok, newtext = CleanCommit(ref.text_law, newtxt)
        ok2, newttle = CleanCommit(ref.title, newtitle)
        if (ok or ok2):
            commit = Commit.objects.create(
                commit_txt=CleanText(ref.text_law),
                commit_title=CleanText(ref.title),
                commit_details=details,
                comments=comments,
                content_object=ref,
            )
            commit.save()
    elif type(ref) is Proposition:
        ok, newtext = CleanCommit(ref.text_prp, newtxt)
        ok2, newttle = CleanCommit(ref.title, newtitle)
        if (ok or ok2):
            commit = Commit.objects.create(
                commit_txt=CleanText(ref.text_prp),
                commit_title=CleanText(ref.title),
                commit_details=CleanText(details),
                comments=comments,
                content_object=ref,
            )
            commit.save()
    print("commit!")


def CleanCommit(oldtxt, newtxt):
    """Test if it worth a  new commit. 
    Avoid changes like space insertion"""
    flag = False
    SeqMatch = SequenceMatcher()
    SeqMatch.set_seqs(oldtxt, newtxt)
    stackerror = []
    for el in SeqMatch.get_opcodes():
        if el[0] != "equal":
            old = (oldtxt[el[1]:el[2]])
            new = (newtxt[el[3]:el[4]])
            if (old or new) not in [" ", ""]:
                flag = True
            else:
                stackerror.append((el[3], el[4]))
    stackerror.reverse()
    for elmt in stackerror:
        newtxt = newtxt[:elmt[0]]+newtxt[elmt[1]:]
    return flag, newtxt


def CleanText(text):
    """ remove useless space and line break"""
    text = text.replace("\n", "")
    while "  " in text:
        text = text.replace("  ", " ")
    return text


@require_POST
def GetHistory(request):
    """ 
    OBSOLETE! ùaybe reused...
    """
    if not request.headers.get('x-requested-with') == 'XMLHttpRequest':
        raise Http404
    typeref = request.POST.get('typeref', None)
    idref = int(request.POST.get('idref', None))
    try:
        ref = get_the_instance(typeref, idref)
    except Exception:
        raise Http404
    template = render_to_string("commit.html", locals())
    Commits = ref.commit.all().order_by("posted")
    ctx = {
        "ref": serializers.serialize("json", [ref]),
        "history": serializers.serialize("json", Commits),
        "template": template,
    }
    return JsonResponse(ctx)


@require_GET
def GetTimeLine(request):
    if not request.headers.get('x-requested-with') == 'XMLHttpRequest':
        raise Http404
    typeref = request.GET.get('typeref', None)
    idref = int(request.GET.get('idref', None))
    try:
        ref = get_the_instance(typeref, idref)
    except Exception:
        raise Http404
    Commits = ref.commit.all().order_by("posted")
    template = render_to_string("TimeLine.html", locals())
    ctx = {
        "ref": serializers.serialize("json", [ref]),
        "commits": serializers.serialize("json", Commits),
        "template": template,
    }
    return JsonResponse(ctx)


@require_GET
def GetCommitInteract(request):
    if not request.headers.get('x-requested-with') == 'XMLHttpRequest':
        raise Http404
    ref_id = request.GET.get('cmt_id', None)
    user = request.user
    typeref = "cmt"
    try:
        ref = Commit.objects.get(id=ref_id)
    except Exception:
        raise Http404
    listcom = list(ref.explaination_set.filter(is_censured=False))
    listcom.extend(list(ref.question_set.filter(is_censured=False)))
    listcom = sorted(
        listcom,
        key=operator.attrgetter('approval_factor')
    )
    listsrc = list(ref.sourcelink_set.filter(is_censured=False))
    CommitInteract = render_to_string(
        'Interact.html',
        locals()
    )
    ctx = {
        'div': CommitInteract,
    }
    return JsonResponse(ctx)
