from django.apps import AppConfig


class CommitappConfig(AppConfig):
    name = 'CommitApp'
