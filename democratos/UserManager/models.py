# -*-coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from datetime import date


class CYL_user(AbstractUser):
    """ Describe a User of CYL"""
    date_of_birth = models.DateField(default=date.today)
    end_of_sanction = models.DateTimeField(null=True, default=None)
    has_job_to_do = models.BooleanField(default=False)
    contacts = models.ManyToManyField('self')
    # elector_number = models.IntegerField()
    # digital_print = models.ImageField()

try:
    import sys
    if not 'pytest' in sys.argv[0]:
        Gov, created = CYL_user.objects.get_or_create(id=1, username="government")
        Gov.save()
except:
    print("DB not created, Please do migrations first ('python manage.py makemigrations')")

class Note(models.Model):
    user = models.ForeignKey(CYL_user, on_delete=models.CASCADE)
    approve = models.BooleanField(null=True)
    content_type = models.ForeignKey(ContentType,
                                     null=True,
                                     on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


"""
  OBSOLETE?
class UserSession(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    session = models.ForeignKey(Session)"""
