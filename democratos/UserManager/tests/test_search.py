from django.urls import reverse
from django.test import RequestFactory, TestCase
from django.test.client import Client
from django.http import (
    JsonResponse, Http404,
    HttpResponseNotAllowed
)
from mixer.backend.django import mixer

from elasticsearch import Elasticsearch
from UserManager.documents import *
from UserManager.search import *
from elasticsearch_dsl import Index
import pytest
import string
import random
from elasticsearch_dsl import connections


schemas={}
documents = [
    CYL_userDocument,
]

for doc in documents:
    i = Index(doc._index._name)
    i.document(doc)
    i.analyzer(edge_ngram_completion)
    schemas[doc._index._name] = i.to_dict()["mappings"]


ELASTICSEARCH_TEST_HOST = [
    "127.0.0.1:9201",
    "elasticsearch:9200"
]

class Esearch_class(Elasticsearch):
    def __init__(self, HOST, schemas=schemas, edge_ngram_completion=edge_ngram_completion):
        super().__init__(HOST)
        self.schemas = schemas
        for index_name, schema in self.schemas.items():
            if self.indices.exists(index=index_name):
                self.indices.delete(index=index_name)
            body = {
                "settings": {
                    "number_of_shards": 1,
                    "number_of_replicas": 1,
                    "index.store.type": "mmapfs",
                    "analysis":{
                        "analyzer":{
                            "edge_ngram_completion":edge_ngram_completion.get_definition(),
                        },
                        "filter":{
                            "edge_ngram_completion_filter":edge_ngram_completion_filter.get_definition(),
                        },
                    }
                },
                "mappings": schema,
            }
            self.indices.create(index=index_name, body=body)

    def clearES(self):
        for index_name in self.schemas.keys():
            self.indices.flush()

    def __del__(self):
        for index_name in self.schemas.keys():
            self.indices.delete(index=index_name)


@pytest.mark.django_db
class Testsearch_userView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(Testsearch_userView, cls).setUpClass()
        cls.esearch = Esearch_class(ELASTICSEARCH_TEST_HOST)
        cls.path = reverse('search_user')
        cls.user = mixer.blend(CYL_user, username="main_user")
        cls.client = Client()
        rand_str = lambda n: ''.join(
            [random.choice(string.ascii_letters) for i in range(n)]
        )
        for i in range(1,100):
            if i%2==0:
                if i%4==0:
                    setattr(cls, "p"+str(i), mixer.blend(
                        CYL_user,
                        username=rand_str(3)+"test"+str(i/2)+rand_str(3))
                    )
                else:
                    setattr(cls, "p"+str(i), mixer.blend(
                        CYL_user, username="test"+str(i/2)+rand_str(3))
                    )
            else:
                setattr(cls, "p"+str(i), mixer.blend(
                    CYL_user)) # , username="p"+str(i)))
            getattr(cls, "p"+str(i)).save()
            

    @classmethod
    def tearDownClass(cls):
        del(cls.esearch)
        super(Testsearch_userView, cls).tearDownClass()

    def setUp(self):
        self.client.force_login(self.user)

    def test_search_user_HTTP_request(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = searchCYL_user(request)

    def test_search_user_unauthenticated_user(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')
    
    def test_search_user_AJAX_GET_request(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

"""
    def test_search_user_all_search_type_works(self):
        client = self.client
        response = client.post(
            self.path,
            {
                'input':'test',
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "results")
        content = eval(response.content)
        hits_len = len(eval(content["results"]))
        print(content["results"], "    ", hits_len)
        self.assertTrue(hits_len==3)

    def test_search_user_conversation_search_type_works(self):
        client = self.client
        response = client.post(
            self.path,
            {
                'input':'convers_test',
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "results")
        content = eval(response.content)
        hits_len = len(eval(content["results"]))
        print(content["results"], "    ", hits_len)
        self.assertTrue(hits_len==3)

    def test_search_user_message_search_type_works(self):
        client = self.client
        response = client.post(
            self.path,
            {
                'input':'from p2',
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "results")
        content = eval(response.content)
        hits_len = len(eval(content["results"]))
        self.assertTrue(hits_len==1)

    def test_search_user_participant_search_type_works(self):
        client = self.client
        response = client.post(
            self.path,
            {
                'input':self.p1.username,
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "results")
        content = eval(response.content)
        hits_len = len(eval(content["results"]))
        print(content["results"], "    ", hits_len)
        self.assertTrue(hits_len==2)
"""
