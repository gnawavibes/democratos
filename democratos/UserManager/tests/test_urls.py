from django.urls import reverse, resolve


class TestUrls:

    def test_checkbox_url(self):
        path = reverse('checkbox')
        assert resolve(path).view_name == "checkbox"

    def test_profile_url(self):
        path = reverse('profile')
        assert resolve(path).view_name == "profile"

    def test_InfoChangeDone_url(self):
        path = reverse('InfoChangeDone')
        assert resolve(path).view_name == "InfoChangeDone"
