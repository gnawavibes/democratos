from django.urls import reverse, resolve
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase, Client
from django.http import JsonResponse, HttpResponse
from django.http.response import Http404
from django.template.response import TemplateResponse
from mixer.backend.django import mixer
import pytest
from democratos import settings, log
from datetime import date
from unittest import mock

from CreateYourLaws.models import LawArticle, Posopinion
from UserManager.models import CYL_user, Note
from UserManager.views import *
from datetime import timedelta, datetime
from captcha.models import CaptchaStore
import sys


@pytest.mark.django_db
class Testinfo_change_doneView(TestCase):
    def setUp(self):
        self.path = reverse('InfoChangeDone')
        self.user = mixer.blend(CYL_user)

    def test_info_change_done_user_unauthenticated(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = AnonymousUser()
        response = info_change_done(request)
        self.assertIn('UserManager/accounts/login/', response.url)

    def test_info_change_done_Valid_no_extra_context(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        response = info_change_done(request)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('registration/info_change_done.html')
        self.assertTrue(isinstance(response, TemplateResponse))
        self.assertContains(response, "title")

    def test_info_change_done_Valid_with_extra_context(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        response = info_change_done(request, extra_context={"truc": "machin"})
        # content = eval(response.context)
        log.debug(response.context_data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('registration/info_change_done.html')
        self.assertTrue(isinstance(response, TemplateResponse))
        self.assertContains(response, "title")
        self.assertEqual(response.context_data["truc"], "machin")


class Testview_profileView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(Testview_profileView, cls).setUpClass()
        cls.path = reverse('profile')
        cls.user = CYL_user.objects.create_user(
            username='testclient',
            password='password',
            email='testclient@example.com'
        )
        cls.client = Client()

    def setUp(self):
        self.client.force_login(self.user)

    def _login(self):
        response = self.client.post('/UserManager/accounts/login/', {
            'username': self.user.username,
            'password': self.user.password,
        })

    def test_view_profile_user_unauthenticated(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = AnonymousUser()
        response = view_profile(request)
        self.assertIn('UserManager/accounts/login/', response.url)

    def test_view_profile_user_authenticate_see_profile_POST(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        response = view_profile(request)
        print(response)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('account_set.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "script2")

    def test_view_profile_user_authenticated_see_profile_GET(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = view_profile(request)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('account_set.html')
        self.assertTrue(isinstance(response, HttpResponse))

    def test_view_profile_user_authenticate_info_change_valid(self):
        response = self.client.post(
            self.path,
            {
                'NewUserName': 'Tester',
                'NewUserFamillyName': "Testor",
                'NewUserFirstName': "El",
                'NewEmail': 'toto@tatane.com',
                'infochangebut': "",
            },
        )
        print(response)
        self.assertRedirects(
            response,
            "/UserManager/info/change/done",
        )

    def test_view_profile_user_authenticate_info_change_not_valid(self):
        request = RequestFactory().post(
            self.path,
            {
                'NewUserName': 'Tester',
                'NewUserFamillyName': "Testor",
                'NewUserFirstName': "El",
                'NewEmail': 'tototatanecom',
                'infochangebut': "",
            },
        )
        request.user = self.user
        response = view_profile(request)
        print(response, type(response))
        self.assertEqual(
            response,
            None,
        )

    def test_view_profile_user_authenticate_change_pwd_valid(self):
        self._login()
        response = self.client.post(
            self.path,
            {
                'old_password': "password",
                'new_password1': "Testor123",
                'new_password2': "Testor123",
                'pwdchangebut': "",
            },
        )
        print(response.status_code)
        self.assertRedirects(
            response,
            "/UserManager/accounts/password/change/done",
            target_status_code=301,
        )

    def test_view_profile_user_authenticate_change_pwd_not_valid(self):
        self._login()
        request = RequestFactory().post(
            self.path,
            {
                'old_password': "foobar",
                'new_password1': "Testor",
                'new_password2': "Testor",
                'pwdchangebut': "",
            },
        )
        request.user = self.user
        response = view_profile(request)
        print(response, type(response))
        self.assertEqual(
            response,
            None,
        )

    def test_view_profile_user_authenticate_del_account_valid(self):
        self._login()
        response = self.client.post(
            self.path,
            {
                'password': "password",
                'delaccountbut': "",
            },
        )
        print(response.status_code)
        self.assertRedirects(
            response,
            "/UserManager/accounts/login",
            target_status_code=301,
        )

    def test_view_profile_user_authenticate_del_account_not_valid(self):
        self._login()
        request = RequestFactory().post(
            self.path,
            {
                'password': "foobar",
                'delaccountbut': "",
            },
        )
        request.user = self.user
        response = view_profile(request)
        print(response, type(response))
        self.assertEqual(
            response,
            None,
        )

    @classmethod
    def tearDownClass(cls):
        super(Testview_profileView, cls).tearDownClass()
