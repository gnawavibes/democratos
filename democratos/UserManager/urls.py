# -*-coding: utf-8 -*-

from django.urls import path, include
from . import views
from . import search
from UserManager.forms import Create_CYL_UserForm


urlpatterns = [
    path('checkbox', views.Checkbox, name="checkbox"),
    path('profile', views.view_profile, name="profile"),
    path('profile/<str:set_var>', views.view_profile, name="profile"),
    path('info/change/done', views.info_change_done, name="InfoChangeDone"),
    path('accounts/', include('registration.backends.default.urls')),
    path('search_user', search.searchCYL_user, name="search_user"),
    path('ValidUser', search.ValidCYL_user, name="ValidUser"),
]
