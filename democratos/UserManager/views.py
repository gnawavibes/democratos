from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render, redirect
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.utils.translation import gettext as _
from django.template.response import TemplateResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from UserManager.forms import (
    Create_CYL_UserForm, Del_account_form, Info_Change_Form
)
from django.template.loader import render_to_string
from django.http import (
    JsonResponse, HttpResponse, Http404
)
from UserManager.models import CYL_user
from render_block import render_block_to_string
from democratos import log


@login_required
def info_change_done(request,
                     template_name='registration/info_change_done.html',
                     extra_context=None):
    """ triggered when a user changed his own infos successfuly """
    context = {
        'title': _('Info change successful'),
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context)


@login_required
def view_profile(request, set_var="info"):
    """ """
    log.debug((
        "type of request:",
        request.method,
        "    request:",
        request,
        request.POST,
        "\nWhats in?\n",
        dir(request)))
    if request.method == 'POST' and 'infochangebut' in request.POST:
        infochangeform = Info_Change_Form(user=request.user,
                                          data=request.POST)
        set_var = "info"
        if infochangeform.is_valid():
            infochangeform.save()
            return redirect('/UserManager/info/change/done')
        else:
            return None
    else:
        infochangeform = Info_Change_Form(user=request.user)
    if request.method == 'POST' and 'pwdchangebut' in request.POST:
        pwdchangeform = PasswordChangeForm(user=request.user,
                                           data=request.POST)
        set_var = "pwd"
        if not pwdchangeform.is_valid():
            return None
        pwdchangeform.save()
        update_session_auth_hash(request, pwdchangeform.user)
        return redirect('/UserManager/accounts/password/change/done')
    else:
        pwdchangeform = PasswordChangeForm(user=request.user)
    if request.method == 'POST' and 'delaccountbut' in request.POST:
        del_form = Del_account_form(request.POST)
        set_var = "del"
        if not del_form.is_valid(request.user):
            return None
        request.user.delete()
        return redirect('/UserManager/accounts/login')
    else:
        del_form = Del_account_form()
    if request.method == "POST":
        user = request.user
        intro = render_block_to_string('account_set.html',
                                       "nav",
                                       locals(),
                                       request=request,
                                       )
        content = render_block_to_string('account_set.html',
                                         "Main",
                                         locals(),
                                         request=request,)
        ctx = {'nav': intro,
               'main': content,
               'script2': "",
               }
        return JsonResponse(ctx)
    else:
        try:
            user = request.user
        except:
            print("user not connected.")
        return render(request, 'account_set.html', locals())

# To be studied to create a subscription system


def Checkbox(request):
    if request.method == 'POST':
        typeref = request.POST.get('typeref', None)
        ref_id = request.POST.get('ref_id', None)
        checked = request.POST.get('check', None)
    ctx = {}
    return JsonResponse(ctx)
