from django_elasticsearch_dsl import Document, fields
from elasticsearch_dsl import analyzer
from django_elasticsearch_dsl.registries import registry
from .models import CYL_user

from elasticsearch_dsl import analyzer
from elasticsearch_dsl.analysis import token_filter

edge_ngram_completion_filter = token_filter(
    'edge_ngram_completion_filter',
    type="edge_ngram",
    min_gram=1,
    max_gram=20
)


edge_ngram_completion = analyzer(
    "edge_ngram_completion",
    tokenizer="standard",
    filter=["lowercase", edge_ngram_completion_filter]
)


# @registry.register_document
class CYL_userDocument(Document):
    username = fields.TextField(
        fields={
            'raw': fields.TextField(analyzer='standard'),
            'suggest': fields.CompletionField(),
            'edge_ngram_completion': fields.TextField(
                analyzer=edge_ngram_completion
            ),
        }
    )

    class Index:
        # Name of the Elasticsearch index
        name = 'cyl_user'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = CYL_user  # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
            # 'username',
        ]

        # Ignore auto updating of Elasticsearch when a model is saved
        # or deleted:
        # ignore_signals = True

        # Don't perform an index refresh after every update (overrides global setting):
        # auto_refresh = False

        # Paginate the django queryset used to populate the index with the specified size
        # (by default it uses the database driver's default setting)
        # queryset_pagination = 5000
