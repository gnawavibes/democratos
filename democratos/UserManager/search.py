from elasticsearch_dsl.query import Q, MultiMatch, SF
from UserManager.documents import CYL_userDocument
from elasticsearch_dsl.query import MultiMatch
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from django.http import JsonResponse
from django.views.decorators.http import require_POST, require_GET
from democratos import log
from democratos.decorators import ajax_login_required
import sys
if not 'pytest' in sys.argv[0]:
    from democratos.local_settings import ElasticHost
else:
    ElasticHost = ["localhost:9200", "elasticsearch:9200"]

client = Elasticsearch(ElasticHost)

@ajax_login_required
@require_POST
def ValidCYL_user(request):
    username = request.POST.get('val', None)
    s_user = Search(using=client)
    query = s_user.index("cyl_user").query(
        'match',
        username=username,
    )
    response = query.execute()
    if response.hits:
        ctx = {"valid": "true"}
    else:
        ctx = {"valid": "false"}
    return JsonResponse(ctx)

@ajax_login_required
@require_POST
def searchCYL_user(request):
    name = request.POST.get('input', None)
    s_user = Search(using=client)
    user = request.user
    query = s_user.index(
        "cyl_user"
    ).query(
        'regexp',
        username=name+'.*',
    ).exclude(
        "match",
        id=user.id
    )
    print(user.id)
    response = query.execute()
    if len(response) < 10:
        query = s_user.index("cyl_user").query(
            'regexp',
            username='.*'+name+'.*',
        ).exclude(
            "match",
            id=user.id
        )
        response2 = query.execute()
        toremove = []
        for hit in response2.hits:
            if hit in response.hits:
                toremove.append(hit)
        for hit in toremove:
            response2.hits.remove(hit)
        response.hits.extend(response2.hits[:10-len(response)])
    else:
        response = response[:10]
    if response:
        res = []
        for hit in response.hits:
            print(hit)
            res.append({"username": hit.username, 'id': hit.id})
        ctx = {"results": str(res)}
        print(name, "\n\n", ctx)
    else:
        query = s_user.suggest(
            'suggestion',
            name,
            completion={'field': 'username.suggest'}
        )
        response = query.execute()
        results = str(response.suggest['suggestion'][0]['options'])
        ctx = {
            "results": results.replace("text", "username")
        }
    log.debug(ctx)
    return JsonResponse(ctx)


"""
@require_POST
def searchCYL_user(request):
    return get_searchchCYL_user_query(request).to_queryset()

def get_search_query(phrase):
    query = Q(
        'function_score',
        query=MultiMatch(
            fields=['username'],
            query=phrase
        ),
    )
    return CYL_userDocument.search().query(query)


def search(phrase):
    return get_search_query(phrase).to_queryset()
"""
