# -*-coding: utf-8 -*-

from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.home, name='home'),
    path('UP', views.UP, name="UP"),
    path('DOWN', views.DOWN, name="DOWN"),
    path('InDatBox', views.In_dat_box, name='InDatBox'),
    path('InDatBox/<int:box_type>/<int:box_id>',
         views.In_dat_box,
         name='InDatBox'),
    path('reflection',
         views.get_reflection,
         name='reflection'),
    path('Reflection/<str:typeref>/<int:id_ref>',
         views.get_reflection,
         name='Reflection'),
    path('childcomments',
         views.childcomments,
         name='childcomments'),
    path('CreateNewLaw',
         views.CreateNewLaw,
         name='CreateNewLaw'),
    path('ValidNewLaw',
         views.ValidNewLaw,
         name='ValidNewLaw'),
    path('DeleteReflection',
         views.DeleteReflection,
         name='DeleteReflection'),
    path('ModifReflection',
         views.ModifReflection,
         name='ModifReflection'),
    path('GetForm',
         views.GetForm,
         name='GetForm'),
    path('getnewlawprops',
         views.getnewlawprops,
         name='getnewlawprops'),
    path('postreflection', views.PostReflection, name='PostReflection'),
    path('listref/<str:parent_type>/<int:parent_id>/<str:list_ref_type>',
         views.list_of_reflections,
         name='listref'),
    path('ListRef',
         views.list_of_reflections,
         name='ListRef'),
    path('captcha/', include('captcha.urls')),
]
"""+ static(
    settings.STATIC_URL,
    document_root=settings.STATIC_ROOT
) + static(
    settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT
)
"""
