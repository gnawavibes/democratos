# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django import forms
from CreateYourLaws.models import (
    Explaination, Proposition, LawArticle, Posopinion,
    Negopinion, Question, CYL_user, SourceLink
)
from ckeditor.widgets import CKEditorWidget


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('title', 'text_qst')
        labels = {'title': ('En quelques mots... :'),
                  'text_qst': ("Développez votre question")}
        widgets = {
            'title': forms.TextInput(attrs={'size': 80}),
            'text_qst': CKEditorWidget()
        }


class ExplainationForm(forms.ModelForm):
    class Meta:
        model = Explaination
        fields = ('title', 'text_exp')
        labels = {'title': ("Titre (Décrivez votre idée en quelques mots):"),
                  'text_exp': ('Développez votre commentaire')}
        widgets = {
            'title': forms.TextInput(attrs={'size': 80}),
            'text_exp': CKEditorWidget()
        }

    def clean(self):
        cleaned_data = super(ExplainationForm, self).clean()
        title = cleaned_data.get("title")
        text_exp = cleaned_data.get("text_exp")
        if title == "" and len(text_exp) > 300:
            self.add_error('title',
                           'Titre nécessaire car votre commentaire est long')
        return cleaned_data


class PosopinionForm(forms.ModelForm):
    class Meta:
        model = Posopinion
        fields = ('title', 'text_opp')
        labels = {'title': ('En quelques mots...'),
                  'text_opp': ("Votre opinion:"),
                  }
        widgets = {
            'text_opp': CKEditorWidget()
        }


class NegopinionForm(forms.ModelForm):
    class Meta:
        model = Negopinion
        fields = ('title', 'text_opn')
        labels = {'title': ('En quelques mots...'),
                  'text_opn': ("Votre opinion:"),
                  }
        widgets = {
            'text_opn': CKEditorWidget()
        }


class PropositionForm(forms.ModelForm):
    class Meta:
        model = Proposition
        fields = ('title', 'text_prp', 'details_prp')
        labels = {'title': ('Nommez votre proposition de loi'),
                  'text_prp': ("votre proposition de loi"),
                  'details_prp': ("But recherché par " +
                                  "cette contre-proposition:"),
                  }
        widgets = {
            'text_prp': CKEditorWidget(config_name='redac_law'),
            'details_prp': CKEditorWidget(config_name='redac_law'),
        }


class SourceLinkForm(forms.ModelForm):
    class Meta:
        model = SourceLink
        fields = ('url', 'title', 'description')
        labels = {'url': ("Insérer l'url à ajouter"),
                  'title': ("nom du lien"),
                  'description': ("(optionnel) décriver l'interet du lien "),
                  }
        widgets = {
            'description': CKEditorWidget(config_name='redac_law'),
        }


class ModifPropositionForm(PropositionForm):
    commit_com = forms.CharField(
        widget=CKEditorWidget(config_name='redac_law'))


class CreateNewLawForm(forms.ModelForm):
    class Meta:
        model = LawArticle
        fields = ('title', 'text_law', 'details_law')
        labels = {'title': ('Intitulé de votre loi'),
                  'text_law': ("votre proposition de loi"),
                  'details_law': ("But recherché par " +
                                  "cette nouvelle loi:"),
                  }
        widgets = {
            'text_law': CKEditorWidget(config_name='redac_law'),
            'details_law': CKEditorWidget(config_name='redac_law'),
        }
