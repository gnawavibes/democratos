# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from CreateYourLaws.models import LawArticle, Commit  # , UserSession
import CreateYourLaws.models
from django.template.loader_tags import BlockNode, ExtendsNode
from difflib import SequenceMatcher


def get_path(obj):
    """ findthe path made to the object in input.
    The Object must be made from the models classes:
    - Question
    - Disclaim
    - Opinion
    - Explaination
    Return list of parents until law article & law code """

    parent = obj.content_type.model_class().objects.get(id=obj.object_id)
    if parent.title is not None:
        list_parents = [(get_model_type_in_str(parent),
                         parent.id,
                         parent.title)]
    else:
        text = get_ref_text(obj)
        list_parents = [(get_model_type_in_str(parent),
                         parent.id,
                         text)]
    while isinstance(parent, LawArticle) is False:
        parent = parent.content_type.model_class().objects.get(
            id=parent.object_id)
        if parent.title is not None:
            list_parents.append((get_model_type_in_str(parent),
                                 parent.id,
                                 parent.title))
        else:
            text = get_ref_text(obj)
            list_parents.append((get_model_type_in_str(parent),
                                 parent.id,
                                 text))
    list_parents.reverse()
    LawCode = parent.law_code
    return LawCode, list_parents


def get_model_type_in_str(obj):
    """ Return the model type of obj in str for urls"""
    if type(obj) is CreateYourLaws.models.Question:
        return 'qst'
    elif type(obj) is CreateYourLaws.models.Explaination:
        return 'exp'
    elif type(obj) is CreateYourLaws.models.Posopinion:
        return 'opp'
    elif type(obj) is CreateYourLaws.models.Negopinion:
        return 'opn'
    elif type(obj) is CreateYourLaws.models.Proposition:
        return 'prp'
    elif type(obj) is CreateYourLaws.models.LawArticle:
        return 'law'
    elif type(obj) is CreateYourLaws.models.Commit:
        return 'cmt'
    elif type(obj) is CreateYourLaws.models.SourceLink:
        return 'src'
    else:
        raise ValueError


def get_ref_text(obj):
    """ Necessity because each reflection has its own
    text appelation (CKeditor trouble)"""
    if type(obj) is CreateYourLaws.models.Question:
        return obj.text_qst
    elif type(obj) is CreateYourLaws.models.Explaination:
        return obj.text_exp
    elif type(obj) is CreateYourLaws.models.Posopinion:
        return obj.text_opp
    elif type(obj) is CreateYourLaws.models.Negopinion:
        return obj.text_opn
    elif type(obj) is CreateYourLaws.models.Proposition:
        return obj.text_prop
    elif type(obj) is CreateYourLaws.models.SourceLink:
        return obj.description
    elif type(obj) is CreateYourLaws.models.LawArticle:
        return obj.text
    else:
        raise ValueError


def get_the_instance(obj, Id):
    """ From the class in string, and Id, get the corresponding instance"""
    if obj == 'qst':
        return CreateYourLaws.models.Question.objects.get(id=Id)
    elif obj == 'exp':
        return CreateYourLaws.models.Explaination.objects.get(id=Id)
    elif obj == 'opp':
        return CreateYourLaws.models.Posopinion.objects.get(id=Id)
    elif obj == 'opn':
        return CreateYourLaws.models.Negopinion.objects.get(id=Id)
    elif obj == 'prp':
        return CreateYourLaws.models.Proposition.objects.get(id=Id)
    elif obj == 'law':
        return CreateYourLaws.models.LawArticle.objects.get(id=Id)
    elif obj == 'cmt':
        return CreateYourLaws.models.Commit.objects.get(id=Id)
    elif obj == 'src':
        return CreateYourLaws.models.SourceLink.objects.get(id=Id)
    else:
        raise ValueError


def get_box_parents(Box):
    listparents = []
    lastbox = Box
    while lastbox.rank != 1:
        parent = lastbox.block
        listparents.append((parent.title, parent.id, 2))
        lastbox = parent
    parent = Box.law_code
    listparents.append((parent.title, parent.id, 1))
    listparents.reverse()
    return listparents


def get_ref_parents(ref, typeref):
    if typeref == 'law':
        parent = ref.block
        if parent is None:
            listparents = []
        else:
            listparents = [(parent.title, parent.id, 2)]
            while parent.rank != 1:
                parent = parent.block
                listparents.append((parent.title, parent.id, 2))
        law_code = ref.law_code
        listparents.append((law_code.title, law_code.id, 1))
        fstparent = listparents[0]
        listparents.reverse()
    elif typeref == 'prp':
        listparents = []
        fstparent = [get_model_type_in_str(ref.content_object),
                     ref.content_object.id,
                     ref.content_object.title,
                     ]
        law_code = ref.law_article.law_code
    else:
        law_code, listparents = get_path(ref)
        fstparent = listparents[0]
    return law_code, listparents, fstparent


def CleanCkeditor(txt):
    if txt[0:9] == "<p><br />":
        txt = txt[0:3] + txt[10:]
    return txt
