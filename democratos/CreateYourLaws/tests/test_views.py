from django.urls import reverse, resolve
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase
from django.test.client import Client
from django.http import (
    JsonResponse, HttpResponse, Http404, HttpResponseNotFound,
    HttpResponseNotAllowed
)
from django.contrib.contenttypes.models import ContentType
from mixer.backend.django import mixer
from datetime import datetime, timedelta
from CreateYourLaws.views_functions import get_model_type_in_str
import pytz
import pytest
from unittest import mock

from CommonsJustice.models import JusticeTask
from CreateYourLaws.models import *
from CreateYourLaws.views import *
from UserManager.models import Note, CYL_user
import random


@pytest.mark.django_db
class TestHomeView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestHomeView, cls).setUpClass()
        cls.path = reverse('home')
        cls.user = mixer.blend(CYL_user)

    @mock.patch(
        "CreateYourLaws.models.LawCode.objects.all",
        mock.MagicMock(name="all"),
    )
    def test_home_GET_user_logged(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = home(request)
        self.assertTrue(LawCode.objects.all.called)
        self.assertTemplateUsed('home.html')
        self.assertTrue(isinstance(response, HttpResponse))

    @mock.patch(
        "CreateYourLaws.models.LawCode.objects.all",
        mock.MagicMock(name="all"),
    )
    def test_home_GET_anonymous_user(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = AnonymousUser()
        response = home(request)
        self.assertTrue(LawCode.objects.all.called)
        self.assertTemplateUsed('home.html')
        self.assertTrue(isinstance(response, HttpResponse))

    @mock.patch(
        "CreateYourLaws.models.LawCode.objects.all",
        mock.MagicMock(name="all"),
    )
    def test_home_POST_user_logged(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        response = home(request)
        self.assertTrue(LawCode.objects.all.called)
        self.assertTemplateUsed('home.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "script2")

    @mock.patch(
        "CreateYourLaws.models.LawCode.objects.all",
        mock.MagicMock(name="all"),
    )
    def test_home_POST_anonymous_user(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = AnonymousUser()
        response = home(request)
        self.assertTrue(LawCode.objects.all.called)
        self.assertTemplateUsed('home.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "script2")

    @mock.patch(
        "CreateYourLaws.models.LawCode.objects.all",
        mock.MagicMock(name="all"),
    )
    def test_home_POST_loadall_False_user_logged(self):
        request = RequestFactory().post(
            self.path,
            loadall=False,
        )
        request.user = self.user
        response = home(request)
        self.assertTrue(LawCode.objects.all.called)
        self.assertTemplateUsed('home.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "intro")
        self.assertContains(response, "content")

    @mock.patch(
        "CreateYourLaws.models.LawCode.objects.all",
        mock.MagicMock(name="all"),
    )
    def test_home_POST_loadall_False_anonymous_user(self):
        request = RequestFactory().post(
            self.path,
            loadall=False,
        )
        request.user = AnonymousUser()
        response = home(request)
        self.assertTrue(LawCode.objects.all.called)
        self.assertTemplateUsed('home.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "intro")
        self.assertContains(response, "content")

    @classmethod
    def tearDownClass(cls):
        super(TestHomeView, cls).tearDownClass()


@pytest.mark.django_db
class TestUpView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestUpView, cls).setUpClass()
        cls.path = reverse('UP')

    def setUp(self):
        self.user = mixer.blend(CYL_user, end_of_sanction=None)

    def test_UP_unauthenticated_user(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = AnonymousUser()
        response = UP(request)
        self.assertIn('UserManager/accounts/login/', response.url)

    def test_UP_User_Punished(self):
        self.user.end_of_sanction = datetime.now(
            tz=pytz.UTC) + timedelta(days=5)
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        response = UP(request)
        self.assertIn(
            '/CommonsJustice/YouArePunished',
            response.url,
        )

    def test_UP_authenticated_user_GET(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = UP(request)
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_UP_User_has_job(self):
        Task1 = mixer.blend(
            JusticeTask,
            worker=self.user,
            type_task=1,
        )
        ref = mixer.blend(Explaination)
        client = Client()
        client.force_login(self.user)
        response = client.post(
            self.path,
            {
                "slug": get_model_type_in_str(ref)+":"+str(ref.id),
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertEqual(content["message"], "user_has_job")

    def test_UP_authenticated_reflection_does_not_exist(self):
        client = Client()
        client.force_login(self.user)
        response = client.post(
            self.path,
            {
                "slug": "odf:23",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @mock.patch(
        "CreateYourLaws.models.Note.save",
        mock.MagicMock(name="save"),
    )
    def test_UP_authenticated_Standard_reflection_has_no_notes(self):
        ref = mixer.blend(Explaination)
        request = RequestFactory().post(
            self.path,
            {
                "slug": get_model_type_in_str(ref)+":"+str(ref.id),
            }
        )
        request.user = self.user
        response = UP(request)
        self.assertTrue(Note.save.called)
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "message")
        self.assertContains(response, "approb")
        self.assertContains(response, "data")
        content = eval(response.content)
        self.assertEqual(content["message"], "")

    @mock.patch(
        "CreateYourLaws.models.Note.save",
        mock.MagicMock(name="save"),
    )
    def test_UP_authenticated_Standard_reflection_has_note_approved(self):
        ref = mixer.blend(Explaination)
        ct = ContentType.objects.get_for_model(ref)
        note = mixer.blend(
            Note,
            user=self.user,
            content_type=ct,
            object_id=ref.id,
            approve=True,
        )
        request = RequestFactory().post(
            self.path,
            {
                "slug": get_model_type_in_str(ref)+":"+str(ref.id),
            }
        )
        request.user = self.user
        with mock.patch(
            'UserManager.models.Note.objects.get_or_create',
            return_value=(note, False),
        ) as mock_get_or_create:
            response = UP(request)
            self.assertTrue(Note.save.called)
            self.assertTrue(isinstance(response, JsonResponse))
            self.assertContains(response, "message")
            self.assertContains(response, "approb")
            self.assertContains(response, "data")
            content = eval(response.content)
            self.assertNotEqual(content["message"], "")

    @mock.patch(
        "CreateYourLaws.models.Note.save",
        mock.MagicMock(name="save"),
    )
    @mock.patch(
        "CreateYourLaws.models.Note.objects.get",
        mock.MagicMock(name="get"),
    )
    @mock.patch(
        "CreateYourLaws.models.Note.objects.filter",
        mock.MagicMock(name="filter"),
    )
    @mock.patch(
        "CreateYourLaws.models.Proposition.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_UP_authenticated_Proposition(self):
        law = mixer.blend(LawArticle)
        prop = mixer.blend(Proposition, law_article=law)
        otherprop = []
        notes = []
        for x in range(5):
            new_prop = mixer.blend(
                Proposition,
                law_article=law,
                approval_ratio=50,
            )
            otherprop.append(new_prop)
            notes.append(mixer.blend(
                Note,
                user=self.user,
                content_object=new_prop,
                approve=bool(random.getrandbits(1)),
            ))
        note = mixer.blend(
            Note,
            user=self.user,
            content_object=prop,
            approve=True,
        )
        request = RequestFactory().post(
            self.path,
            {
                "slug": get_model_type_in_str(prop)+":"+str(prop.id),
            },
        )
        request.user = self.user
        response = UP(request)
        self.assertTrue(Note.save.called)
        self.assertTrue(Note.objects.get.called)
        self.assertTrue(Proposition.objects.filter.called)
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "message")
        self.assertContains(response, "approb")
        self.assertContains(response, "data")
        content = eval(response.content)
        self.assertTrue(content["message"], "")

    @mock.patch(
        "CreateYourLaws.models.Note.save",
        mock.MagicMock(name="save"),
    )
    @mock.patch(
        "CreateYourLaws.models.Proposition.objects.filter",
        mock.MagicMock(name="filter"),
    )
    # <-------------------------------------------
    def test_UP_authenticated_LawArticle(self):
        # To be continue...
        law = mixer.blend(LawArticle)
        otherprop = []
        notes = []
        for x in range(5):
            new_prop = mixer.blend(
                Proposition,
                law_article=law,
                approval_ratio=50,
            )
            otherprop.append(new_prop)
            for y in range(3):
                notes.append(mixer.blend(
                    Note,
                    user=self.user,
                    content_object=new_prop,
                    approve=bool(random.getrandbits(1)),
                ))
        note = mixer.blend(
            Note,
            user=self.user,
            content_object=law,
            approve=True,
        )
        request = RequestFactory().post(
            self.path,
            {
                "slug": get_model_type_in_str(law)+":"+str(law.id),
            },
        )
        request.user = self.user
        response = UP(request)
        self.assertTrue(Note.save.called)
        self.assertTrue(Proposition.objects.filter.called)
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "message")
        self.assertContains(response, "approb")
        self.assertContains(response, "data")
        content = eval(response.content)

    @classmethod
    def tearDownClass(cls):
        super(TestUpView, cls).tearDownClass()


@pytest.mark.django_db
class TestDownView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestDownView, cls).setUpClass()
        cls.path = reverse('DOWN')

    def setUp(self):
        self.user = mixer.blend(CYL_user, end_of_sanction=None)

    def test_DOWN_unauthenticated_user(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = AnonymousUser()
        response = DOWN(request)
        self.assertIn('UserManager/accounts/login/', response.url)

    def test_DOWN_User_Punished(self):
        self.user.end_of_sanction = datetime.now(
            tz=pytz.UTC) + timedelta(days=5)
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        response = DOWN(request)
        self.assertIn(
            '/CommonsJustice/YouArePunished',
            response.url,
        )

    def test_DOWN_authenticated_user_GET(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = DOWN(request)
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_DOWN_User_has_job(self):
        Task1 = mixer.blend(
            JusticeTask,
            worker=self.user,
            type_task=1,
        )
        ref = mixer.blend(Explaination)
        client = Client()
        client.force_login(self.user)
        response = client.post(
            self.path,
            {
                "slug": get_model_type_in_str(ref)+":"+str(ref.id),
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertEqual(content["message"], "user_has_job")

    def test_DOWN_authenticated_reflection_does_not_exist(self):
        client = Client()
        client.force_login(self.user)
        response = client.post(
            self.path,
            {
                "slug": "odf:23",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @mock.patch(
        "CreateYourLaws.models.Note.save",
        mock.MagicMock(name="save"),
    )
    def test_DOWN_authenticated_has_no_notes(self):
        ref = mixer.blend(Explaination)
        request = RequestFactory().post(
            self.path,
            {
                "slug": get_model_type_in_str(ref)+":"+str(ref.id),
            }
        )
        request.user = self.user
        response = DOWN(request)
        self.assertTrue(Note.save.called)
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "message")
        self.assertContains(response, "approb")
        content = eval(response.content)
        self.assertEqual(content["message"], "")

    @mock.patch(
        "CreateYourLaws.models.Note.save",
        mock.MagicMock(name="save"),
    )
    def test_DOWN_authenticated_has_note_approved(self):
        ref = mixer.blend(Explaination)
        ct = ContentType.objects.get_for_model(ref)
        note = mixer.blend(
            Note,
            user=self.user,
            content_type=ct,
            object_id=ref.id,
            approve=True,
        )
        request = RequestFactory().post(
            self.path,
            {
                "slug": get_model_type_in_str(ref)+":"+str(ref.id),
            }
        )
        request.user = self.user
        with mock.patch(
            'UserManager.models.Note.objects.get_or_create',
            return_value=(note, False),
        ) as mock_get_or_create:
            response = DOWN(request)
            self.assertTrue(Note.save.called)
            self.assertTrue(isinstance(response, JsonResponse))
            self.assertContains(response, "message")
            self.assertContains(response, "approb")
            content = eval(response.content)
            self.assertEqual(content["message"], "")

    @mock.patch(
        "CreateYourLaws.models.Note.save",
        mock.MagicMock(name="save"),
    )
    def test_DOWN_authenticated_has_note_not_approved(self):
        ref = mixer.blend(Explaination)
        ct = ContentType.objects.get_for_model(ref)
        note = mixer.blend(
            Note,
            user=self.user,
            content_type=ct,
            object_id=ref.id,
            approve=False,
        )
        request = RequestFactory().post(
            self.path,
            {
                "slug": get_model_type_in_str(ref)+":"+str(ref.id),
            }
        )
        request.user = self.user
        with mock.patch(
            'UserManager.models.Note.objects.get_or_create',
            return_value=(note, False),
        ) as mock_get_or_create:
            response = DOWN(request)
            self.assertTrue(Note.save.called)
            self.assertTrue(isinstance(response, JsonResponse))
            self.assertContains(response, "message")
            self.assertContains(response, "approb")
            content = eval(response.content)
            self.assertNotEqual(content["message"], "")

    @classmethod
    def tearDownClass(cls):
        super(TestDownView, cls).tearDownClass()


@pytest.mark.django_db
class TestInDatBoxView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestInDatBoxView, cls).setUpClass()
        cls.path = reverse('InDatBox')
        cls.user = mixer.blend(CYL_user, end_of_sanction=None)
        cls.client = Client()
        cls.client.force_login(cls.user)
        cls.codes = []
        cls.blocs = []
        cls.articles = []
        idbox = 1
        for i in range(2):
            c = mixer.blend(LawCode, id=i+1)
            cls.codes.append(c)
            for j in range(3):
                b1 = mixer.blend(CodeBlock, rank=1, id=idbox, law_code=c)
                idbox += 1
                cls.blocs.append(b1)
                for k in range(3):
                    b2 = mixer.blend(CodeBlock, rank=2,
                                     block=b1, id=idbox, law_code=c)
                    idbox += 1
                    if k % 2 == 0:
                        b2.is_cbp = True
                    else:
                        b2.is_cbp = False
                        for l in range(4):
                            art = mixer.blend(
                                LawArticle, block=b2, law_code=c)
                            if l % 2 == 0:
                                art.is_lwp = True
                            else:
                                art.is_lwp = False
                            cls.articles.append(art)
                    cls.blocs.append(b1)
        a_block = mixer.blend(CodeBlock, rank=2, block=b1, id=999, law_code=c)

    @mock.patch(
        "CreateYourLaws.models.LawArticle.objects.filter",
        mock.MagicMock(name="filter"),
    )
    @mock.patch(
        "CreateYourLaws.models.CodeBlock.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_InDatBox_request_method_is_ajax(self):
        response = self.client.get(self.path, {"slug": "1:2"}, **{'HTTP_X_REQUESTED_WITH':
                                                                  'XMLHttpRequest'})
        self.assertEqual(LawArticle.objects.filter.call_count, 2)
        self.assertEqual(CodeBlock.objects.filter.call_count, 2)
        self.assertTemplateUsed('InDatBox.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "main")
        self.assertContains(response, "nav")
        content = eval(response.content)
        self.assertEqual(content["box_type"], "1")
        self.assertEqual(content["box_id"], "2")

    @mock.patch(
        "CreateYourLaws.models.LawArticle.objects.filter",
        mock.MagicMock(name="filter"),
    )
    @mock.patch(
        "CreateYourLaws.models.CodeBlock.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_InDatBox_request_method_is_not_ajax_law_code(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = In_dat_box(request, box_type=1, box_id=1)
        self.assertEqual(LawArticle.objects.filter.call_count, 2)
        self.assertEqual(CodeBlock.objects.filter.call_count, 2)
        self.assertTemplateUsed('InDatBox.html')
        self.assertTrue(isinstance(response, HttpResponse))

    @mock.patch(
        "CreateYourLaws.models.LawArticle.objects.filter",
        mock.MagicMock(name="filter"),
    )
    @mock.patch(
        "CreateYourLaws.models.CodeBlock.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_InDatBox_request_method_is_not_ajax_code_block(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = In_dat_box(request, box_type=1, box_id=2)
        self.assertEqual(LawArticle.objects.filter.call_count, 2)
        self.assertEqual(CodeBlock.objects.filter.call_count, 2)
        self.assertTemplateUsed('InDatBox.html')
        self.assertTrue(isinstance(response, HttpResponse))

    @mock.patch(
        "CreateYourLaws.models.LawArticle.objects.filter",
        mock.MagicMock(name="filter"),
    )
    @mock.patch(
        "CreateYourLaws.models.CodeBlock.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_InDatBox_box_is_law_code(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = In_dat_box(request, box_type=1, box_id=2)
        self.assertEqual(LawArticle.objects.filter.call_count, 2)
        self.assertEqual(CodeBlock.objects.filter.call_count, 2)
        self.assertTemplateUsed('InDatBox.html')
        self.assertTrue(isinstance(response, HttpResponse))

    @mock.patch(
        "CreateYourLaws.models.LawArticle.objects.filter",
        mock.MagicMock(name="filter"),
    )
    @mock.patch(
        "CreateYourLaws.models.CodeBlock.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_InDatBox_box_is_code_block(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = In_dat_box(request, box_type=2, box_id=999)
        self.assertEqual(LawArticle.objects.filter.call_count, 2)
        self.assertEqual(CodeBlock.objects.filter.call_count, 2)
        self.assertTemplateUsed('InDatBox.html')
        self.assertTrue(isinstance(response, HttpResponse))

    def test_InDatBox_box_unknown_box_type(self):
        response = self.client.get(self.path, {"slug": "3:2"}, **{'HTTP_X_REQUESTED_WITH':
                                                                  'XMLHttpRequest'})
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_InDatBox_request_method_is_POST(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    @classmethod
    def tearDownClass(cls):
        super(TestInDatBoxView, cls).tearDownClass()


@pytest.mark.django_db
class TestgetnewlawpropsView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestgetnewlawpropsView, cls).setUpClass()
        cls.path = reverse('getnewlawprops')
        cls.user = mixer.blend(CYL_user, end_of_sanction=None)
        cls.code = mixer.blend(LawCode)
        cls.bloc = mixer.blend(CodeBlock, rank=1, law_code=cls.code)
        cls.client = Client()
        cls.lawartc = mixer.blend(
            LawArticle,
            law_code=cls.code,
            is_lwp=True)
        cls.lawartb = mixer.blend(
            LawArticle,
            block=cls.bloc,
            is_lwp=True
        )

    def setUp(self):
        self.client.force_login(self.user)

    @mock.patch(
        "CreateYourLaws.models.LawArticle.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_getnewlawprops_request_method_is_POST(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_getnewlawprops_working_new_lwp_for_law_code(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'D:1'
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, 'listofNLP')
        content = eval(response.content)
        self.assertEqual(content['box_type'], 'D')
        self.assertEqual(content['box_id'], '1')
        self.assertTemplateUsed('GetForm.html')

    ###########################################################
    #                                                         #
    #                   TO BE CONTINUED                       #
    #                                                         #
    ###########################################################

    @classmethod
    def tearDownClass(cls):
        super(TestgetnewlawpropsView, cls).tearDownClass()


@pytest.mark.django_db
class Testget_reflectionView(TestCase):

    @classmethod
    def setUpClass(cls):
        super(Testget_reflectionView, cls).setUpClass()
        cls.path = reverse('reflection')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode, id=1)
        cls.codebloc1 = mixer.blend(
            CodeBlock,
            id=1,
            rank=1,
            law_code=cls.lawcode,
        )
        cls.codebloc2 = mixer.blend(
            CodeBlock,
            id=2,
            rank=2,
            law_code=cls.lawcode,
            block=cls.codebloc1,
        )
        cls.lawart = mixer.blend(LawArticle, block=cls.codebloc2, id=1)
        cls.qst = mixer.blend(Question, content_object=cls.lawart, id=1)
        cls.exp = mixer.blend(Explaination, content_object=cls.qst, id=1)
        cls.opp = mixer.blend(Posopinion, content_object=cls.lawart, id=1)
        cls.opn = mixer.blend(Negopinion, content_object=cls.lawart, id=1)
        cls.prp = mixer.blend(Proposition, content_object=cls.lawart, id=1)
        cls.listref = ["law", "qst", "exp", "opp", "opn", "prp"]

    def setUp(self):
        self.client.force_login(self.user)

    def test_get_reflections_request_method_is_GET(self):
        for el in self.listref:
            response = self.client.get(
                self.path,
                {'slug': el+':1'},
                **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertTemplateUsed('GetReflection.html')
            self.assertTrue(isinstance(response, JsonResponse))
            self.assertContains(response, "main")
            self.assertContains(response, "nav")
            content = eval(response.content)
            self.assertEqual(content["typeref"], el)
            self.assertEqual(content["id_ref"], "1")

    def test_get_reflections_unkown_ref_type(self):
        response = self.client.get(
            self.path,
            {'slug': 'sjn:1'},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_get_reflections_unkown_obj(self):
        response = self.client.get(
            self.path,
            {'slug': 'exp:112'},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_get_reflections_request_method_is_POST(self):
        response = self.client.post(
            self.path,
            {'slug': 'exp:1'},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_get_reflections_request_method_is_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        for el in self.listref:
            response = get_reflection(request, typeref=el, id_ref=1)
            self.assertTrue(isinstance(response, HttpResponse))

    @classmethod
    def tearDownClass(cls):
        super(Testget_reflectionView, cls).tearDownClass()


@pytest.mark.django_db
class TestPostReflectionView(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestPostReflectionView, cls).setUpClass()
        cls.path = reverse('PostReflection')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.listref = ["law", "qst", "exp", "opp", "opn", "prp"]
        cls.lawcode = mixer.blend(LawCode)
        cls.codebloc1 = mixer.blend(
            CodeBlock,
            rank=1,
            id=1,
            law_code=cls.lawcode,
        )
        cls.codebloc2 = mixer.blend(
            CodeBlock,
            rank=2,
            id=2,
            law_code=cls.lawcode,
            block=cls.codebloc1,
        )
        cls.law = mixer.blend(LawArticle, block=cls.codebloc2)
        cls.qst = mixer.blend(Question, content_object=cls.law)
        cls.exp = mixer.blend(Explaination, content_object=cls.law)
        cls.opp = mixer.blend(Posopinion, content_object=cls.law)
        cls.prp = mixer.blend(
            Proposition, content_object=cls.law,
            title="bli jnroi qrub ",
            text_prp="bli jnroi qrub",
            text_details="bli jnroi qrsdf f dgub ",
        )
        cls.opn = mixer.blend(Negopinion, content_object=cls.law)
        cls.src = mixer.blend(
            SourceLink,
            content_object=cls.law,
        )
        for i, el in enumerate(['qst', 'exp', 'opp', 'opn', "prp"]):
            setattr(cls, 'qst'+el, mixer.blend(
                    Question,
                    content_object=getattr(cls, el, None),
                    )
                    )
            setattr(cls, 'exp'+el, mixer.blend(
                    Explaination,
                    content_object=getattr(cls, el, None),
                    )
                    )
            setattr(cls, 'opp'+el, mixer.blend(
                    Posopinion,
                    content_object=getattr(cls, el, None),
                    )
                    )
            setattr(cls, 'prp'+el, mixer.blend(
                    Proposition,
                    content_object=getattr(cls, el, None),
                    )
                    )
            setattr(cls, 'src'+el, mixer.blend(
                    SourceLink,
                    content_object=getattr(cls, el, None),
                    )
                    )
            setattr(cls, 'opn'+el, mixer.blend(
                    Negopinion,
                    content_object=getattr(cls, el, None),
                    )
                    )

    def setUp(self):
        self.client.force_login(self.user)

    def test_post_reflection_user_unauthenticated(self):
        response = Client().post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_post_reflection_user_is_punished(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=datetime.now(tz=pytz.UTC)+timedelta(seconds=180)
        )
        client = Client()
        client.force_login(user)
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn('/CommonsJustice/YouArePunished', response.url)

    def test_post_reflection_User_has_job(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=None
        )
        Task1 = mixer.blend(
            JusticeTask,
            worker=user,
            type_task=1,
        )
        client = Client()
        client.force_login(user)
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertEqual(content["message"], "user_has_job")

    def test_post_reflection_request_is_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = PostReflection(request)

    def test_post_reflection_request_method_is_GET(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_post_reflection_all_simple_posting_working_case_except_sourceform(self):
        for ref in self.listref:
            print("Dans pytest on est à ref: ", ref)
            if ref in ["opn", "opp", "qst", "exp"]:
                listform = ["qstf", "expf"]
            else:
                listform = ["prpf", "qstf", "expf", "opnf", "oppf"]
            for form in listform:
                response = self.client.post(
                    self.path,
                    {
                        'typeref': ref,
                        'typeform': form,
                        'ref_id': getattr(self, ref).id,
                        'IsModif': '',
                        'title': "a title",
                        'text_'+form[0:3]: "blablabla",
                        'details_'+form[0:3]: "blablabla2",

                    },
                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
                )
                self.assertTrue(isinstance(response, JsonResponse))
                content = eval(response.content)
                print(response.content)
                self.assertEqual(content['section_type'], form[0:3])
                self.assertEqual(content["message"], "")
                if form in ["qstf", "expf"]:
                    self.assertEqual(content['typeref'], ref)
                    self.assertEqual(content["typeform"], form)
                    assert (content['tdid'] != "" and int(content['tdid']))
                else:
                    assert (content['tdid'] == "")

    @mock.patch(
        "CommitApp.views.create_commit",
        mock.MagicMock(name="create_commit"),
    )
    def test_post_reflection_all_modif_working_case_except_sourceform(self):
        for ref in self.listref:
            print("Dans pytest on est à ref: ", ref)
            if ref == "law":
                ref2 = ""
            else:
                ref2 = ref
            if ref in ["opn", "opp", "qst", "exp"]:
                listform = ["qstf", "expf"]
            else:
                listform = ["qstf", "expf", "opnf", "oppf"]
            for form in listform:
                response = self.client.post(
                    self.path,
                    {
                        'typeref': ref,
                        'typeform': form,
                        'idform': getattr(self, form[0:3]+ref2).id,
                        'ref_id': getattr(self, ref).id,
                        'IsModif': True,
                        'title': "a title",
                        'text_'+form[0:3]: "blablabla",
                        'details_'+form[0:3]: "blablabla2",
                    },
                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
                )
                self.assertTrue(isinstance(response, JsonResponse))
                content = eval(response.content)
                self.assertEqual(content["message"],
                                 "Votre réflection a bien été modifiée!")

    def test_post_reflection_simple_posting_working_case_sourceform(self):
        self.src = mixer.blend(SourceLink, content_object=self.law)
        for ref in self.listref:
            response = self.client.post(
                self.path,
                {
                    'typeref': ref,
                    'typeform': 'srcf',
                    'ref_id': getattr(self, ref).id,
                    'IsModif': '',
                    'title': "a title",
                    'url': "www.testing.com",
                    'description': "blablabla2",
                },
                **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertTrue(isinstance(response, JsonResponse))
            content = eval(response.content)
            self.assertEqual(content['section_type'], "src")
            self.assertEqual(content["message"], "")

    def test_post_reflection_is_modif_working_case_sourceform(self):
        self.src = mixer.blend(SourceLink, content_object=self.law)
        for ref in self.listref:
            if ref == "law":
                ref2 = ""
            else:
                ref2 = ref
            response = self.client.post(
                self.path,
                {
                    'typeref': ref,
                    'typeform': 'srcf',
                    'idform': getattr(self, "src"+ref2).id,
                    'ref_id': getattr(self, ref).id,
                    'IsModif': True,
                    'title': "a title",
                    'url': "www.testing.com",
                    'description': "blablabla2",
                },
                **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertTrue(isinstance(response, JsonResponse))
            content = eval(response.content)
            print(content["message"])
            self.assertEqual(content["message"],
                             "Votre réflection a bien été modifiée!")

    def test_post_reflection_ref_type_does_not_exist(self):
        response = self.client.post(
            self.path,
            {
                'typeref': 'pdt',
                'typeform': 'srcf',
                'idform': 1,
                'ref_id': 1,
                'IsModif': True,
                'title': "a title",
                'url': "www.testing.com",
                'description': "blablabla2",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_post_reflection_form_type_does_not_exist(self):
        response = self.client.post(
            self.path,
            {
                'typeref': 'law',
                'typeform': 'pdtg',
                'idform': 1,
                'ref_id': 1,
                'IsModif': True,
                'title': "a title",
                'url': "www.testing.com",
                'description': "blablabla2",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @classmethod
    def tearDownClass(cls):
        super(TestPostReflectionView, cls).tearDownClass()


@pytest.mark.django_db
class Testlist_of_reflections_View(TestCase):

    @classmethod
    def setUpClass(cls):
        super(Testlist_of_reflections_View, cls).setUpClass()
        cls.path = reverse('ListRef')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.listref = ["qst", "exp", "opp", "opn", "prp"]
        cls.lawcode = mixer.blend(LawCode, id=1)
        cls.codebloc1 = mixer.blend(
            CodeBlock,
            rank=1,
            id=1,
            law_code=cls.lawcode,
        )
        cls.codebloc2 = mixer.blend(
            CodeBlock,
            rank=2,
            id=2,
            law_code=cls.lawcode,
            block=cls.codebloc1,
        )
        cls.lawart = mixer.blend(LawArticle, block=cls.codebloc2, id=1)
        for i in range(5):
            setattr(cls, 'qst'+str(i), mixer.blend(Question,
                    content_object=cls.lawart, id=i+1))
            setattr(cls, 'exp'+str(i), mixer.blend(Explaination,
                    content_object=cls.lawart, id=i+1))
            setattr(cls, 'opp'+str(i), mixer.blend(Posopinion,
                    content_object=cls.lawart, id=i+1))
            setattr(cls, 'prp'+str(i), mixer.blend(
                Proposition, content_object=cls.lawart,
                title="bli jnroi qrub ",
                text_prp="bli jnroi qrub",
                text_details="bli jnroi qrsdf f dgub ",
                id=i+1
            ))
            setattr(cls, 'opn'+str(i), mixer.blend(Negopinion,
                    content_object=cls.lawart, id=i+1))

    def setUp(self):
        self.client.force_login(self.user)

    def test_list_of_reflections_all_type_working_cases_ajax(self):
        for ref in self.listref:
            response = self.client.get(
                self.path,
                {
                    'slug': 'law:1:'+ref,
                },
                **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertTemplateUsed('displaylist.html')
            self.assertTrue(isinstance(response, JsonResponse))
            self.assertContains(response, "main")
            self.assertContains(response, "nav")
            content = eval(response.content)
            self.assertEqual(content['parent_type'], 'law')
            self.assertEqual(content['parent_id'], "1")

    def test_list_of_reflections_all_type_working_cases_url_call(self):
        for ref in self.listref:
            response = self.client.get(
                reverse(
                    'listref',
                    kwargs={
                        'parent_type': "law",
                        'parent_id': 1,
                        'list_ref_type': ref,
                    }
                )
            )
            self.assertTrue(isinstance(response, HttpResponse))
            self.assertTemplateUsed('displaylist.html')

    def test_list_of_reflectionsajax_POST_error(self):
        response = self.client.post(
            self.path,
            {
                'slug': 'law:1:exp',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    @classmethod
    def tearDownClass(cls):
        super(Testlist_of_reflections_View, cls).tearDownClass()


@pytest.mark.django_db
class TestchildcommentsView(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestchildcommentsView, cls).setUpClass()
        cls.path = reverse('childcomments')
        cls.user = mixer.blend(CYL_user)
        cls.listref = ["qst", "exp"]
        cls.lawcode = mixer.blend(LawCode, id=1)
        cls.client = Client()
        cls.codebloc1 = mixer.blend(
            CodeBlock,
            rank=1,
            id=1,
            law_code=cls.lawcode,
        )
        cls.codebloc2 = mixer.blend(
            CodeBlock,
            rank=2,
            law_code=cls.lawcode,
            block=cls.codebloc1,
            id=2,
        )
        cls.lawart = mixer.blend(LawArticle, block=cls.codebloc2, id=1)
        cls.qst1 = mixer.blend(Question, content_object=cls.lawart, id=1)
        cls.exp1 = mixer.blend(Explaination, content_object=cls.lawart, id=1)
        for i in range(2, 6):
            print('exp' + str(i-1))
            if i == 5:
                setattr(cls, 'qst'+str(i), mixer.blend(
                    Question,
                    content_object=getattr(cls, 'exp' + str(i-1)),
                    is_censured=True,
                    id=i,
                )
                )
                setattr(cls, 'exp'+str(i), mixer.blend(
                    Explaination,
                    content_object=getattr(cls, 'qst' + str(i-1)),
                    is_censured=True,
                    id=i,
                )
                )

            else:
                setattr(cls, 'qst'+str(i), mixer.blend(
                    Question,
                    content_object=getattr(cls, 'exp' + str(i-1)),
                    id=i,
                )
                )
                setattr(cls, 'exp'+str(i), mixer.blend(
                    Explaination,
                    content_object=getattr(cls, 'qst' + str(i-1)),
                    id=i,
                )
                )

    def setUp(self):
        self.client.force_login(self.user)

    def test_childcomments_exist_and_are_working(self):
        for ref in ["exp", "qst"]:
            for i in range(1, 4):
                response = self.client.get(
                    self.path,
                    {
                        'slug': 'child'+ref+str(i),
                    },
                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
                )
                self.assertTemplateUsed('GetReflection.html')
                self.assertTrue(isinstance(response, JsonResponse))
                self.assertContains(response, "newcomments")
                content = eval(response.content)
                self.assertEqual(content['message'], '')
                self.assertEqual(content['typeref'], ref)

    def test_childcomments_does_not_exist(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'childexp'+str(12),
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "newcomments")
        content = eval(response.content)
        self.assertEqual(
            content['message'],
            "comment unfindable in DB"
        )

    def test_childcomments_unknown_ref_case(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'childiug'+str(12),
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_childcomments_POST(self):
        response = self.client.post(
            self.path,
            {
                'slug': 'childexp'+str(2),
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_childcomments_is_censured_True(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'childexp'+str(5),
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "newcomments")
        content = eval(response.content)
        self.assertEqual(
            content['message'],
            "comment unfindable in DB"
        )

    @classmethod
    def tearDownClass(cls):
        super(TestchildcommentsView, cls).tearDownClass()


@pytest.mark.django_db
class TestGetFormView(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestGetFormView, cls).setUpClass()
        cls.path = reverse('GetForm')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.listform = ["qst", "exp"]

    def setUp(self):
        self.client.force_login(self.user)

    def test_get_form_user_aunenthicated(self):
        response = Client().post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_get_form_user_is_punished(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=datetime.now(tz=pytz.UTC)+timedelta(seconds=180)
        )
        client = Client()
        client.force_login(user)
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn('/CommonsJustice/YouArePunished', response.url)

    def test_get_form_all_working_cases_GET(self):
        import random
        listref = ["law", "qst", "exp", "opp", "opn", "prp"]
        for ref in listref:
            for form in self.listform:
                idref = random.randint(0, 9999)
                name = ref+':'+form+':'+str(idref)
                response = self.client.get(
                    self.path,
                    {
                        'name': name,
                    },
                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
                )
                self.assertTemplateUsed('GetForm.html')
                self.assertTrue(isinstance(response, JsonResponse))
                self.assertContains(response, "newform")
                content = eval(response.content)
                self.assertEqual(content['idref'], str(idref))
                self.assertEqual(content['typeref'], ref)

    def test_get_form_request_POST(self):
        response = self.client.post(
            self.path,
            {
                'name': "law:exp:2",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_get_form_unknown_ref_case(self):
        response = self.client.get(
            self.path,
            {
                'name': "xdq:exp:2",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_get_form_unknown_form_case(self):
        response = self.client.get(
            self.path,
            {
                'name': "law:ong:2",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_get_form_request_is_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetForm(request)

    @classmethod
    def tearDownClass(cls):
        super(TestGetFormView, cls).tearDownClass()


@pytest.mark.django_db
class TestModifReflectionView(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestModifReflectionView, cls).setUpClass()
        cls.path = reverse('ModifReflection')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode, id=1)
        cls.codebloc1 = mixer.blend(
            CodeBlock,
            rank=1,
            id=1,
            law_code=cls.lawcode,
        )
        cls.codebloc2 = mixer.blend(
            CodeBlock,
            rank=2,
            id=2,
            law_code=cls.lawcode,
            block=cls.codebloc1,
        )
        cls.lawart = mixer.blend(LawArticle, block=cls.codebloc2, id=1)
        cls.prp = mixer.blend(
            Proposition, content_object=cls.lawart,
            title="bli jnroi qrub ",
            text_prp="bli jnroi qrub",
            text_details="bli jnroi qrsdf f dgub ",
            id=1,
        )
        for el in [cls.lawart, cls.prp]:
            for j in range(1, 3):
                if el == cls.lawart:
                    i = j
                else:
                    i = j+2
                setattr(cls, 'qst'+str(i), mixer.blend(
                    Question,
                    content_object=el,
                    title="title",
                    text_qst="some text...",
                    id=i,
                )
                )
                setattr(cls, 'exp'+str(i), mixer.blend(
                    Explaination,
                    content_object=el,
                    title="title",
                    text_exp="some text...",
                    id=i,
                )
                )
                setattr(cls, 'opp'+str(i), mixer.blend(
                    Posopinion,
                    content_object=el,
                    title="title",
                    text_opp="some text...",
                    id=i,
                )
                )
                setattr(cls, 'prp'+str(i), mixer.blend(
                    Proposition, content_object=el,
                    title="bli jnroi qrub ",
                    text_prp="bli jnroi qrub",
                    id=i,
                    text_details="bli jnroi qrsdf f dgub ")
                )
                setattr(cls, 'src'+str(i), mixer.blend(
                    SourceLink, content_object=el,
                    title="bli jnroi qrub ",
                    text_prp="bli jnroi qrub",
                    id=i,
                    text_details="bli jnroi qrsdf f dgub ")
                )
                setattr(cls, 'opn'+str(i), mixer.blend(
                    Negopinion,
                    content_object=el,
                    title="title",
                    text_opn="some text...",
                    id=i,
                )
                )

    def setUp(self):
        self.client.force_login(self.user)

    def test_modif_reflection_GET_request(self):
        response = self.client.get(
            self.path,
            {
                'typerequest': 'form',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_modif_reflection_unauthenticated_user(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_modif_reflection_User_Punished(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=datetime.now(tz=pytz.UTC) + timedelta(days=5),
        )
        client = Client()
        client.force_login(user)
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn(
            '/CommonsJustice/YouArePunished',
            response.url,
        )

    def test_modif_reflection_User_has_job(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=None
        )
        Task1 = mixer.blend(
            JusticeTask,
            worker=user,
            type_task=1,
        )
        client = Client()
        client.force_login(user)
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertEqual(content["message"], "user_has_job")

    def test_modif_reflection_request_is_not_ajax(self):
        request = RequestFactory().post(
            self.path,
            {
                'typerequest': 'form',
            },
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = ModifReflection(request)

    def test_modif_reflection_working_cases(self):
        for ref in ['law', 'prp']:
            for form in ["qst", "exp", "opp", 'opn', 'prp', 'src']:
                print(ref, "  ", form)
                response = self.client.post(
                    self.path,
                    {
                        'typerequest': 'form',
                        'typeform': form,
                        'idform': '2',
                        'typeref': ref,
                        'idref': '1',
                    },
                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
                )
                print(response)
                self.assertTrue(isinstance(response, JsonResponse))
                self.assertContains(response, "ModifForm")
                content = eval(response.content)
                self.assertEqual(content['typeref'], ref)
                self.assertEqual(content['typeform'], form)
                self.assertEqual(content['idref'], 1)
                self.assertEqual(content['idform'], 2)

    def test_modif_reflection_unknown_form_type_case(self):
        response = self.client.post(
            self.path,
            {
                'typerequest': 'form',
                'typeform': 'fgp',
                'idform': '2',
                'typeref': 'law',
                'idref': '1',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_modif_reflection_unknown_ref_type_case(self):
        response = self.client.post(
            self.path,
            {
                'typerequest': 'form',
                'typeform': 'exp',
                'idform': '2',
                'typeref': 'grd',
                'idref': '1',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_modif_reflection_unknown_form_case(self):
        response = self.client.post(
            self.path,
            {
                'typerequest': 'form',
                'typeform': 'exp',
                'idform': '152',
                'typeref': 'law',
                'idref': '1',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_modif_reflection_unknown_ref_case(self):
        response = self.client.post(
            self.path,
            {
                'typerequest': 'form',
                'typeform': 'exp',
                'idform': '2',
                'typeref': 'law',
                'idref': '9999585951',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @classmethod
    def tearDownClass(cls):
        super(TestModifReflectionView, cls).tearDownClass()


@pytest.mark.django_db
class TestDeleteReflectionView(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestDeleteReflectionView, cls).setUpClass()
        cls.path = reverse('DeleteReflection')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode, id=1)
        cls.codebloc1 = mixer.blend(
            CodeBlock,
            rank=1,
            id=1,
            law_code=cls.lawcode,
        )
        cls.codebloc2 = mixer.blend(
            CodeBlock,
            rank=2,
            id=2,
            law_code=cls.lawcode,
            block=cls.codebloc1,
        )
        cls.lawart = mixer.blend(LawArticle, block=cls.codebloc2, id=1)
        for i in range(1, 3):
            setattr(cls, 'qst'+str(i), mixer.blend(
                Question,
                content_object=cls.lawart,
                id=i,
            )
            )
            setattr(cls, 'exp'+str(i), mixer.blend(
                Explaination,
                content_object=cls.lawart,
                id=i,
            )
            )
            setattr(cls, 'opp'+str(i), mixer.blend(
                Posopinion,
                content_object=cls.lawart,
                id=i,
            )
            )
            setattr(cls, 'prp'+str(i), mixer.blend(
                Proposition,
                content_object=cls.lawart,
                id=i,
            )
            )
            setattr(cls, 'src'+str(i), mixer.blend(
                SourceLink,
                content_object=cls.lawart,
                id=i,
            )
            )
            setattr(cls, 'opn'+str(i), mixer.blend(
                Negopinion,
                content_object=cls.lawart,
                id=i,
            )
            )

    def setUp(self):
        self.client.force_login(self.user)

    def test_delete_reflection_GET_request(self):
        response = self.client.get(
            self.path,
            {
                'typerequest': 'form',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_delete_reflection_unauthenticated_user(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_delete_reflection_User_Punished(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=datetime.now(tz=pytz.UTC) + timedelta(days=5),
        )
        client = Client()
        client.force_login(user)
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn(
            '/CommonsJustice/YouArePunished',
            response.url,
        )

    def test_delete_reflection_User_has_job(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=None
        )
        Task1 = mixer.blend(
            JusticeTask,
            worker=user,
            type_task=1,
        )
        client = Client()
        client.force_login(user)
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertEqual(content["message"], "user_has_job")

    def test_delete_reflection_request_is_not_ajax(self):
        request = RequestFactory().post(
            self.path,
            {
                'typerequest': 'form',
            },
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = ModifReflection(request)

    def test_delete_reflection_working_cases(self):
        for ref in ["qst", "exp", "opp", 'opn', 'prp', 'src']:
            for i in range(1, 3):
                response = self.client.post(
                    self.path,
                    {
                        'typeref': ref,
                        'idref': str(i),
                    },
                    **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
                )
                print(response)
                self.assertTrue(isinstance(response, JsonResponse))
                self.assertContains(response, "message")

    def test_delete_reflection_unknown_ref_type_case(self):
        response = self.client.post(
            self.path,
            {
                'typeref': 'grd',
                'idref': '1',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_delete_reflection_unknown_ref_case(self):
        response = self.client.post(
            self.path,
            {
                'typeref': 'exp',
                'idref': '9999585951',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @classmethod
    def tearDownClass(cls):
        super(TestDeleteReflectionView, cls).tearDownClass()


@pytest.mark.django_db
class TestCreateNewLawView(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestCreateNewLawView, cls).setUpClass()
        cls.path = reverse('CreateNewLaw')
        cls.user = mixer.blend(
            CYL_user,
            end_of_sanction=None
        )
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode, id=1)
        cls.codebloc1 = mixer.blend(
            CodeBlock,
            rank=1,
            id=1,
            law_code=cls.lawcode,
        )
        cls.codebloc2 = mixer.blend(
            CodeBlock,
            rank=2,
            law_code=cls.lawcode,
            block=cls.codebloc1,
        )

    def setUp(self):
        self.client.force_login(self.user)

    def test_CreateNewLaw_POST_request(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_CreateNewLaw_unauthenticated_user(self):
        client = Client()
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_CreateNewLaw_User_Punished(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=datetime.now(tz=pytz.UTC) + timedelta(days=5),
        )
        client = Client()
        client.force_login(user)
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn(
            '/CommonsJustice/YouArePunished',
            response.url,
        )

    def test_CreateNewLaw_User_has_job(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=None
        )
        Task1 = mixer.blend(
            JusticeTask,
            worker=user,
            type_task=1,
        )
        client = Client()
        client.force_login(user)
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertEqual(content["message"], "user_has_job")

    def test_CreateNewLaw_request_is_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = CreateNewLaw(request, 2, 1)

    def test_CreateNewLaw_request_is_ajax_working(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'D:1',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertTemplateUsed('GetForm')

    def test_CreateNewLaw_request_is_ajax_error_block(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'D:10',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_CreateNewLaw_request_is_ajax_unknown_parent(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'ZIo:10',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_CreateNewLaw_request_is_ajax_error_law_code(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'E:10',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_CreateNewLaw_modif_lwp_from_law_code(self):
        lawart = mixer.blend(
            LawArticle,
            id=10,
            title="test",
            text_law="C'est un test",
            law_code_id=1,
            is_lwp=True,
        )
        response = self.client.get(
            self.path,
            {
                'slug': 'D:1:10',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))

    def test_CreateNewLaw_modif_lwp_from_block(self):
        lawart = mixer.blend(
            LawArticle,
            id=10,
            title="test",
            text_law="C'est un test",
            block_id=1,
            law_code_id=1,
            is_lwp=True,
        )
        response = self.client.get(
            self.path,
            {
                'slug': 'E:1:10',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))

    def test_CreateNewLaw_modif_lwp_does_not_exist(self):
        response = self.client.get(
            self.path,
            {
                'slug': 'E:1:12',
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @classmethod
    def tearDownClass(cls):
        super(TestCreateNewLawView, cls).tearDownClass()


@pytest.mark.django_db
class TestValidNewLawView(TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestValidNewLawView, cls).setUpClass()
        cls.path = reverse('ValidNewLaw')
        cls.user = mixer.blend(
            CYL_user,
            end_of_sanction=None
        )
        cls.client = Client()
        cls.lawcode = mixer.blend(LawCode, id=1)
        cls.codebloc1 = mixer.blend(
            CodeBlock,
            rank=1,
            id=1,
            law_code=cls.lawcode,
        )
        cls.codebloc2 = mixer.blend(
            CodeBlock,
            rank=2,
            id=2,
            law_code=cls.lawcode,
            block=cls.codebloc1,
        )

    def setUp(self):
        self.client.force_login(self.user)

    def test_ValidNewLaw_GET_request(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_ValidNewLaw_unauthenticated_user(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_ValidNewLaw_User_Punished(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=datetime.now(tz=pytz.UTC) + timedelta(days=5),
        )
        client = Client()
        client.force_login(user)
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn(
            '/CommonsJustice/YouArePunished',
            response.url,
        )

    def test_ValidNewLaw_User_has_job(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=None
        )
        Task1 = mixer.blend(
            JusticeTask,
            worker=user,
            type_task=1,
        )
        client = Client()
        client.force_login(user)
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertEqual(content["message"], "user_has_job")

    def test_ValidNewLaw_request_is_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = ValidNewLaw(request)

    def test_ValidNewLaw_form_valid(self):
        for boxtype in ["D", "E"]:
            response = self.client.post(
                self.path,
                {
                    'title': ['TEST'],
                    'text_law': ['<p>un test</p>\r\n'],
                    'details_law': ['<p>,gh</p>\r\n'],
                    'box_id': ['1'],
                    'box_type': ['E'],
                    'typeform': ['lawf'],
                    'idform': [''],
                    'IsModif': [''],
                    'place': ['#content'],
                },
                **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'},
            )
            self.assertTemplateUsed("GetReflection.html")
            self.assertTrue(isinstance(response, JsonResponse))
            self.assertContains(response, "intro")
            self.assertContains(response, "content")
            self.assertContains(response, "id_ref")
            self.assertContains(response, "message")
            content = eval(response.content)
            self.assertEqual(content['typeform'], "lawf")

    def test_ValidNewLaw_container_type_not_valid(self):
        response = self.client.post(
            self.path,
            {
                'title': ['TEST'],
                'text_law': ['<p>un test</p>\r\n'],
                'details_law': ['<p>,gh</p>\r\n'],
                'box_id': ['1'],
                'box_type': ['G'],
                'typeform': ['lawf'],
                'idform': [''],
                'IsModif': [''],
                'place': ['#content'],
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'},
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_ValidNewLaw_container_id_not_valid(self):
        response = self.client.post(
            self.path,
            {
                'title': ['TEST'],
                'text_law': ['<p>un test</p>\r\n'],
                'details_law': ['<p>,gh</p>\r\n'],
                'box_id': ['59'],
                'box_type': ['D'],
                'typeform': ['lawf'],
                'idform': [''],
                'IsModif': [''],
                'place': ['#content'],
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'},
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_ValidNewLaw_isModif_lwp_does_not_exist(self):
        response = self.client.post(
            self.path,
            {
                'title': ['TEST'],
                'text_law': ['<p>un test</p>\r\n'],
                'details_law': ['<p>,gh</p>\r\n'],
                'box_id': ['1'],
                'box_type': ['E'],
                'typeform': ['lawf'],
                'idform': ['1'],
                'IsModif': ['True'],
                'place': ['#content'],
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'},
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_ValidNewLaw_isModif_lwp_works(self):
        originallwp = mixer.blend(
            LawArticle,
            is_lwp=True,
            id=1)
        response = self.client.post(
            self.path,
            {
                'title': ['TEST'],
                'text_law': ['<p>un test</p>\r\n'],
                'details_law': ['<p>,gh</p>\r\n'],
                'box_id': ['1'],
                'box_type': ['E'],
                'typeform': ['lawf'],
                'idform': ['1'],
                'IsModif': ['True'],
                'place': ['#content'],
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'},
        )
        self.assertTemplateUsed("GetReflection.html")
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "intro")
        self.assertContains(response, "content")
        self.assertContains(response, "id_ref")
        self.assertContains(response, "message")
        content = eval(response.content)
        self.assertEqual(content['typeform'], "lawf")

    @classmethod
    def tearDownClass(cls):
        super(TestValidNewLawView, cls).tearDownClass()
