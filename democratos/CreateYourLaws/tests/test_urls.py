from django.urls import reverse, resolve


class TestUrls:

    def test_UP_url(self):
        path = reverse('UP')
        assert resolve(path).view_name == "UP"

    def test_DOWN_url(self):
        path = reverse('DOWN')
        assert resolve(path).view_name == "DOWN"

    def test_InDatBox_url(self):
        path = reverse('InDatBox', kwargs={"box_type": 1, "box_id": 1})
        assert resolve(path).view_name == "InDatBox"

    def test_InDatBox_url2(self):
        path = reverse('InDatBox')
        assert resolve(path).view_name == "InDatBox"

    def test_home_url(self):
        path = reverse('home')
        assert resolve(path).view_name == "home"

    def test_reflection_url(self):
        path = reverse('reflection')
        assert resolve(path).view_name == "reflection"

    def test_Reflection_url(self):
        path = reverse('Reflection', kwargs={"typeref": "law", "id_ref": 1})
        assert resolve(path).view_name == "Reflection"

    def test_childcomments_url(self):
        path = reverse('childcomments')
        assert resolve(path).view_name == "childcomments"

    def test_CreateNewLaw_url(self):
        path = reverse('CreateNewLaw')
        assert resolve(path).view_name == "CreateNewLaw"

    def test_ValidNewLaw_url(self):
        path = reverse('ValidNewLaw')
        assert resolve(path).view_name == "ValidNewLaw"

    def test_DeleteReflection_url(self):
        path = reverse('DeleteReflection')
        assert resolve(path).view_name == "DeleteReflection"

    def test_ModifReflection_url(self):
        path = reverse('ModifReflection')
        assert resolve(path).view_name == "ModifReflection"

    def test_GetForm_url(self):
        path = reverse('GetForm')
        assert resolve(path).view_name == "GetForm"

    def test_getnewlawprops_url(self):
        path = reverse('getnewlawprops')
        assert resolve(path).view_name == "getnewlawprops"

    def test_postreflection_url(self):
        path = reverse('PostReflection')
        assert resolve(path).view_name == "PostReflection"

    def test_listref_url(self):
        path = reverse('listref', kwargs={"parent_type": "law",
                                          "parent_id": 1,
                                          "list_ref_type": "opp",
                                          })
        assert resolve(path).view_name == "listref"

    def test_ListRef_url(self):
        path = reverse('ListRef')
        assert resolve(path).view_name == "ListRef"
