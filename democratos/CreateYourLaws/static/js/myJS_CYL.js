function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

/*
The functions below will create a header with csrftoken
*/
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function DisplayHasJobToDoMessage(){
    var message = `
    <p><strong>Vous devez rendre une/des délibération(s) sur un/des confits 
    au sein de la communauté pour pouvoir continuer</strong></p>
    <p>En effet, cette plateforme fonctionne sur un système d'auto-modération avec
    un système de banissement temporaire et progressif des comportements inapropriés </p>
    <p> Veuillez consulter <a href="/Prez">la documentation</a> pour plus d'information à ce sujet</p>
    `
    $('<div></div>').appendTo('body')
                    .html('<div>'+message+'?</div>')
                    .dialog({
                        modal: true, title: "Appel à délibération", zIndex: 10000, autoOpen: true,
                        width: 'auto', resizable: false,
                        buttons: {
                            Délibérer: function () {
                                var url = "/CommonsJustice/TakeDecisions";
                                var slug = "GoCommonJustice";
                                $("nav").width("15%");
                                GoAjax(url,slug,true);
                                $(this).dialog("close");                               
                            },
                            Annuler: function () {
                                $(this).dialog("close");
                            }
                        },
                        close: function (event, ui) {
                            $(this).remove();
                        }
                    });
};

function GetRefGoAnchor(h, tab){
    console.log("dans GetRefGoAnchor ", tab);
    $("."+tab+"tab").tab('show')
    try{
        var top = document.getElementById(h);
        console.log("h");
        if (top==null){
            var top = document.getElementsByClassName(tab+"tab")[0];
        };
    }
    catch(e){
        var top = document.getElementsByClassName(tab+"tab")[0];
        console.log("tab");
    }
    console.log(top);
    $('html, body').animate({
            scrollTop: $(top).offset().top
    }, 1);
    console.log("End GetRefGoAnchor"); 
}


function ConfirmDialog(qorigin, title, message, data) {
    $('<div></div>').appendTo('body')
                    .html('<div>'+message+'?</div>')
                    .dialog({
                        modal: true, title: title, zIndex: 10000, autoOpen: true,
                        width: 'auto', resizable: false,
                        buttons: {
                            Oui: function () {
                                Confirmresult(qorigin, true, data);
                                $(this).dialog("close");                               
                            },
                            Annuler: function () {                                                                
                                Confirmresult(qorigin, false, data);
                                $(this).dialog("close");
                            }
                        },
                        close: function (event, ui) {
                            $(this).remove();
                        }
                    });
};


function GoRedirectJusticeTask(response){
    $("nav").html(response.nav);
    $("#main").html(response.main);
    $("#script2").html(response.script2);
    eval($("body").find("script"));
                    
}

function Confirmresult(qorigin, answer, data) {
    switch (qorigin){
        case "DelOwnRef":
            if (answer){
                $.ajax({
                    type: "POST",
                    url: '/CYL/DeleteReflection',
                    data: {'typeref': data[1] ,'idref': data[2] ,csrfmiddlewaretoken: csrftoken},
                    dataType: "json",
                    success: function(rs) {
                        alert(rs.message);
                        console.log(history.state)
                        if (history.state==null) {
                            location.reload();
                            console.log("Appel GetRefGoAnchor");
                            GetRefGoAnchor(data[1]+"td"+data[2], data[1]);
                        }
                        else{
                           GoAjax(history.state.url, history.state.slug, false, tab=data[1], anchor=data[1]+"td"+data[2]);
                        }
                    },
                    error: function(rs, e) {
                        alert(rs.responseText);
                    }
                });
            }
        //case
    }
}

function UpAndDown(tomodif,dontomodif,slug,url){
    $.ajax({
       type: "POST",
       url: url,
       data: {'slug': slug,csrfmiddlewaretoken: csrftoken},
       dataType: "json",
       success: function(response) {
            if (response.redirect){
                GoRedirectJusticeTask(data)
            }
            else{
                if (response.message != ""){
                    if(response.message=="user_has_job"){
                        DisplayHasJobToDoMessage();
                    }
                    else{
                        alert(response.message);
                    }
                };
                if(response.data != {}){
                    for (var key in response.data) {
                        var context = $(key)[0].getContext('2d');
                        context.clearRect(0, 0, context.width, context.height);
                        $(key).trigger('MakeMyDonuts', response.data[key]);
                    }
                };
                // $(tomodif).html(Math.round((parseFloat(response.approb))*100)/100);
                var context = $(dontomodif)[0].getContext('2d');
                context.clearRect(0, 0, context.width, context.height);
                $(dontomodif).trigger('MakeMyDonuts', [response.approb]);
            }
        },
        error: function(rs, e) {
               alert(rs.responseText);
        }
    });
}


function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
};


// ######################### functions ############################
function UpSizeDebate(idtomodif){  // See function CheckIf Resize(NewEl)bellow
    console.log(idtomodif);
    // A revoir ici: parent 1 ne marche pas. La suite fonctionne.
    var NewEl = $('#content').find(idtomodif);
    console.log("UpsizeDebate:     ",NewEl, NewEl.get(0).tagName);
    if (NewEl.get(0).tagName == "DIV"){
        console.log('yeah')
        CheckIfResize(NewEl);
    };
    NewEl.parents("td").each(function(){
        CheckIfResize(this);
        var daddy = $('#content').find(this).closest("td");
        console.log(daddy);
        CheckIfResize(daddy);
    });
}


function CheckIfResize(NewEl){
    if (NewEl != 0){
        try{
            var trash = NewEl.find("td")
        }
        catch(e){
            NewEl = $(NewEl.id)
        }
        console.log(NewEl);
        MaxWidthchildren = Math.max.apply(Math, NewEl.find("td, form").map(function(){ 
                //console.log($(this).attr("id"), $(this).width());
                return $(this).width(); 
            }).get())
        datwidth = NewEl.width()
        //console.log(NewEl.attr("id"), datwidth, MaxWidthchildren, NewEl.find("td"))
        if (datwidth - MaxWidthchildren < 20){
            NewEl.css('width',(MaxWidthchildren+50).toString()+'px');  // modifier le +50 pour la largeur (trouver pq + 50 et vérifier sur autre navigateurs & tailles écrans)
        };
    }
    else{
        console.log(NewEl);
    };
}

function Setabutform(buttype){
    //console.log('Setabutform in')
    $("#" + buttype + "form").css('display','none');
    $(".but" + buttype).click(function(event) {
        if ( $("#" + buttype + "form").css('display') == 'none' ){
            $("#" + buttype + "form").css('display','block');
        }
        else{
            $("#" + buttype + "form").css('display','none');
        }
    });
}


function Setallbutform(){
    Setabutform('opp');
    Setabutform('opn');
    Setabutform('exp');
    Setabutform('qst');
    Setabutform('prp');
    Setabutform('src');
}

function SetDonuts(){
    $("#content").find('.donut').each(function(){
        $(this).on('MakeMyDonuts', function(event, value) {
            var nameread = $(this).attr('name').replace(',','.')
            var value = (typeof value !== 'undefined') ? value : parseFloat(nameread);
            var ctx = $(this)[0].getContext('2d');
            var donvalue = Math.round(((parseFloat(value))+100)*5)/10;
            var data = {
                datasets: [{
                    data: [donvalue, 100-donvalue,],
                    backgroundColor: [
                        "#87de87",
                        "#de8787",
                    ],
                    borderColor: [
                        "#74cd74",
                        "#cd7474",
                    ],
                }],
                labels: [
                    'pour',
                    'contre',
                ]
            };
            var options = {
                responsive: false,
                layout: {
                    padding: {
                        left: 15,
                        right: 15,
                        top: 5,
                        bottom: 5,
                    }
                },
                title: {
                    display: false,
                    position: "top",
                    text: "Aprobation",
                    fontSize: 18,
                    fontColor: "#111"
                },
                legend: {
                    display: false,
                    position: "right",
                    labels: {
                        fontColor: "#333",
                        fontSize: 16
                    }
                },
                aspectRatio: 1.25,
            };
            var donutChart = new Chart(ctx, {
                type: 'doughnut',
                data: data,
                options: options
            });
        }); 
        $(this).trigger('MakeMyDonuts');
    });
}

function SetNewForm(place){
    // Set and Enable all form at the indicated place (place as str)
    // console.log("SetNewForm called");
    $(place).find('form').each(function(){
        SetTheForm($(this).attr('id')) // joindre l'ID de la réflexion
        $(this).find('textarea').each(function() {
            InitNewCkeditor($(this));
        });
    });
};

function InitNewCkeditor(textarea){
    // enable a Ckeditor instance from a textarea object
    var t = document.getElementById(textarea.attr("id"));
    //console.log(t, typeof(t));
    if (t.getAttribute('data-processed') == '0' && t.id.indexOf('__prefix__') == -1) {
        //console.log('hey');
        t.setAttribute('data-processed', '1');
        var ext = JSON.parse(t.getAttribute('data-external-plugin-resources'));
        for (var j=0; j<ext.length; ++j) {
            CKEDITOR.plugins.addExternal(ext[j][0], ext[j][1], ext[j][2]);
        }
        CKEDITOR.replace(t.id, JSON.parse(t.getAttribute('data-config')));
    }
};

function SetTheForm(FormId){ // Il faut aussi joindre l'ID de la reflection auquel est attaché le form
    console.log("Set " + FormId);
    $('#'+FormId).submit(function(e){ // catch the form's submit event
        console.log(FormId + " submitted!");
        for ( instance in CKEDITOR.instances ) // recover data in CKeditor fields
            CKEDITOR.instances[instance].updateElement();
        var datatosend = $(this).serialize()
        var IsModif = this.IsModif.value;
        var typeform = $(this).attr('name');
        typeform = typeform.substring(4, 7);
        datatosend["csrfmiddlewaretoken"]=csrftoken;
        var place = "#" + this.closest(".UpSection").id;
        console.log(this, "place:",place);
        datatosend += '&place=' + place;
        $.ajax({ // create an AJAX call...
            data: datatosend, // get the form data
            type: $(this).attr('method'), // GET or POST
            url: $(this).attr('action'), // the file to call
            success: function(rs) { // on success..
                if (rs.redirect){
                    GoRedirectJusticeTask(data)
                }
                else{
                    if(rs.message=="form error"){
                        $(".alert-danger").parent().remove();
                        var errors = $.parseJSON(rs.errors);
                        $("#"+FormId).css('display','block')
                        for (var key in errors) {
                            var errormsg = '<div class="bs-component"><div class="alert alert-dismissible alert-danger">'+ errors[key]+"</div></div>";
                            $("#"+FormId).find("[id*='"+key+"']").first().before(errormsg);
                        }
                    }
                    else if(rs.message=="user_has_job"){
                        DisplayHasJobToDoMessage();
                    }
                    else{
                        if (rs.typeform == "lawf"){
                            $("#intro").html(rs.intro);
                            $("#content").html(rs.content);
                            eval($("#content").find("script").text());
                            Setallbutform();
                            url = "/CYL/Reflection";
                            slug = "law:" + rs.id_ref;
                            window.history.pushState({url: url,slug: slug, typeajax: "POST"}, null, url + "/" + rs.typeref + "/" + rs.id_ref);
                            window.histstate.lastref = true; 
                        }
                        else {
                            $(place).html('');
                            $(place).html(rs.NewSection);
                            var formtodel = "#" +  rs.typeref + 'askform' + rs.idref;
                            $('#content').find(formtodel).html('');
                            if ((typeform === "exp" || typeform ==="qst") && IsModif === ""){
                                UpSizeDebate(place);
                            }
                            console.log("Appel GetRefGoAnchor");
                            GetRefGoAnchor(place, typeform);
                        }
                        SetNewForm(place);
                        Setabutform(typeform);
                        SetDonuts();
                        if (rs.message != ""){
                            alert(rs.message);
                        }
                    }
                    console.log('endsucess setform')
                }
            },
            error: function(rs, e) {
               alert(rs.responseText);
            },
        });
        return false;
    });
    return false;
}

// #################  AJAX, Back and Forward ##########################


window.histstate = {pop:false, hash: false, lastref:false};

var popstatehaspoped = new Event("popstatehaspoped", {"bubbles":true, "cancelable":true});

window.addEventListener("popstate",function(e){
    e.target.histstate.pop = true;
    document.dispatchEvent(popstatehaspoped)
});

window.addEventListener("popstatehaspoped",function(e){
    popstatehaspoped.stopPropagation();
    setTimeout(isbackorforward, 200);
});

function isbackorforward(){
    console.log(window.histstate)
    if (window.histstate.pop){
        window.histstate.pop = false;
        try{
            GoAjax(history.state.url, history.state.slug, false, typeajax=history.state.typeajax);
        }
        catch(e){
            console.log(e);
            GoAjax('/', null, false);
        }
    }
}

function GoAjax(url, slug, push, typeajax="POST", tab="", anchor="") {
    console.log('in Goajax: '+ url, typeof(url));
    if (url.indexOf("/CYL/")!==-1 && $("#CYLtrees").length){
        var neednav = false;
    }
    else{
        var neednav = true;
    }
    console.log(neednav, window.location.href, url);
    if (url.indexOf("Reflection") > 0){
        url = url.replace("Reflection","reflection");
    }
    else if (url.indexOf("CommitDebate") > 0){
        url = url.replace("CommitDebate","commitdebate");
    }
    $.ajax({
        type: typeajax,
        url: url,
        data: {'slug': slug, csrfmiddlewaretoken: csrftoken},
        dataType: "json",
        success: function(response) {
            if(response.message=="user_has_job"){
                DisplayHasJobToDoMessage();
                return
            }
            console.log(slug, typeof(slug));
            switch(url){
                case "/CYL/CreateNewLaw":
                    $("#intro").html(response.intro);
                    $("#content").html(response.content);
                    //eval($("#content").find("script").text());
                    break
                default:
                    if (neednav){
                        $("nav").html(response.nav);
                        $("#script2").html(response.script2);
                        eval($("#script2").find("script"));
                    }
                    $("#main").html(response.main);
                    //eval($("main").find("script").text());
                    break;
            }
            if (url.substring(0, 5)=="/CYL/"){
                if (url !== "/CYL/InDatBox"){
                    SetNewForm("#content");
                    Setallbutform();
                    SetDonuts();
                }
            }   
            console.log("html loaded");
            $('html,body').scrollTop(0);
            // need a pushState if not Back or Forward
            if (push){
                switch(url){
                    case "/CYL/InDatBox":
                        window.history.pushState({url: url,slug: slug, typeajax: typeajax}, null, url + "/" + response.box_type + "/" + response.box_id);
                        window.histstate.lastref = true;
                        break;
                    case "/CYL/Reflection":
                    case "/CYL/reflection":
                        if (url.indexOf("reflection") > 0){
                            url = url.replace("reflection","Reflection");
                        }
                        window.history.pushState({url: url,slug: slug, typeajax: typeajax}, null, url + "/" + response.typeref + "/" + response.id_ref);
                        window.histstate.lastref = true; 
                        break;
                    case "/CYL/listref":
                    case "/CYL/ListRef":
                        if (url.indexOf("ListRef") > 0){
                            url = url.replace("ListRef","listref",);
                        }
                        window.history.pushState({url: url,slug: slug, typeajax: typeajax}, null, url + "/" + response.parent_type + "/" + response.parent_id + "/" + response.list_ref_type);
                        window.histstate.lastref = true; 
                        break;
                    case "/CYL/CreateNewLaw":
                        window.history.pushState({url: url,slug: slug, typeajax: typeajax}, null, url + "/" + response.box_id);
                        window.histstate.lastref = false;
                        break;
                    case "/CYL/":
                    case "/CommonsJustice/TakeDecisions":
                    case "/UserManager/profile":
                    case "/Expectations/":
                    case "/Prez/":
                        window.history.pushState({url: url,slug: slug, typeajax: typeajax}, null, url);
                        window.histstate.lastref = true;
                        break;
                    default:
                        console.log("GoAjax error")
                        break;
                    }
                }
            if(tab!="" && anchor!=""){
                GetRefGoAnchor(anchor, tab);
            }
            console.log('out GoAjax');
            },
        error: function(rs, e) {
        console.log("GoAjax error: "+e);
        alert(e, rs.responseText);
        }   
    });
}


// ##########################  MAIN   #############################


$(document).ready(function() {
    /////////////////// NAVBAR /////////////////////
    // ------------- Activate Navbar tabs -------------
    if (window.location.href.includes("/CYL/")){
        SetNewForm("body");
    }  
    $("#navbarColor01").find(".nav-link").on("click", function(){
       $("#navbarColor01").find(".active").removeClass("active");
       $(this).addClass("active");
    });
    $("#GoCYL").on("click",function(e){
        e.preventDefault();
        var url = "/CYL/";
        var slug = "GoCYL";
        $("nav").width('15%');
        GoAjax(url,slug,true);
    });
    $("#GoToPrez").on("click",function(e){
        e.preventDefault();
        var url = "/Prez/";
        var slug = "GoToPrez";
        $("nav").width("15%");
        GoAjax(url,slug,true,typeajax="GET");
    });
    $("#GoBudget").on("click", function(e){
        e.preventDefault();
        var url = "/Budget/";
        var slug = "GoBudget";
        $("nav").width("15%");
        GoAjax(url,slug,true);
    });
    $("#GoDefineExpectations").on("click", function(e){
        e.preventDefault();
        var url = "/Expectations/";
        var slug = "Expectations";
        $("nav").width("15%");
        GoAjax(url,slug,true,typeajax="GET");
    });
    $("#GoProfile").on("click", function(e){
        e.preventDefault();
        var url = "/UserManager/profile";
        var slug = "GoProfile";
        $("nav").width("15%");
        GoAjax(url,slug,true);
    });
    $("#GoCommonJustice").on("click",function(e){
        e.preventDefault();
        var url = "/CommonsJustice/TakeDecisions";
        var slug = "GoCommonJustice";
        $("nav").width("15%");
        GoAjax(url,slug,true);
    });
    //////////////////////////////////////////////////
    // -------------Displaying Forms for q,exp,op etc. -------------
    Setallbutform();
    // --------------- Set Donuts -----------------------
    SetDonuts();
    // --------------- up button -----------------------
    $('body').on('click','.UP',function(){
        var tomodif = '#' + $(this).attr('name').replace(':','');
        var dontomodif = '#don' + $(this).attr('name').replace(':','');   
        var slug = $(this).attr('name');     
        UpAndDown(tomodif,dontomodif,slug,'/CYL/UP');
    });

    // --------------- down button -----------------------
    $('body').on('click','.DOWN',function(){
        var tomodif = '#' + $(this).attr('name').replace(':','');
        var dontomodif = '#don' + $(this).attr('name').replace(':','');
        var slug = $(this).attr('name');
        UpAndDown(tomodif,dontomodif,slug,'/CYL/DOWN');
    });
    // --------------- ask for form -----------------------
    $('body').on('click','.butexp, .butqst',function(e){
        if ($(this).attr('name').substring(3,7) != "form"){
            $.ajax({
                type: "GET",
                url: '/CYL/GetForm',
                data: {'name': $(this).attr('name') ,csrfmiddlewaretoken: csrftoken},
                dataType: "json",
                success: function(response) {
                    if (response.redirect){
                        GoRedirectJusticeTask(response)
                    }
                    else if(response.message=="user_has_job"){
                        DisplayHasJobToDoMessage();
                    }
                    else{
                        var idtomodif = "#" +  response.typeref + 'askform' + response.idref;
                        $('#content').find(idtomodif).html(response.newform);
                        $(idtomodif).ready(function() {
                            SetNewForm(idtomodif);
                            UpSizeDebate(idtomodif)
                            UpSizeDebate($(idtomodif).closest("td"));;
                        });
                        //Setabutform("exp");    <-- A revoir (p-e numéroter les boutons)
                        //Setabutform("qst");
                    }
                },
                error: function(rs, e) {
                    alert(rs.responseText);
                }
            });
        };

    });
   
    // --------------- Checkbox suscribe -----------------------
    $('.suscribe').click(function(e){
        e.preventDefault();
        var checked = $(this).is(':checked');
        var idbox = $(this).attr('id').replace('check','');
        var typeref = idbox.substring(0,3);
        var ref_id = idbox.slice(3,idbox.length);
        $.ajax({
            type: "POST",
            url: '/CYL/checkbox',
            data: { check : checked, typeref : typeref, ref_id : ref_id, csrfmiddlewaretoken: csrftoken},
            dataType: "json",
            success: function(data) {
                if (checked) {
                    alert('Vous êtes abonné');
                }
                else{
                    alert('Vous êtes désabonné');
                }
            },
            error: function() {
                alert('it broke');
            },
        });
    });
   
    // --------------- Box loading AJAX -----------------------
    $("body").on("click",".InDatBox",function(e){
        e.preventDefault();
        var url = '/CYL/InDatBox';
        var slug = $(this).attr('name');
        GoAjax(url,slug,true, typeajax="GET");
    });

    // --------------- Reflection  loading AJAX -------------------
    $("body").on("click",".GetReflection",function(e){
        e.preventDefault();
        var url = '/CYL/reflection';
        var slug = $(this).attr('name');
        GoAjax(url,slug,true, typeajax="GET");
    });
    // --- Get List subreflections from a specific Reflection loading AJAX ------
    $("body").on("click",".GetListRef",function(e){
        e.preventDefault();
        var url = '/CYL/ListRef';
        var slug = $(this).attr('name');
        GoAjax(url,slug,true, typeajax="GET");
    });
    // --------------- CreateNewLaw  loading AJAX -------------------
    // modif url so url = '/CYL/CreateNewLaw/BOXID
    $("body").on("click",".CreateNewLaw",function(e){
        e.preventDefault();
        var slug = $(this).attr('name');
        var url = '/CYL/CreateNewLaw';
        GoAjax(url,slug,false, typeajax="GET");
    });
    $("body").on("click",".ModifNewLaw",function(){
        var slug = $(this).attr('name');
        var url = '/CYL/CreateNewLaw';
        GoAjax(url,slug,false, typeajax="GET");
    });
       //---------------------  Get Child comments---------------------
    $("body").on("click", ".GetDebateChild",function(){
        $.ajax({
            type: "GET",
            url: '/CYL/childcomments',
            data: {'slug': $(this).attr('name')},
            dataType: "json",
            success: function(response) {
                if (response.message != ""){
                    alert(response.message);
                }
                var idtomodif = "#child" +  response.typeref + response.idref;
                $('#content').find(idtomodif).replaceWith(response.newcomments);
                UpSizeDebate('#'+response.typeref+ 'debate' + response.idref);
                SetDonuts();
            },
            error: function(rs, e) {
                alert(rs.responseText);
            }
        });
    });
    //---------------------  Get New Laws Prop ---------------------
    $("body").on("click", ".GetNewLawProps",function(e){
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: '/CYL/getnewlawprops',
            data: {'slug': $(this).attr('id')},
            dataType: "json",
            success: function(response) {
                var idtomodif = "#" +  response.box_type + "NLPbox" + response.box_id;
                console.log(idtomodif);
                $('#content').find(idtomodif).replaceWith(response.listofNLP);
                console.log("getnewlawprops has end");
            },
            error: function(rs, e) {
                alert(rs.responseText);
            }
        });
    });
    //---------------------  Delete own ref---------------------
    /* 
    Del the selected td
    */
    $('body').on('click', '.DelOwnRef', function(){
        var name = $(this).attr('name').split(":");
        console.log(name);
        ConfirmDialog("DelOwnRef", "supprimer réflexion", 'Êtes vous sûr de vouloir supprimer cette réflexion?', name);
    });
    //---------------------  Modif own ref---------------------
    /* 
    Pop a form pre-filled with the current content in order to 
    modify a td and replace it directly via ajax in the current page
    */
    $('body').on('click', '.ModifRef', function(){
        var data =  $(this).attr('name').split(":");
        $.ajax({
            type: "POST",
            url: '/CYL/ModifReflection',
            data: {'typerequest': 'form', 'typeform': data[1] ,'idform': data[2], 'typeref': data[3] ,'idref': data[4] ,csrfmiddlewaretoken: csrftoken},
            dataType: "json",
            success: function(rs) {
                if (data.redirect){
                    GoRedirectJusticeTask(data)
                }
                else if(rs.message=="user_has_job"){
                    DisplayHasJobToDoMessage();
                }
                else{
                    eval($(document).find("script").text());
                    var idtomodif = "#" +  rs.typeform + 'td' + rs.idform;
                    var oldhtml = $('#content').find(idtomodif).html();
                    $('#content').find(idtomodif).html(rs.ModifForm);
                    $('#content').find("#Cancel" + rs.typeform + rs.idform + "form").each(function(){
                        $(this).click(function(){
                            $('#content').find(idtomodif).html(oldhtml);
                            SetDonuts();
                        });
                    });
                    SetNewForm(idtomodif);
                }
            },
            error: function(rs, e) {
                alert(rs.responseText);
            }
        });
    });
    //---------------------  Report own ref---------------------
    /* 
    Send a repport for abusive behavior or comment
    */
    $('body').on('click', '.ReportRef', function(){
        var title = "Signaler un abus";
        var data =  $(this).attr('name').split(":");
        var typeref = data[1]; 
        var idref = data[2];
        var repportid = ""
        $.get("/CommonsJustice/LoadRepportForm", function(data){
            if(data.template){
                var message = eval('`'+data.template+'`');
                var dialogbox = $('<div></div>').appendTo('body')
                    .html('<div>'+message+'</div>')
                    .dialog({
                        modal: true, title: title, zIndex: 10000, autoOpen: true,
                        width: 'auto', resizable: false,
                        close: function (event, ui) {
                            $(this).remove();
                        }
                    }); 
                dialogbox.find('textarea').each(function() {
                    InitNewCkeditor($(this));
                    var t = document.getElementById($(this).attr("id"));
                    var editor = CKEDITOR.instances[t.id];             
                    editor.config.resize_enabled =false;
                    editor.config.removePlugins = 'resize,autogrow';
                    editor.on('instanceReady', function(e){
                        editor.resize('100%',250);
                    });
                });
                $("#CancelDialog").on("click", function(){
                    dialogbox.remove()
                });
                dialogbox.find('form').each(function() {
                    $(this).on('submit',function(e){ // catch the form's submit event
                        for ( instance in CKEDITOR.instances ){// recover data in CKeditor fields
                            CKEDITOR.instances[instance].updateElement();
                        };
                        var datatosend = $(this).serialize();
                        datatosend["csrfmiddlewaretoken"]=csrftoken;
                        $.ajax({ // create an AJAX call...
                            data: datatosend, // get the form data
                            type: $(this).attr('method'), // GET or POST
                            url: $(this).attr('action'), // the file to call
                            beforeSend: function(){
                                dialogbox.remove()
                            },
                            success: function(rs) {
                                $('<div></div>').appendTo('body').html('<div>'+rs.message+'</div>').dialog({
                                    modal: true, 
                                    title: "confirmation signalement", 
                                    zIndex: 10000, 
                                    autoOpen: true,
                                    width: 'auto', 
                                    resizable: false,
                                    close: function (event, ui) {
                                        $(this).remove();
                                    },
                                    buttons: [
                                        {
                                            text: "OK",
                                            click: function() {
                                                $( this ).dialog( "close" );
                                          }
                                        }
                                      ],
                                }); 
                            },
                            error: function(rs, e) {
                               alert(rs.responseText);
                            },
                        });
                    });
                });
            }
            else{
                console.log(data);
                if (data.redirect){
                    GoRedirectJusticeTask(data)
                }
                else if(data.message=="user_has_job"){
                    DisplayHasJobToDoMessage();
                }
                else{
                    alert("Error");
                    console.log("LoadForm error");
                }
            }
        })
    });

    console.log("doc ready");
});

