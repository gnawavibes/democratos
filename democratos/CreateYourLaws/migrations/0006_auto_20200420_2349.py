# Generated by Django 3.0.5 on 2020-04-20 23:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('CreateYourLaws', '0005_auto_20190311_1131'),
    ]

    operations = [
        migrations.RenameField(
            model_name='question',
            old_name='text_q',
            new_name='text_qst',
        ),
    ]
