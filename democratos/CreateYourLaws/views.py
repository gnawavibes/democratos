# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.http import Http404, JsonResponse, HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.decorators.http import require_POST, require_GET
# from django.views.decorators.csrf import ensure_csrf_cookie
from render_block import render_block_to_string
import operator
from bs4 import BeautifulSoup as BS4
from CommonsJustice.decorators import user_has_no_job_to_do
from CreateYourLaws.view_functions.nav_jstree import up_nav, init_nav
from CreateYourLaws.models import (
    LawCode, LawArticle, CodeBlock, Question, Negopinion,
    Explaination, Posopinion, Proposition, Note, SourceLink)
from CreateYourLaws.forms import (
    QuestionForm, PropositionForm, ExplainationForm, PosopinionForm,
    NegopinionForm, CreateNewLawForm, ModifPropositionForm,
    SourceLinkForm)
from CreateYourLaws.views_functions import (
    get_the_instance, get_box_parents, get_ref_parents, CleanCkeditor)
from DLcodes.functions import get_something
from CommitApp.views import (
    create_commit,
)
from CommonsJustice.models import (
    JusticeTask,
)
from democratos.decorators import ajax_login_required
import json
from UserManager.models import CYL_user
from django.utils import timezone
from django_user_agents.utils import get_user_agent
from democratos import log


def home(request, loadall=True):
    """
    Exemple de page HTML, non valide pour que 
    l'exemple soit concis 
    """
    qs = LawCode.objects.all().order_by("id")
    lqs = list(qs)
    user_agent = get_user_agent(request)
    print("mobile ?", user_agent.is_mobile)
    if request.method == "POST":
        # print(dict(locals()))
        if loadall:
            main = render_block_to_string('home.html',
                                          "Main",
                                          locals())
            nav = render_block_to_string('home.html',
                                         "nav",
                                         locals())
            script2 = render_block_to_string('home.html',
                                             "script2",
                                             locals())
            ctx = {'nav': nav,
                   'main': main,
                   'script2': script2, }
        else:
            intro = render_block_to_string('home.html',
                                           "intro",
                                           locals())
            content = render_block_to_string('home.html',
                                             "content",
                                             locals())
            ctx = {'intro': intro,
                   'content': content,
                   }
        return JsonResponse(ctx)
    else:
        try:
            user = request.user
        except:
            log.info("user not connected.")
        template = render_to_string('home.html', locals())
        index = template.find('id="GoCYL"')
        template = f"{template[:index-2]} active{template[index-2:]}"
        return HttpResponse(template)


# ################################ UP and DOWN ################################

@login_required
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
@require_POST
def UP(request):
    user = request.user
    slug = request.POST.get('slug', None)
    typ, Id = slug.split(sep=":")
    try:
        obj = get_the_instance(typ, Id)
        assert obj is not None
    except:
        raise Http404
    ct = ContentType.objects.get_for_model(obj)
    # -------------------------------------------------------------
    # About proposition and user must have 1 position:
    # Impossible to approve a Law and a counter-proposition about the Law
    # or 2 counter-propositions about the same law
    # -------------------------------------------------------------
    data = {}
    if isinstance(obj, Proposition):
        try:
            note = Note.objects.get(user=user,
                                    content_type=ct,
                                    object_id=obj.id)
            if note.approve:
                getit = True
            else:
                getit = False
        except Exception:
            getit = False
        listprop = list(Proposition.objects.filter(
            law_article=obj.law_article,
        ))
        for x in listprop:
            data['#donprp' + str(x.id)] = str(x.approval_ratio)
            x.notes.filter(user=user, approve=True).delete()
        lart = obj.law_article
        data['#donlaw' + str(lart.id)] = str(lart.approval_ratio)
        lart.notes.filter(user=user, approve=True).delete()
    elif isinstance(obj, LawArticle):
        getit = False
        listprop = list(Proposition.objects.filter(
            law_article=obj))
        for x in listprop:
            data['#donprp' + str(x.id)] = str(x.approval_ratio)
            x.notes.filter(user=user, approve=True).delete()
    else:
        getit = False
    # -------------------------------------------------------------
    note, created = Note.objects.get_or_create(user=user,
                                               content_type=ct,
                                               object_id=obj.id)
    if (created is False and note.approve) or getit:
        message = "Vous approuvez déjà cette réflexion."\
            + "\nVous ne pouvez approuver ou"\
            + " désapprouver qu'une seule fois une réflexion."\
            + "\nVous pouvez Cependant changer "\
            + "d'avis autant de fois que vous voulez."
    else:
        message = ""
    note.approve = True
    note.save()
    obj = get_the_instance(typ, Id)
    ctx = {'message': message,
           'approb': str(obj.approval_ratio),
           'data': data}
    return JsonResponse(ctx)


@login_required
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
@require_POST
def DOWN(request):
    user = request.user
    slug = request.POST.get('slug', None)
    typ, Id = slug.split(sep=":")
    try:
        obj = get_the_instance(typ, Id)
    except:
        raise Http404
    ct = ContentType.objects.get_for_model(obj)
    note, created = Note.objects.get_or_create(user=user,
                                               content_type=ct,
                                               object_id=obj.id)
    if created is False and note.approve is False:
        message = "Vous désapprouvez déjà cette réflexion."\
            + "\nVous ne pouvez approuver ou"\
            + " désapprouver qu'une seule fois une réflexion."\
            + "\nVous pouvez Cependant changer "\
            + "d'avis autant de fois que vous voulez."
    else:
        message = ""
    note.approve = False
    note.save()
    obj = get_the_instance(typ, Id)
    ctx = {'message': message, 'approb': str(obj.approval_ratio)}
    return JsonResponse(ctx)


# ############################# Interaction  ##################################

@require_GET
def In_dat_box(request, box_type=None, box_id=None):
    """ List the blocks or articles contained in a law code or box """
    user = request.user
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        slug = request.GET.get('slug', None)
        box_type, box_id = slug.split(sep=":")
        box_id = int(box_id)
    else:
        box_type = str(box_type)
    if box_type == '1':
        lqs = list(
            LawArticle.objects.filter(law_code=box_id,
                                      is_lwp=False,
                                      block_id__isnull=True).order_by('id'))
        lqs += list(
            CodeBlock.objects.filter(rank=1,
                                     law_code=box_id,
                                     is_cbp=False,
                                     ).order_by('id'))
        HasLawProp = LawArticle.objects.filter(law_code=box_id,
                                               is_lwp=True,
                                               block_id__isnull=True).exists()
        HasBlocProp = CodeBlock.objects.filter(rank=1,
                                               law_code=box_id,
                                               is_cbp=True,
                                               ).exists()
        Box = LawCode.objects.get(id=box_id)
        listparents = []
    elif box_type == '2':
        lqs = list(
            LawArticle.objects.filter(block=box_id,
                                      is_lwp=False).order_by('id'))
        lqs += list(
            CodeBlock.objects.filter(block=box_id,
                                     is_cbp=False).order_by('id'))
        Box = CodeBlock.objects.get(id=box_id)
        listparents = get_box_parents(Box)
        HasLawProp = LawArticle.objects.filter(block=box_id,
                                               is_lwp=True).exists()
        HasBlocProp = CodeBlock.objects.filter(block=box_id,
                                               is_cbp=True).exists()
    else:
        raise Http404
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        main = render_block_to_string('InDatBox.html',
                                      "Main",
                                      locals())
        nav = render_block_to_string('InDatBox.html',
                                     "nav",
                                     locals())
        ctx = {'main': main,
               'nav': nav,
               'box_type': str(box_type),
               'box_id': str(box_id),
               }

        return JsonResponse(ctx)
    else:
        template = render_to_string('InDatBox.html', locals())
        index = template.find('id="GoCYL"')
        template = f"{template[:index-2]} active{template[index-2:]}"
        return HttpResponse(template)

###########################################################
#             WORK IN PROGRESS: NOT USED YET              #
###########################################################


@require_GET
def getnewlawprops(request):
    """ NOT USED YET
    Return the new law proposition for a specific block"""
    slug = request.GET.get('slug', None)
    box_type = slug[0]
    box_id = int(slug[2:])
    if box_type == 'D':
        lqs = list(
            LawArticle.objects.filter(law_code=box_id,
                                      is_lwp=True,
                                      block_id__isnull=True).order_by('id'))
    elif box_type == 'E':
        lqs = list(
            LawArticle.objects.filter(block=box_id,
                                      is_lwp=True).order_by('id'))
    else:
        raise Http404
    listofNLP = render_block_to_string('InDatBox.html',
                                       "getlist",
                                       locals())
    ctx = {'listofNLP': listofNLP,
           'box_type': str(box_type),
           'box_id': str(box_id)
           }
    return JsonResponse(ctx)

###########################################################
###########################################################
###########################################################


@require_GET
def get_reflection(request, typeref=None, id_ref=None):
    """ View which display a reflection and its child
    reflections from its ID"""
    # Does the reflection extist?
    log.debug("GetReflection")
    user = request.user
    log.debug(("user: ", user, type(user), user.is_authenticated))
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        slug = request.GET.get('slug', None)
        if slug is None:
            typeref = request.typeref
            id_ref = int(request.id_ref)
        else:
            typeref, id_ref = slug.split(sep=":")
            id_ref = int(id_ref)
    log.debug(("typeref", typeref))
    try:
        ref = get_the_instance(typeref, id_ref)
    except Exception:
        raise Http404
    # where is it from? path to the reflection
    law_code, listparents, fstparent = get_ref_parents(ref, typeref)
    # forms initializations
    qstform = QuestionForm()
    expform = ExplainationForm()
    oppform = PosopinionForm()
    opnform = NegopinionForm()
    prpform = PropositionForm()
    srcform = SourceLinkForm()
    log.debug("forms loaded")
    # load all the disclaims, other proposions, opinions, comments and
    # questions about the reflection
    listsources = list(ref.sources.filter(is_censured=False))
    listexplainations = list(ref.explainations.filter(is_censured=False))
    listquestions = list(ref.questions.filter(is_censured=False))
    listcom = listexplainations
    listcom.extend(listquestions)
    listcom = sorted(listcom, key=operator.attrgetter('approval_factor'))
    listcom.reverse()
    if typeref == 'law' or typeref == 'prp':
        listposop = list(ref.posopinions.filter(is_censured=False))
        listnegop = list(ref.negopinions.filter(is_censured=False))
        if typeref == 'prp':
            listpropositions = list(
                ref.law_article.propositions.filter(is_censured=False))
        else:
            listpropositions = list(ref.propositions.filter(is_censured=False))
    log.debug('Lists child reflection loaded')
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        main = render_block_to_string('GetReflection.html',
                                      "Main",
                                      locals())
        nav = render_block_to_string('GetReflection.html',
                                     "nav",
                                     locals())
        ctx = {'main': main,
               'nav': nav,
               'typeref': typeref,
               'id_ref': str(id_ref)}
        log.debug("sucess Ajax load")
        return JsonResponse(ctx)
    else:
        log.debug("sucess stdt load")
        template = render_to_string('GetReflection.html', locals())
        index = template.find('id="GoCYL"')
        template = f"{template[:index-2]} active{template[index-2:]}"
        return HttpResponse(template)


@ajax_login_required
@require_POST
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
def PostReflection(request):  # Trouver un moyen d'avoir ID_ref
    typeform = request.POST.get('typeform', '')
    typeref = request.POST.get('typeref', '')
    place = request.POST.get('place', '')
    id_ref = int(request.POST.get('ref_id', None))
    IsModif = bool(request.POST.get('IsModif', False))
    print("place: ",
          place,
          "\ntypeform: ",
          typeform,
          "\ntyperef: ",
          typeref,
          "\nIsModif: ",
          IsModif)
    if IsModif:
        idform = int(request.POST.get('idform', None))
    user = request.user
    try:
        ref = get_the_instance(typeref, id_ref)
    except:
        raise Http404
    fstparent = [typeref,
                 ref.id,
                 ref.title,
                 ]
    id_ref = str(id_ref)
    # ####################  PropositionForm ###########################
    if typeform == 'prpf':
        print("c'est un prpf")
        prpform = PropositionForm(request.POST)
        if prpform.is_valid():
            proptitle = prpform.cleaned_data['title']
            prop = CleanCkeditor(prpform.cleaned_data['text_prp'])
            details_prp = CleanCkeditor(prpform.cleaned_data['details_prp'])
            print(proptitle, prop, details_prp)
            if isinstance(ref, LawArticle):
                lawart = ref
            else:
                lawart = ref.law_article
            if IsModif:
                comments = CleanCkeditor(request.POST.get('commit_com', ''))
                prp = Proposition.objects.get(id=idform)
                create_commit(
                    prp,
                    prop,
                    proptitle,
                    details_prp,
                    comments)
                prp.text_prp = prop
                prp.title = proptitle
                prp.details_prp = details_prp
                prp.save()
                listpropositions = list(
                    ref.propositions.filter(is_censured=False))
            else:
                if isinstance(ref, Proposition):
                    ctob = ref.law_article  # all prp must point on a law
                else:
                    ctob = ref
                prp = Proposition.objects.create(text_prp=prop,
                                                 title=proptitle,
                                                 author=user,
                                                 details_prp=details_prp,
                                                 law_article=lawart,
                                                 content_object=ctob)
                prp.save()
                listpropositions = list(
                    ctob.propositions.filter(is_censured=False))
            prpform = PropositionForm()
            NewSection = render_block_to_string('GetReflection.html',
                                                'content',
                                                locals())
            soup = BS4(NewSection, "html.parser")
            soup.prettify()
            iddiv = f"{typeref}propsection{str(id_ref)}"
            NewSection = soup.find("div", id=iddiv)
            NewSection = "".join(str(e) for e in NewSection.contents)
            ctx = {'NewSection': NewSection,
                   'section_type': "prp",
                   'tdid': ""}
        else:
            ctx = {'message': "form error",
                   'errors': json.dumps(prpform.errors),
                   }

    # ####################  OpinionForm #########################
    #      <---- Revoir si séparer Posop et Negop

    elif typeform == 'oppf':
        oppform = PosopinionForm(request.POST)
        if oppform.is_valid():
            optitle = oppform.cleaned_data['title']
            opin = CleanCkeditor(oppform.cleaned_data['text_opp'])
            if IsModif:
                opp = Posopinion.objects.get(id=idform)
                opp.text_opp = opin
                opp.title = optitle
            else:
                opp = Posopinion.objects.create(text_opp=opin,
                                                title=optitle,
                                                author=user,
                                                content_object=ref)
            opp.save()
            listposop = list(ref.posopinions.filter(is_censured=False))
            oppform = PosopinionForm()
            NewSection = render_block_to_string('GetReflection.html',
                                                'content',
                                                locals())
            soup = BS4(NewSection, "html.parser")
            soup.prettify()
            iddiv = "posopsection"
            NewSection = soup.find("article", id=iddiv)
            NewSection = "".join(str(e) for e in NewSection.contents)
            ctx = {'NewSection': NewSection,
                   'section_type': "opp",
                   'tdid': ""}
        else:
            ctx = {'message': "form error",
                   'errors': json.dumps(oppform.errors),
                   }

    elif typeform == 'opnf':
        opnform = NegopinionForm(request.POST)
        if opnform.is_valid():
            optitle = opnform.cleaned_data['title']
            opin = CleanCkeditor(opnform.cleaned_data['text_opn'])
            if IsModif:
                opn = Negopinion.objects.get(id=idform)
                opn.text_opn = opin
                opn.title = optitle
            else:
                opn = Negopinion.objects.create(text_opn=opin,
                                                title=optitle,
                                                author=user,
                                                content_object=ref)
            opn.save()
            listnegop = list(ref.negopinions.filter(is_censured=False))
            NewSection = render_block_to_string('GetReflection.html',
                                                'content',
                                                locals())
            soup = BS4(NewSection, "html.parser")
            soup.prettify()
            iddiv = "negopsection"
            NewSection = soup.find("article", id=iddiv)
            NewSection = "".join(str(e) for e in NewSection.contents)
            ctx = {'NewSection': NewSection,
                   'section_type': "opn",
                   'tdid': ""}
        else:
            ctx = {'message': "form error",
                   'errors': json.dumps(opnform.errors),
                   }

    # ####################  SourceForm ###########################
    elif typeform == 'srcf':
        srcform = SourceLinkForm(request.POST)
        if srcform.is_valid():
            srctitle = srcform.cleaned_data['title']
            src_url = srcform.cleaned_data['url']
            description = CleanCkeditor(srcform.cleaned_data['description'])
            if IsModif:
                src = SourceLink.objects.get(id=idform)
                src.description = description
                src.url = src_url
                src.title = srctitle
            else:
                src = SourceLink.objects.create(description=description,
                                                title=srctitle,
                                                author=user,
                                                url=src_url,
                                                content_object=ref)
            src.save()
            listsources = list(ref.sources.filter(is_censured=False))
            NewSection = render_block_to_string('GetReflection.html',
                                                'content',
                                                locals())
            soup = BS4(NewSection, "html.parser")
            soup.prettify()
            iddiv = f"{typeref}SourceLinks{str(id_ref)}"
            NewSection = soup.find("div", id=iddiv)
            NewSection = "".join(str(e) for e in NewSection.contents)
            ctx = {'NewSection': NewSection,
                   'section_type': "src",
                   'tdid': ""}
        else:
            ctx = {'message': "form error",
                   'errors': json.dumps(srcform.errors),
                   }

    # ####################  QuestionForm ###########################
    elif (typeform == 'qstf' or typeform == 'expf'):
        if typeform == 'expf':
            form = ExplainationForm(request.POST)
            if form.is_valid():
                exptitle = form.cleaned_data['title']
                explain = CleanCkeditor(form.cleaned_data['text_exp'])
                if IsModif:
                    exp = Explaination.objects.get(id=idform)
                    exp.text_exp = explain
                    exp.title = exptitle
                else:
                    exp = Explaination.objects.create(title=exptitle,
                                                      text_exp=explain,
                                                      author=user,
                                                      content_object=ref)
                exp.save()
        elif typeform == 'qstf':
            form = QuestionForm(request.POST)
            if form.is_valid():
                qtitle = form.cleaned_data['title']
                question = CleanCkeditor(form.cleaned_data['text_qst'])
                if IsModif:
                    q = Question.objects.get(id=idform)
                    q.text_qst = question
                    q.title = qtitle
                else:
                    q = Question.objects.create(text_qst=question,
                                                title=qtitle,
                                                author=user,
                                                content_object=ref)
                q.save()
        if form.is_valid():
            listsources = list(ref.sources.filter(is_censured=False))
            listexplainations = list(
                ref.explainations.filter(is_censured=False))
            listquestions = list(ref.questions.filter(is_censured=False))
            listcom = listexplainations
            listcom.extend(listquestions)
            listcom = sorted(listcom,
                             key=operator.attrgetter('approval_factor'))
            expform = ExplainationForm()
            qstform = QuestionForm()
            NewSection = render_block_to_string('GetReflection.html',
                                                'content',
                                                locals())
            soup = BS4(NewSection, "html.parser")
            soup.prettify()
            iddiv = typeref + 'debate' + str(id_ref)
            NewSection = soup.find("div", id=iddiv)
            ctx = {'NewSection': str(NewSection),
                   'section_type': typeform[0:3],
                   'typeform': typeform,
                   'typeref': typeref,
                   'tdid': str(id_ref)}
        else:
            ctx = {'message': "form error",
                   'errors': json.dumps(form.errors),
                   }
    else:
        log.error("FORM NON VALIDE. ERREURE")
        raise Http404
    if not "message" in ctx:
        if IsModif:
            ctx["message"] = "Votre réflection a bien été modifiée!"
        else:
            ctx["message"] = ""
    log.debug("end PostReflection")
    return JsonResponse(ctx)


@require_GET
def list_of_reflections(request,
                        parent_type=None,
                        parent_id=None,
                        list_ref_type=None):
    """ display the list of given reflection type from the parent obj.
    ex: list of the questions asked about law article X."""
    print(request, parent_type, parent_id, list_ref_type)
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        slug = request.GET.get('slug', None)
        if slug is None:
            parent_type = request.parent_type
            parent_id = int(request.parent_id)
            list_ref_type = request.list_ref_type
        else:
            parent_type, parent_id, list_ref_type = slug.split(sep=":")
            parent_id = int(parent_id)
    parent = get_the_instance(parent_type, parent_id)
    law_code, listparents, fstparent = get_ref_parents(parent, parent_type)
    if list_ref_type == 'qst':
        list_to_display = list(parent.questions.filter(is_censured=False))
    elif list_ref_type == 'exp':
        list_to_display = list(parent.explainations.filter(is_censured=False))
    elif list_ref_type == 'opp':
        list_to_display = list(parent.posopinions.filter(is_censured=False))
    elif list_ref_type == 'opn':
        list_to_display = list(parent.negopinions.filter(is_censured=False))
    elif list_ref_type == 'prp':
        list_to_display = list(parent.propositions.filter(is_censured=False))
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        main = render_block_to_string('displaylist.html',
                                      "Main",
                                      locals())
        nav = render_block_to_string('displaylist.html',
                                     "nav",
                                     locals())
        ctx = {'main': main,
               'nav': nav,
               'parent_type': parent_type,
               'parent_id': str(parent_id),
               'list_ref_type': list_ref_type}
        print("sucess Ajax load")
        return JsonResponse(ctx)
    else:
        template = render_to_string('displaylist.html', locals())
        index = template.find('id="GoCYL"')
        template = f"{template[:index-2]} active{template[index-2:]}"
        return HttpResponse(template)


@require_GET
def childcomments(request):
    """ View which display a reflection and its child
    reflections from its ID and typeref"""
    if not request.headers.get('x-requested-with') == 'XMLHttpRequest':
        raise Http404
    slug = request.GET.get('slug', None)
    typeref = slug[5:8]
    id_ref = slug[8:len(slug)]
    id_ref = int(id_ref)
    user = request.user
    log.debug("children from", typeref, id_ref)
    message = ""
    if typeref not in ['qst', 'exp']:
        raise Http404
    try:
        if typeref == 'qst':   # Does the reflection extist?
            ref = Question.objects.get(id=id_ref)
        elif typeref == 'exp':
            ref = Explaination.objects.get(id=id_ref)
        fstparent = [typeref,
                     ref.id,
                     ref.title,
                     ]
        listexplainations = list(ref.explainations.filter(is_censured=False))
        listquestions = list(ref.questions.filter(is_censured=False))
        listcom = listexplainations
        listcom.extend(listquestions)
        assert listcom
        listcom = sorted(listcom, key=operator.attrgetter('approval_factor'))
        listcom.reverse()
        NewSection = render_block_to_string('GetReflection.html',
                                            'content',
                                            locals())
        NewSection = render_block_to_string('GetReflection.html',
                                            'content',
                                            locals())
        soup = BS4(NewSection, "html.parser")
        soup.prettify()
        iddiv = typeref + 'debate' + str(id_ref)
        NewSection = soup.find("div", id=iddiv)
        # NewSection = "".join(str(e) for e in NewSection.contents)
        ctx = {'message': message,
               'newcomments': str(NewSection),
               'typeref': typeref,
               "idref": str(id_ref)
               }
    except Exception:
        message = "comment unfindable in DB"
        ctx = {'message': message,
               'newcomments': "",
               'typeref': typeref,
               "idref": str(id_ref)
               }
    return JsonResponse(ctx)


@ajax_login_required
@require_GET
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
def GetForm(request):
    """ Get the question or explaination form in a reflection
    page """
    if not request.headers.get('x-requested-with') == 'XMLHttpRequest':
        return None
    modifForm = False
    name = request.GET.get('name', None)
    typeref, typeform, idref = name.split(sep=":")
    if (typeref not in [
            "law", "qst", "exp", "opp", "opn", "prp"]):
        raise Http404
    if typeform == 'qst':   # which form to load?
        form = QuestionForm()
    elif typeform == 'exp':
        form = ExplainationForm()
    else:
        raise Http404
    NewForm = render_block_to_string('GetForm.html',
                                     "content",
                                     locals())
    ctx = {'newform': NewForm, 'typeref': typeref, "idref": idref}
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
def ModifReflection(request):
    """ Load the form for an Author to
    modify his own reflection content once posted"""
    if not request.headers.get('x-requested-with') == 'XMLHttpRequest':
        return None
    if not(request.POST.get('typerequest', None) == 'form'):
        raise Http404
    IsModif = True
    typeform = request.POST.get('typeform', None)
    idform = int(request.POST.get('idform', None))
    typeref = request.POST.get('typeref', None)
    idref = int(request.POST.get('idref', None))
    try:
        obj = get_the_instance(typeform, idform)
        # ref just for check it exist
        ref = get_the_instance(typeref, idref)
    except:
        log.error((
            'Unknow typeform or typeref instance. typeform:',
            typeform,
            "  idform:",
            idform,
            ' typeref:',
            typeref,
            "  idref:",
            idref,))
        raise Http404
    if typeform == 'qst':
        form = QuestionForm(initial={'title': obj.title,
                                     'text_qst': obj.text_qst
                                     })
    elif typeform == 'exp':
        form = ExplainationForm(initial={'title': obj.title,
                                         'text_exp': obj.text_exp
                                         })
    elif typeform == 'opp':
        form = PosopinionForm(initial={'title': obj.title,
                                       'text_opp': obj.text_opp
                                       })
    elif typeform == 'opn':
        form = NegopinionForm(initial={'title': obj.title,
                                       'text_opn': obj.text_opn
                                       })
    elif typeform == 'src':
        form = SourceLinkForm(initial={
            'url': obj.url,
            'title': obj.title,
            'description': obj.description,
        })
    elif typeform == 'prp':
        form = ModifPropositionForm(initial={
            'title': obj.title,
            'text_prp': obj.text_prp,
            'details_prp': obj.details_prp,
        })
    formhtml = render_block_to_string('GetForm.html',
                                      'content',
                                      locals())
    ctx = {'ModifForm': formhtml,
           'typeform': typeform,
           'typeref': typeref,
           'idref': idref,
           'idform': idform,
           }
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
def DeleteReflection(request):
    """ Enable the Author or the comunity to delete a comment """
    if not request.headers.get('x-requested-with') == 'XMLHttpRequest':
        return None
    typeref = request.POST.get('typeref', None)
    idref = int(request.POST.get('idref', None))
    try:
        ref = get_the_instance(typeref, idref)
    except:
        log.error((
            'Unknown typeref instance. typeref:',
            typeref,
            "  idref:",
            idref))
        raise Http404
    ref.delete()
    ctx = {'message': 'Votre commentaire a bien été supprimé'}
    return JsonResponse(ctx)


@require_GET
@ajax_login_required
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
def CreateNewLaw(request, box_type=None, box_id=None):
    """ View to ask form to create a new law article """
    log.debug(("CreateNewLaw: ", request.GET.get('slug', None)))
    box = request.GET.get('slug', None).split(":")
    try:
        idform = int(box[2])
        IsModif = True
    except:
        IsModif = False
    box_type = box[0]
    box_id = int(box[1])
    try:
        if box_type == 'D':
            box = LawCode.objects.get(id=box_id)
        elif box_type == "E":
            box = CodeBlock.objects.get(id=box_id)
            listparents = get_box_parents(box)
            listparents.append((box.title, box.id, 1))
        else:
            raise Http404
    except:
        raise Http404
    typeform = "lawf"
    form = CreateNewLawForm()
    if IsModif:
        try:
            lwptomodif = LawArticle.objects.get(id=idform)
        except:
            raise Http404
        form = CreateNewLawForm(instance=lwptomodif)
    else:
        form = CreateNewLawForm()
    intro = render_block_to_string('GetForm.html',
                                   "intro",
                                   locals())
    content = render_block_to_string('GetForm.html',
                                     "content",
                                     locals())
    ctx = {'intro': intro,
           'content': content,
           'box_type': box_type,
           'box_id': str(box_id)}
    return JsonResponse(ctx)


@require_POST
@ajax_login_required
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
def ValidNewLaw(request):
    user = request.user
    typeref = "law"
    lawform = CreateNewLawForm(request.POST)
    if lawform.is_valid():
        lawtitle = lawform.cleaned_data['title']
        law_text = CleanCkeditor(lawform.cleaned_data['text_law'])
        law_details = CleanCkeditor(lawform.cleaned_data['details_law'])
        IsModif = bool(request.POST.get('IsModif', False))
        if IsModif:
            idform = int(request.POST.get('idform', None))
            try:
                ref = LawArticle.objects.get(id=idform)
            except:
                raise Http404
            ref.text_law = law_text
            ref.title = lawtitle
            ref.details_law = law_details
            ref.notes.all().delete()
        else:
            boxid = int(request.POST.get('box_id', None))
            boxtype = request.POST.get('box_type', None)
            try:
                if boxtype == 'D':
                    box = None
                    Law_code = LawCode.objects.get(id=boxid)
                elif boxtype == 'E':
                    box = CodeBlock.objects.get(id=boxid)
                    Law_code = box.law_code
                else:
                    raise ValueError("boxtype error")
            except:
                raise Http404
            ref = LawArticle.objects.create(text_law=law_text,
                                            title=lawtitle,
                                            author=user,
                                            is_lwp=True,
                                            law_code=Law_code,
                                            details_law=law_details,
                                            block=box)
        ref.save()
        parent = ref.block
        if parent is None:
            listparents = []
        else:
            listparents = [(parent.title, parent.id, 2)]
            while parent.rank != 1:
                parent = parent.block
                listparents.append((parent.title, parent.id, 2))
        parent = ref.law_code
        listparents.append((parent.title, parent.id, 1))
        listparents.reverse()
        # forms initializations
        qstform = QuestionForm()
        expform = ExplainationForm()
        oppform = PosopinionForm()
        opnform = NegopinionForm()
        prpform = PropositionForm()
        # load all the disclaims, other proposions, opinions, comments and
        # questions about the reflection
        listexplainations = list(ref.explainations.filter(is_censured=False))
        listquestions = list(ref.questions.filter(is_censured=False))
        listcom = listexplainations
        listcom.extend(listquestions)
        listcom = sorted(listcom, key=operator.attrgetter('approval_factor'))
        listcom.reverse()
        listposop = list(ref.posopinions.filter(is_censured=False))
        listnegop = list(ref.negopinions.filter(is_censured=False))
        listpropositions = list(ref.propositions.filter(is_censured=False))
        intro = render_block_to_string('GetReflection.html',
                                       "intro",
                                       locals(),)
        # request)
        content = render_block_to_string('GetReflection.html',
                                         "content",
                                         locals(),)
        # request)
        ctx = {'intro': intro,
               'content': content,
               'typeref': typeref,
               'typeform': "lawf",
               'id_ref': str(ref.id),
               'message': "Votre proposition de loi a bien été ajoutée."}
    else:
        ctx = {'message': "form error",
               'errors': json.dumps(lawform.errors),
               }
    return JsonResponse(ctx)


@login_required
@user_has_no_job_to_do
@user_passes_test(
    lambda u: u.end_of_sanction is None or timezone.now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
def create_new_box():
    """ View to create a new Law Code or codeblock """
    pass
