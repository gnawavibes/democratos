# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth import get_user_model
from CommitApp.models import Commit
from UserManager.models import CYL_user, Note
from CommonsJustice.models import Repport
from ckeditor.fields import RichTextField


###############################################################################
# #################### Classes made for website ############################# #
###############################################################################


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class Reflection(models.Model):
    author = models.ForeignKey(CYL_user,
                               on_delete=models.SET(get_sentinel_user),
                               null=True)
    up = models.IntegerField(default=0)
    down = models.IntegerField(default=0)
    approval_factor = models.FloatField(default=0)
    approval_ratio = models.FloatField(default=0)
    posted = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    notes = GenericRelation(Note)
    content_type = models.ForeignKey(ContentType,
                                     null=True,
                                     on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    is_censured = models.BooleanField(default=False)
    # repport_level = models.IntegerField(default=0)
    repportjobs = GenericRelation(Repport)

    class Meta:
        abstract = True
        ordering = ['-approval_factor', '-update']


class Question(Reflection):
    text_qst = RichTextField()
    explainations = GenericRelation('Explaination')
    questions = GenericRelation('self')
    sources = GenericRelation("SourceLink")
    nb_exp = models.IntegerField(default=0)
    nb_q = models.IntegerField(default=0)
    commit = models.ForeignKey(
        Commit,
        on_delete=models.CASCADE,
        null=True,
    )


class Explaination(Reflection):
    text_exp = RichTextField()
    questions = GenericRelation(Question)
    explainations = GenericRelation('self')
    sources = GenericRelation("SourceLink")
    nb_exp = models.IntegerField(default=0)
    nb_q = models.IntegerField(default=0)
    nb_dis = models.IntegerField(default=0)
    commit = models.ForeignKey(
        Commit,
        on_delete=models.CASCADE,
        null=True,
    )


class Posopinion(Reflection):
    text_opp = RichTextField()
    questions = GenericRelation(Question)
    explainations = GenericRelation(Explaination)
    sources = GenericRelation("SourceLink")
    nb_exp = models.IntegerField(default=0)
    nb_q = models.IntegerField(default=0)
    nb_dis = models.IntegerField(default=0)


class Negopinion(Reflection):
    text_opn = RichTextField()
    questions = GenericRelation(Question)
    explainations = GenericRelation(Explaination)
    sources = GenericRelation("SourceLink")
    nb_exp = models.IntegerField(default=0)
    nb_q = models.IntegerField(default=0)
    nb_dis = models.IntegerField(default=0)


class Proposition(Reflection):
    text_prp = RichTextField()
    details_prp = RichTextField()
    law_article = models.ForeignKey('LawArticle',
                                    on_delete=models.CASCADE)
    questions = GenericRelation(Question)
    explainations = GenericRelation(Explaination)
    posopinions = GenericRelation(Posopinion)
    negopinions = GenericRelation(Negopinion)
    propositions = GenericRelation('self')
    sources = GenericRelation("SourceLink")
    nb_exp = models.IntegerField(default=0)
    nb_q = models.IntegerField(default=0)
    nb_posop = models.IntegerField(default=0)
    nb_negop = models.IntegerField(default=0)
    commit = GenericRelation(Commit)


class SourceLink(Reflection):
    url = models.URLField()
    description = RichTextField()
    commit = models.ForeignKey(
        Commit,
        on_delete=models.CASCADE,
        null=True,
    )
    nb_exp = models.IntegerField(default=0)
    nb_q = models.IntegerField(default=0)

###############################################################################
# ############# Classes linked to law codes architecture #################### #
###############################################################################


class LawArticle(Reflection):
    url = models.URLField(max_length=1000, blank=True)  #  to be deleted?
    text_law = RichTextField()
    law_code = models.ForeignKey('LawCode', on_delete=models.CASCADE)
    block = models.ForeignKey(
        'CodeBlock',
        on_delete=models.CASCADE,
        blank=True,
        null=True)
    updated = models.BooleanField(default='True')
    questions = GenericRelation(Question)
    explainations = GenericRelation(Explaination)
    posopinions = GenericRelation(Posopinion)
    negopinions = GenericRelation(Negopinion)
    propositions = GenericRelation(Proposition)
    sources = GenericRelation("SourceLink")
    nb_exp = models.IntegerField(default=0)
    nb_q = models.IntegerField(default=0)
    nb_posop = models.IntegerField(default=0)
    nb_negop = models.IntegerField(default=0)
    nb_prop = models.IntegerField(default=0)
    commit = GenericRelation(Commit)
    legifranceID = models.CharField(
        max_length=20,
        blank=True,
        null=True)
    is_lwp = models.BooleanField(default=False)
    details_law = RichTextField(default='')
    abrogated = models.DateField(null=True)


class LawCode(models.Model):
    title = models.CharField(max_length=300)
    updated = models.BooleanField(default='True')
    created = models.DateField(null=True)
    abrogated = models.DateField(null=True)
    lastupdate = models.DateTimeField(auto_now=True,
                                      verbose_name="Last update date")


class CodeBlock(models.Model):
    title = models.CharField(max_length=1000)
    position = models.IntegerField()
    rank = models.IntegerField()
    created = models.DateField(null=True)
    abrogated = models.BooleanField(default=False)
    updated = models.BooleanField(default=True)
    law_code = models.ForeignKey('LawCode', on_delete=models.CASCADE)
    block = models.ForeignKey(
        'self',
        on_delete=models.CASCADE,
        blank=True,
        null=True)
    lastupdate = models.DateTimeField(auto_now=True,
                                      verbose_name="Last update date")
    # is new code block proposition?
    is_cbp = models.BooleanField(default=False)
