from functools import wraps
from django.http import Http404, JsonResponse


def ajax_login_required(view_func):
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        if not request.headers.get('x-requested-with') == 'XMLHttpRequest':
            raise Http404
        if request.user.is_authenticated:
            return view_func(request, *args, **kwargs)
        return JsonResponse({'not_authenticated': True})
    return wrapper
