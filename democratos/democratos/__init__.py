# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app

__all__ = ('celery_app',)

import logging
import pdb

stream_handler = logging.StreamHandler()

log = logging.Logger("GIGN")
logging.basicConfig()
log.addHandler(stream_handler)
log.setLevel(logging.DEBUG)
