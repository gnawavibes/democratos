function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function SetAllNewCkeditor(place){
    // Set and Enable all form at the indicated place (place as str)
    $(place).find('form').each(function(){
        $(this).find('textarea').each(function() {
            InitNewCkeditor($(this));
        });
    });
};

function SetMessageForm(FormId){ // Il faut aussi joindre l'ID de la reflection auquel est attaché le form
    $('#'+FormId).on('submit',function(e){ // catch the form's submit event
        for ( instance in CKEDITOR.instances ) // recover data in CKeditor fields
            CKEDITOR.instances[instance].updateElement();
        var datatosend = $(this).serialize();
        $.ajax({ // create an AJAX call...
            data: datatosend, // get the form data
            type: $(this).attr('method'), // GET or POST
            url: $(this).attr('action'), // the file to call
            success: function(rs) { // on success..
                if(rs.message =="form error"){
                    $(".alert-danger").parent().remove();
                    var errors = $.parseJSON(rs.errors);
                    $("#"+FormId).css('display','block')
                    for (var key in errors) {
                        var errormsg = '<div class="bs-component"><div class="alert alert-dismissible alert-danger">'+ errors[key]+"</div></div>";
                        $("#"+FormId).find("[id*='"+key+"']").first().before(errormsg);
                    }
                }
                else{ 
                    $("#message").html(rs.messagepart);
                    eval($("#message").find("script").text());
                    SetAllNewCkeditor("#message");
                    SetMessageForm("MessageForm");
                    if (rs.message != "" && rs.message !="form error"){
                        alert(rs.message);
                    }
                };
            },
            error: function(rs, e) {
               alert(rs.responseText);
            },
        });
        return false;
    });
    return false;
}


function SetJugeDecisionForm(FormId){ // Il faut aussi joindre l'ID de la reflection auquel est attaché le form
    $('#'+FormId).on('submit',function(e){ // catch the form's submit event
        for ( instance in CKEDITOR.instances ) // recover data in CKeditor fields
            CKEDITOR.instances[instance].updateElement();
        var datatosend = $(this).serialize();
        console.log("On go ajax pour JugeDecision!")
        $.ajax({ // create an AJAX call...
            data: datatosend, // get the form data
            type: $(this).attr('method'), // GET or POST
            url: $(this).attr('action'), // the file to call
            success: function(rs) { // on success..
                if(rs.message =="form error"){
                    $(".alert-danger").parent().remove();
                    var errors = $.parseJSON(rs.errors);
                    $("#"+FormId).css('display','block')
                    debugger;
                    for (var key in errors) {
                        var errormsg = '<div class="bs-component"><div class="alert alert-dismissible alert-danger">'+ errors[key]+"</div></div>";
                        $("#"+FormId).find("[for*='"+key+"']").first().before(errormsg);
                    }
                }
                else{ 
                    console.log("success JugeDecision form!");
                    console.log(rs);
                    $("nav").html(rs.nav);
                    $("#main").html(rs.main);
                    $("#content").html(rs.content)
                    SetJusticeTasks();
                    if (rs.message != "" && rs.message !="form error"){
                        alert(rs.message);
                    };
                }
            },
            error: function(rs, e) {
                alert(rs.responseText);
            },
        });
        return false;
    });
    return false;
}


function SetLoadedTask(){
    // ------------- Owner delete own repport -------------
    $(".delrepport").on("click",function(){
        slug = $(this).attr("name")
        url = "DeleteRepport"
        $('<div></div>').appendTo('body')
            .html('<div> Etes vous sûr(e) de vouloir supprimer <br> votre signalement?</div>')
            .dialog({
            modal: true, title: "Confirmez...", zIndex: 10000, autoOpen: true,
            width: 'auto', resizable: false,
            buttons: {
                Oui: function () {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {'slug':slug, csrfmiddlewaretoken: csrftoken},
                        dataType: "json",
                        success: function(response) {
                            $("nav").html(response.nav);
                            $("#main").html(response.main);
                            alert(response.message);
                            SetJusticeTasks();
                        },
                        error: function(rs, e) {
                            console.log("DelRepport error");
                            alert(e, rs.responseText);
                        },
                    });
                    $(this).dialog("close");                               
                },
                Annuler: function () {                                                                
                    Confirmresult(qorigin, false, data);
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
    }); 
    // -------------  Presumed abuser valid repport -------------
    $(".acceptrepport").on("click",function(){
        slug = $(this).attr("name")
        url = "AcceptRepport"
        $('<div></div>').appendTo('body')
            .html('<div> Etes vous sûr(e) de vouloir valider <br> le signalement et de vouloir supprimer <br> votre réflection?</div>')
            .dialog({
            modal: true, title: "Confirmez...", zIndex: 10000, autoOpen: true,
            width: 'auto', resizable: false,
            buttons: {
                Oui: function () {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {'slug':slug, csrfmiddlewaretoken: csrftoken},
                        dataType: "json",
                        success: function(response) {
                            $("nav").html(response.nav);
                            $("#main").html(response.main);
                            alert(response.message);
                            SetJusticeTasks()
                        },
                        error: function(rs, e) {
                            console.log("AccerptRepport error");
                            alert(e, rs.responseText);
                        },
                    });
                    $(this).dialog("close");                               
                },
                Annuler: function () {                                                                
                    Confirmresult(qorigin, false, data);
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
    });  
    // -------------  Owner modify own repport ----------
    $(".modifrepport").on("click",function(){
        var slug = $(this).attr("name")
        var repportid = slug.substring(8)
        var title = "Modifier un signalement";
        var [typeref,idref] = $(this).attr("things").split(":");
        url = "GetModifRepport"
        $.ajax({
            type: "get",
            url: url,
            data: {'slug':slug, csrfmiddlewaretoken: csrftoken},
            dataType: "json",
            success: function(response) {
                var message = eval('`'+response.template+'`');
                var dialogbox = $('<div></div>').appendTo('body')
                    .html('<div>'+message+'</div>')
                    .dialog({
                        modal: true, title: title, zIndex: 10000, autoOpen: true,
                        width: 'auto', resizable: false,
                        close: function (event, ui) {
                            $(this).remove();
                        }
                    }); 
                dialogbox.find('textarea').each(function() {
                    InitNewCkeditor($(this));
                    var t = document.getElementById($(this).attr("id"));
                    var editor = CKEDITOR.instances[t.id];             
                    editor.config.resize_enabled =false;
                    editor.config.removePlugins = 'resize,autogrow';
                    editor.on('instanceReady', function(e){
                        editor.resize('100%',250);
                    });
                });
                $("#CancelDialog").on("click", function(){
                    dialogbox.remove()
                });
                dialogbox.find('form').each(function() {
                    $(this).on('submit',function(e){ // catch the form's submit event
                        for ( instance in CKEDITOR.instances ){// recover data in CKeditor fields
                            CKEDITOR.instances[instance].updateElement();
                        };
                        var datatosend = $(this).serialize();
                        var FormId = $(this).attr('id')
                        datatosend["slug"]=slug;
                        datatosend["csrfmiddlewaretoken"]=csrftoken;
                        $.ajax({ // create an AJAX call...
                            data: datatosend, // get the form data
                            type: "POST", // GET or POST
                            url: "PostModifRepport", // the file to call
                            beforeSend: function(){
                                dialogbox.remove()
                            },
                            success: function(rs) { // REvoir ici!
                                /*if(rs.message=="form error"){
                                    $(".alert-danger").parent().remove();
                                    var errors = $.parseJSON(rs.errors);
                                    $("#"+FormId).css('display','block')
                                    for (var key in errors) {
                                        var errormsg = '<div class="bs-component"><div class="alert alert-dismissible alert-danger">'+ errors[key]+"</div></div>";
                                        $("#"+FormId).find("[id*='"+key+"']").first().before(errormsg);
                                    }
                                }
                                else{ */
                                $("#main").html(rs.main);
                                $('html,body').scrollTop(0);
                                SetAllNewCkeditor("#main");
                                SetMessageForm("MessageForm");
                                SetLoadedTask();
                                //}
                            },
                            error: function(rs, e) {
                               alert(rs.responseText);
                            },
                        });
                    });
                });
            },
            /*
            error: function(rs, e) {
                console.log("LoadTask error");
                alert(e, rs.responseText);
            },*/
        });
    }); 
    // -------------  Presumed abuser disagree and go jury ----------
    $(".gojury").on("click",function(){
        slug = $(this).attr("name")
        url = "GoJury"
        $.ajax({
            type:"get",
            url: url,
            data: {'slug':slug, csrfmiddlewaretoken: csrftoken},
            dataType: "json",
            success: function(response) {
                // console.log("yeah!");
                $("#TaskAction").html("<h4> Action </h4><p> Aucune action possible, le signalement a été sourmis à un jury.</p>");
                alert(response.message);
            },
            error: function(rs, e) {
                console.log("LoadTask error");
                alert(e, rs.responseText);
            },
        });
    });    
}

function SetJusticeTasks(){
    // ------------- Set the Forms for sending msg -------------
    SetMessageForm("MessageForm");
    // ------------- Activate Nav Task on click -------------
    $("#justiceContents").find(".JusticeTask").on("click", function(){
       $("#justiceContents").find(".JusticeTask.active ").removeClass("active");
       $(this).addClass("active");
    });
    // ------------- Load a Justice Task -------------
    $(".JusticeTask").on("click",function(){
        slug = $(this).attr("name")
        url = "LoadTask"
        $.ajax({
            type: "get",
            url: url,
            data: {'slug':slug, csrfmiddlewaretoken: csrftoken},
            dataType: "json",
            success: function(response) {
                // console.log("yeah!");
                $("#main").html(response.main);
                $('html,body').scrollTop(0);
                SetAllNewCkeditor("#main");
                SetMessageForm("MessageForm");
                SetLoadedTask();
                SetJugeDecisionForm("JugeDecisionForm");
            },
            error: function(rs, e) {
                console.log("LoadTask error");
                alert(e, rs.responseText);
            },
        });
    });   
}


$(document).ready(function() {
    $("nav").width($("#justicenav").width());
    SetJusticeTasks();
});