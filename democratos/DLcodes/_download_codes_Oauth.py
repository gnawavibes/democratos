import pdb
from authlib.integrations.requests_client import OAuth2Session, OAuth1Session
import json

# This information is obtained upon registration of a new GitHub


API_key = "96dbcef8-27b2-4ee6-b6cf-d54219a5a2de"
client_id = "2d6c839b-5ba4-4084-8c8c-f914b4eb6ce6"
client_secret = "773f4f8c-0ce8-462e-b9f7-ffeeb5a1047c"
authorization_url = 'https://oauth.aife.economie.gouv.fr/api/oauth/authorize'
token_url = 'https://oauth.aife.economie.gouv.fr/api/oauth/token'
base_url = "https://api.aife.economie.gouv.fr"
get_article_url = base_url + "/dila/legifrance-beta/lf-engine-app/consult/getArticle"


client = OAuth2Session(client_id, client_secret, scope="openid")
authorization_url, state = client.create_authorization_url(authorization_url)
token = client.fetch_token(token_url, authorization_response=authorization_url)
headers = {"Content-Type": "application/json"}

get_test = client.get(
    'https://api.aife.economie.gouv.fr/dila/legifrance-beta/lf-engine-app/list/ping')
get_test2 = client.get(
    'https://api.aife.economie.gouv.fr/dila/legifrance-beta/lf-engine-app/consult/ping')
get_test3 = client.get(
    'https://api.aife.economie.gouv.fr/dila/legifrance-beta/lf-engine-app/chrono/ping')
get_test4 = client.get(
    'https://api.aife.economie.gouv.fr/dila/legifrance-beta/lf-engine-app/search/ping')
get_test5 = client.get(
    'https://api.aife.economie.gouv.fr/dila/legifrance-beta/lf-engine-app/suggest/ping')

print(get_test.text, get_test2.text, get_test3.text,
      get_test4.text, get_test5.text)

resp = client.post(get_article_url, json={
                   "id": "LEGIARTI000030957743"}, headers=headers)

pdb.set_trace()


r = client.post(get_article_url, data=json.dumps(
    {"id": "LEGIARTI000030957743"}))


"""
>>> resp = client.post(get_article_url, json={"id": "LEGIARTI000030957743"}, headers={"Content-Type":"application/json"})
>>> resp.json()
{'executionTime': 0, 'dereferenced': False, 'article': {'id': 'LEGIARTI000030957743', 'idTexte': None, 'type': 'AUTONOME', 'texte': "Les personnes de nationalité étrangère bénéficient dans les conditions propres à chacune de ces prestations : 1° Des prestations d'aide sociale à l'enfance ; 2° De l'aide sociale en cas d'admission dans un centre d'hébergement et de réinsertion sociale ; 3° De l'aide médicale de l'Etat ; 4° Des allocations aux personnes âgées prévues à l'article L. 231-1 à condition qu'elles justifient d'une résidence ininterrompue en France métropolitaine depuis au moins quinze ans avant soixante-dix ans. Elles bénéficient des autres formes d'aide sociale, à condition qu'elles justifient d'un titre exigé des personnes de nationalité étrangère pour séjourner régulièrement en France. Pour tenir compte de situations exceptionnelles, il peut être dérogé aux conditions fixées à l'alinéa ci-dessus par décision du ministre chargé de l'action sociale. Les dépenses en résultant sont à la charge de l'Etat.", 'texteHtml': "<p>Les personnes de nationalité étrangère bénéficient dans les conditions propres à chacune de ces prestations : </p><p>1° Des prestations d'aide sociale à l'enfance ; </p><p>2° De l'aide sociale en cas d'admission dans un centre d'hébergement et de réinsertion sociale ; </p><p>3° De l'aide médicale de l'Etat ; </p><p>4° Des allocations aux personnes âgées prévues à l'article <a href='/affichCodeArticle.do?cidTexte=LEGITEXT000006074069&idArticle=LEGIARTI000006796951&dateTexte=&categorieLien=cid' title='Code de l'action sociale et des familles - art. L231-1 (V)'>L. 231-1</a> à condition qu'elles justifient d'une résidence ininterrompue en France métropolitaine depuis au moins quinze ans avant soixante-dix ans. </p><p>Elles bénéficient des autres formes d'aide sociale, à condition qu'elles justifient d'un titre exigé des personnes de nationalité étrangère pour séjourner régulièrement en France. </p><p>Pour tenir compte de situations exceptionnelles, il peut être dérogé aux conditions fixées à l'alinéa ci-dessus par décision du ministre chargé de l'action sociale. Les dépenses en résultant sont à la charge de l'Etat.</p>", 'num': 'L111-2', 'origine': 'LEGI', 'nature': 'Article', 'versionArticle': '4.0', 'etat': 'VIGUEUR', 'dateDebut': 1446336000000, 'dateFin': 32472144000000, 'dateDebutExtension': 32472144000000, 'dateFinExtension': 32472144000000, 'inap': None, 'ordre': 85898, 'context': {'titresTM': [{'debut': '2000-12-23', 'fin': '2999-01-01', 'titre': 'Partie législative', 'xPath': '/ARTICLE/CONTEXTE/TEXTE/TM/TITRE_TM', 'cid': 'LEGISCTA000006107980', 'id': 'LEGISCTA000006107980', 'etat': 'VIGUEUR'}, {'debut': '2000-12-23', 'fin': '2999-01-01', 'titre': 'Livre Ier : Dispositions générales', 'xPath': '/ARTICLE/CONTEXTE/TEXTE/TM/TM/TITRE_TM', 'cid': 'LEGISCTA000006128457', 'id': 'LEGISCTA000006128457', 'etat': 'VIGUEUR'}, {'debut': '2000-12-23', 'fin': '2999-01-01', 'titre': 'Titre Ier : Principes généraux', 'xPath': '/ARTICLE/CONTEXTE/TEXTE/TM/TM/TM/TITRE_TM', 'cid': 'LEGISCTA000006142831', 'id': 'LEGISCTA000006142831', 'etat': 'VIGUEUR'}, {'debut': '2000-12-23', 'fin': '2999-01-01', 'titre': "Chapitre Ier : Droit à l'aide sociale.", 'xPath': '/ARTICLE/CONTEXTE/TEXTE/TM/TM/TM/TM/TITRE_TM', 'cid': 'LEGISCTA000006157551', 'id': 'LEGISCTA000006157551', 'etat': 'VIGUEUR'}], 'nombreVersionParent': 1, 'longeurChemin': 44, 'titreTxt': [{'debut': '2000-12-23', 'fin': '2999-01-01', 'titre': "Code de l'action sociale et des familles", 'xPath': '/ARTICLE/CONTEXTE/TEXTE/TITRE_TXT', 'cid': 'LEGITEXT000006074069', 'id': 'LEGITEXT000006074069', 'etat': 'VIGUEUR'}]}, 'cid': 'LEGIARTI000006796413', 'cidTexte': None, 'sectionParentCid': 'LEGISCTA000006157551', 'sectionParentId': 'LEGISCTA000006157551', 'sectionParentTitre': "Chapitre Ier : Droit à l'aide sociale.", 'fullSectionsTitre': "Partie législative &gt; Livre Ier : Dispositions générales &gt; Titre Ier : Principes généraux &gt; Chapitre Ier : Droit à l'aide sociale.", 'refInjection': 'IG-20200718', 'idTechInjection': 'LEGIARTI000030957743', 'idEli': None, 'idEliAlias': None, 'calipsos': [], 'textTitles': [{'id': 'LEGITEXT000006074069', 'titre': "Code de l'action sociale et des familles", 'titreLong': "Code de l'action sociale et des familles", 'etat': 'VIGUEUR', 'dateDebut': 977529600000, 'dateFin': 32472144000000, 'cid': 'LEGITEXT000006074069', 'datePubli': 32472144000000, 'datePubliComputed': None, 'dateTexte': 32472144000000, 'dateTexteComputed': None, 'nature': 'CODE', 'nor': '', 'num': '', 'numParution': '', 'originePubli': '', 'appliGeo': None, 'codesNomenclatures': [], 'visas': None, 'nota': None, 'notice': None, 'travauxPreparatoires': None, 'signataires': None, 'dossiersLegislatifs': [], 'ancienId': None}], 'nota': "Conformément au V de l'article 35 de la loi n° 2015-925 du 29 juillet 2015,  les présentes dispositions dans leur rédaction résultant de la présente loi, s'appliquent aux demandeurs d'asile dont la demande a été enregistrée à compter d'une date fixée par décret en Conseil d'Etat, qui ne peut être postérieure au 1er novembre 2015", 'notaHtml': "<p>Conformément au V de l'article 35 de la loi n° 2015-925 du 29 juillet 2015,  les présentes dispositions dans leur rédaction résultant de la présente loi, s'appliquent aux demandeurs d'asile dont la demande a été enregistrée à compter d'une date fixée par décret en Conseil d'Etat, qui ne peut être postérieure au 1er novembre 2015</p>", 'activitePro': [], 'numeroBrochure': [], 'numeroBo': None, 'conteneurs': [], 'lienModifications': [{'textCid': 'JORFTEXT000000402347', 'textTitle': "Rapport au Président de la République relatif à l'ordonnance n° 2000-1249 du 21 décembre 2000", 'linkType': 'CODIFICATION', 'linkOrientation': 'source', 'articleNum': '', 'articleId': 'JORFTEXT000000402347', 'natureText': 'RAPPORT', 'datePubliTexte': '2000-12-23', 'dateSignaTexte': '2999-01-01', 'dateDebutCible': '2999-01-01'}, {'textCid': 'JORFTEXT000000215460', 'textTitle': 'Loi n°2002-2 du 2 janvier 2002', 'linkType': 'CODIFICATION', 'linkOrientation': 'source', 'articleNum': '', 'articleId': 'JORFTEXT000000215460', 'natureText': 'LOI', 'datePubliTexte': '2002-01-03', 'dateSignaTexte': '2002-01-02', 'dateDebutCible': '2999-01-01'}, {'textCid': 'JORFTEXT000030949483', 'textTitle': 'LOI n°2015-925 du 29 juillet 2015 - art. 24', 'linkType': 'MODIFIE', 'linkOrientation': 'cible', 'articleNum': '24', 'articleId': 'LEGIARTI000030950440', 'natureText': 'LOI', 'datePubliTexte': '2015-07-30', 'dateSignaTexte': '2015-07-29', 'dateDebutCible': '2015-07-31'}], 'lienCitations': [{'textCid': 'JORFTEXT000000363587', 'textTitle': 'Décret n°94-294 du 15 avril 1994 - art. 1 (V)', 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': '1', 'articleId': 'LEGIARTI000006684122', 'natureText': 'DECRET', 'date': 1595085375796, 'parentCid': None, 'numTexte': '94-294', 'datePubli': 766454400000, 'dateDebut': 977529600000}, {'textCid': 'JORFTEXT000030949483', 'textTitle': 'LOI n° 2015-925 du 29 juillet 2015 - art. 35 (V)', 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': '35', 'articleId': 'LEGIARTI000030950462', 'natureText': 'LOI', 'date': 1595085375796, 'parentCid': None, 'numTexte': '2015-925', 'datePubli': 1438214400000, 'dateDebut': 1438300800000}, {'textCid': 'JORFTEXT000031194603', 'textTitle': 'DÉCRET n°2015-1166 du 21 septembre 2015 - art. 30 (VD)', 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': '30', 'articleId': 'LEGIARTI000031196336', 'natureText': 'DECRET', 'date': 1595085375796, 'parentCid': None, 'numTexte': '2015-1166', 'datePubli': 1442880000000, 'dateDebut': 1446336000000}, {'textCid': 'LEGITEXT000006074069', 'textTitle': "Code de l'action sociale et des familles - art. L111-1 (V)", 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': 'L111-1', 'articleId': 'LEGIARTI000006796412', 'natureText': 'CODE', 'date': 1595085375796, 'parentCid': None, 'numTexte': '', 'datePubli': 32472144000000, 'dateDebut': 977529600000}, {'textCid': 'LEGITEXT000006074069', 'textTitle': "Code de l'action sociale et des familles - art. L132-8 (V)", 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': 'L132-8', 'articleId': 'LEGIARTI000031728913', 'natureText': 'CODE', 'date': 1595085375796, 'parentCid': None, 'numTexte': '', 'datePubli': 32472144000000, 'dateDebut': 1451433600000}, {'textCid': 'LEGITEXT000006074069', 'textTitle': "Code de l'action sociale et des familles - art. L231-1 (V)", 'linkType': 'CITATION', 'linkOrientation': 'source', 'articleNum': 'L231-1', 'articleId': 'LEGIARTI000006796951', 'natureText': 'CODE', 'date': 1595085375796, 'parentCid': None, 'numTexte': '', 'datePubli': 32472144000000, 'dateDebut': 977529600000}, {'textCid': 'LEGITEXT000006074069', 'textTitle': "Code de l'action sociale et des familles - art. L232-1 (M)", 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': 'L232-1', 'articleId': 'LEGIARTI000006796960', 'natureText': 'CODE', 'date': 1595085375796, 'parentCid': None, 'numTexte': '', 'datePubli': 32472144000000, 'dateDebut': 977529600000}, {'textCid': 'LEGITEXT000006074069', 'textTitle': "Code de l'action sociale et des familles - art. L242-13 (V)", 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': 'L242-13', 'articleId': 'LEGIARTI000006797076', 'natureText': 'CODE', 'date': 1595085375797, 'parentCid': None, 'numTexte': '', 'datePubli': 32472144000000, 'dateDebut': 1108166400000}, {'textCid': 'LEGITEXT000006074069', 'textTitle': "Code de l'action sociale et des familles - art. L541-1 (V)", 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': 'L541-1', 'articleId': 'LEGIARTI000035902920', 'natureText': 'CODE', 'date': 1595085375797, 'parentCid': None, 'numTexte': '', 'datePubli': 32472144000000, 'dateDebut': 1514764800000}, {'textCid': 'LEGITEXT000006072665', 'textTitle': 'Code de la santé publique - art. L3112-3 (Ab)', 'linkType': 'CITATION', 'linkOrientation': 'cible', 'articleNum': 'L3112-3', 'articleId': 'LEGIARTI000037065099', 'natureText': 'CODE', 'date': 1595085375797, 'parentCid': None, 'numTexte': '', 'datePubli': 32472144000000, 'dateDebut': 1528934400000}], 'lienConcordes': [{'textCid': 'LEGITEXT000006072637', 'textTitle': "Code de la famille et de l'aide sociale. - art. 186 (P)", 'linkType': 'CONCORDANCE', 'linkOrientation': 'source', 'articleNum': '186', 'articleId': 'LEGIARTI000006681404', 'natureText': 'CODE'}, {'textCid': 'LEGITEXT000006072637', 'textTitle': "Code de la famille et de l'aide sociale. - art. 186 (Ab)", 'linkType': 'CONCORDE', 'linkOrientation': 'cible', 'articleNum': '186', 'articleId': 'LEGIARTI000006681407', 'natureText': 'CODE'}], 'lienAutres': [], 'articleVersions': [{'id': 'LEGIARTI000006796413', 'etat': 'MODIFIE', 'version': '1.0', 'dateDebut': 977529600000, 'dateFin': 1041292800000, 'numero': None, 'ordre': None}, {'id': 'LEGIARTI000006796414', 'etat': 'MODIFIE', 'version': '2.0', 'dateDebut': 1041292800000, 'dateFin': 1153785600000, 'numero': None, 'ordre': None}, {'id': 'LEGIARTI000006796415', 'etat': 'MODIFIE', 'version': '3.0', 'dateDebut': 1153785600000, 'dateFin': 1446336000000, 'numero': None, 'ordre': None}, {'id': 'LEGIARTI000030957743', 'etat': 'VIGUEUR', 'version': '4.0', 'dateDebut': 1446336000000, 'dateFin': 32472144000000, 'numero': None, 'ordre': None}], 'computedNums': ['L111-2', '111-2'], 'versionPrecedente': 'LEGIARTI000006796415', 'conditionDiffere': None, 'historique': None, 'surtitre': None, 'renvoi': None}}
""

text = ""
<p></p>   Les membres du Gouvernement ont accès aux deux Assemblées. Ils sont entendus quand ils le demandent.<p></p><p></p>   Ils peuvent se faire assister par des commissaires du Gouvernement.<p></p>
""

def removeUselessPTags(text):
    ""
    Remove the useless '<p></p>' from the text recovered from DILA

    :param text: text to clean
    :type text: sting

    :return: cleaned text
    :rtype: string
    ""
    occur = text.count("<p></p>")
    if occur%2==0:
        for x in range(occur):
            if x%2==0:
                text = text.replace("<p></p>","<p>",1)
            else:
                text = text.replace("<p></p>","</p>",1)
    return text

print(removeUselessPTags(text))

import pdb; pdb.set_trace()
"""
