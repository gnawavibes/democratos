# -*- coding: utf-8 -*-

# #############################################################################
# ################################   Notes   ##################################
# #############################################################################
#
# 1. run the script
# 2. acces to the codes by listofcodes: for the 'x'th code:
#     listofcodes[x][0] or listofcodes[x][2].name: code's title
#     listofcodes[x][1]: ref legifrance
#     listofcodes[x][2]: the code
#
# *listofcodes[x][2].contents[] will return the main titles of the codes
# *listofcodes[x][2].contents[0].contents[] will return the rank 2 titles or
# the Articles contained in the first main title of the code
# *etc.
#
# #############################################################################

#--------------- IMPORTS -----------------------

from selenium.webdriver.firefox.options import Options as options
from selenium.webdriver.firefox.service import Service
import selenium

if __name__ == "__main__":
    import sys
    import os
    import django
    BASE_DIR = os.path.abspath(os.pardir)
    # On charge l'ORM django
    sys.path.append(BASE_DIR+"/democratos/")
    os.environ['DJANGO_SETTINGS_MODULE'] = 'democratos.settings'
    django.setup()

from CreateYourLaws.models import (
    LawCode, LawArticle, CodeBlock
)
from UserManager.models import CYL_user
from django.utils import timezone
from CommitApp.models import Commit
from django.contrib.contenttypes.models import ContentType
from DLcodes.functions import get_something,remove_piece_of_text
from democratos import log

import ssl
import dateparser
import time
from datetime import datetime, timedelta
from bs4 import BeautifulSoup as BS4
import pytz
from authlib.integrations.requests_client import OAuth2Session
import json

# Selenium web driver
from selenium.webdriver import Firefox
driver = Firefox() # executable_path=driver_path)


# timeout function
from contextlib import contextmanager
import signal
import time

@contextmanager
def timeout(duration):
    def timeout_handler(signum, frame):
        raise TimeoutError(f'block timedout after {duration} seconds')
    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(duration)
    try:
        yield
    finally:
        signal.alarm(0)

# This information is obtained upon registration of a new GitHub
API_key = "6f52f04e-ac0e-479d-a73a-1579d3389ef3"
client_id = "dd7868b0-704e-4783-affb-4d361ffb9e1f"
client_secret = "69922f28-07c2-4d1f-ba25-8aecc9698824"
Authorization_URL = 'https://oauth.aife.economie.gouv.fr/api/oauth/authorize'
token_url = 'https://oauth.aife.economie.gouv.fr/api/oauth/token'
base_url = "https://api.aife.economie.gouv.fr"
get_article_url = base_url + "/dila/legifrance-beta/lf-engine-app/consult/getArticle"
headers = {"Content-Type": "application/json"}

# ----- Access to the DILA REST API -----


def CreateGlobalOAuth2Values():
    """
    Get client and token for Oauth connection
    """
    global client
    global token
    if 'client' in globals():
        del client
    if 'token' in globals():
        del token
    client = OAuth2Session(client_id, client_secret, scope="openid")
    authorization_url, state = client.create_authorization_url(
        Authorization_URL)
    token = client.fetch_token(
        token_url, authorization_response=authorization_url)


CreateGlobalOAuth2Values()

#---------------- GLOBAL VARIABLES ---------------------------------------


current_tz = timezone.get_current_timezone()

headers2 = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0'}

URL_HTML_BASE = """https://www.legifrance.gouv.fr/"""

URL_LEGIFRANCE_REF_CODES = URL_HTML_BASE + \
    """liste/code?etatTexte=VIGUEUR&etatTexte=ABROGE&page=1#code"""

MOIS = [
    "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "août",
    "septembre",
    "octobre",
    "novembre",
    "décembre"
]


class Title:
    """
    Containers of Articles or other containers. Used only\
    to organize the download step
    """

    def __init__(self, rank, name):
        self.rank = rank
        self.name = name
        self.idsql = 0
        self.contents = []


def Get_codes_ref(url):  # 1
    """
    recover all the legifrance law code reference

    title-result-item link
    """
    driver.get(url)
    soup = BS4(driver.page_source, "html.parser")
    titles = soup.find_all("h2", attrs={"class": "title-result-item-code"})
    list_of_codes = []
    for el in titles:
        if el.find("p", attrs={"class": "tag"}).get_text() == "Abrogé":
            abrogated = True
            info = el.parent.find("p", attrs={"class": "code-info"}).get_text()
            info = info.split('.')[1].split('au ')
            date = [
                dateparser.parse(info[0].split('du ')[1], languages=['fr']),
                dateparser.parse(info[1], languages=['fr']),
            ]
        elif el.find("p", attrs={"class": "tag"}).get_text() == "Vigueur différée":
            abrogated = False
            info = el.parent.find("p", attrs={"class": "code-info"}).get_text()
            date = [dateparser.parse(
                info.split("à partir du ")[1], languages=['fr'])]
        else:
            abrogated = False
            info = el.parent.find("p", attrs={"class": "code-info"}).get_text()
            date = [dateparser.parse(
                info.split("En vigueur depuis le ")[1], languages=['fr'])]
        list_of_codes.append((
            el.a.get("id")[2:],
            el.a.get_text(),
            abrogated,
            date,
        ))
    return list_of_codes


def GetRank(parents):
    rank = 0
    for parent in parents:
        if parent.name == "ul":
            rank += 1
    return rank


def get_code_data_REST(code_LEGITEXT, url_fail, Gov_id):  # 2
    """ Recover the data (titles and articles) from a specified code
    (param code LEGITEXT corresponding on Legifrance) """
    # --------- Get code summary from link --------------
    url = URL_HTML_BASE + "codes/texte_lc/" + code_LEGITEXT[0] +\
        "?etatTexte=VIGUEUR&etatTexte=VIGUEUR_DIFF" +\
        "&etatTexte=ABROGE&page=1"
    try:
        driver.get(url)
        soup = BS4(driver.page_source, "html.parser")
    except:
        log.error(("Impossible reach url for code: ", code_LEGITEXT))
        url_fail.append(('code', code_LEGITEXT))
        return url_fail
    # ---------------- initialization ---------------
    pos_counter = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    list_title_open = []
    soup.prettify()
    # --------- Code Creation ---------------
    try:
        Law_Code, created = LawCode.objects.get_or_create(
            title=code_LEGITEXT[1])
        log.debug(code_LEGITEXT[1])
        if code_LEGITEXT[2]:
            Law_Code.abrogated = code_LEGITEXT[3][1]
        else:
            Law_Code.abrogated = None
        Law_Code.created = code_LEGITEXT[3][0]
        Law_Code.updated = True
        Law_Code.save()
    except:
        log.error((
            "Impossible to get/create the law code: ",
            code_LEGITEXT[1]))
        url_fail.append(('code', code_LEGITEXT))
        return url_fail
    # ------- find paragraph title & Articles ------------
    resume = soup.find(id="liste-sommaire")
    links = resume.find_all("a")
    new_title = None
    for link in links:
        # CREATE BLOCK
        if link["id"][:8] == "LEGISCTA":
            log.info(("code bloc named:", link.get_text()))
            if "class" in link.attrs and "abrogated" in link["class"]:
                abrogated = True
            else:
                abrogated = False
            rank = GetRank(link.parents)
            title_txt = str(link.get_text())
            title = Title(rank, title_txt)
            if title.rank == 1:
                pos_counter[2] = 0
                list_title_open.clear()
            else:
                for x in range(len(list_title_open) - 1, 0, -1):
                    if list_title_open[x].rank >= title.rank:
                        if list_title_open[x].rank > title.rank:
                            pos_counter[list_title_open[x].rank] = 0
                        del list_title_open[x]
                list_title_open[-1].contents.append(title)
            pos_counter[title.rank] += 1
            new_title, created = CodeBlock.objects.get_or_create(
                title=title.name,
                position=pos_counter[title.rank],
                abrogated=abrogated,
                rank=title.rank,
                law_code=Law_Code)
            title.idsql = new_title.id
            if (title.rank > 1 and
                    list_title_open[-1].idsql < title.idsql):
                new_title.block = CodeBlock.objects.get(
                    title=list_title_open[-1].name,
                    rank=list_title_open[-1].rank,
                    id=list_title_open[-1].idsql,
                    law_code=Law_Code)
            new_title.updated = True
            new_title.save()
            list_title_open.append(title)
            log.debug((title.name, ' ', title.rank))
        # CREATE ARTICLE
        elif link["id"][:8] == "artLEGIA":
            url_fail = get_article_REST(
                link["id"][3:],
                Law_Code.id,
                new_title.id,
                url_fail,
                Gov_id,
            )
        else:
            log.error(("Unkown id in the resume!\n" +
                       "must check for link:\n",
                       link,
                       "\nWith link id: ",
                       link["id"]))
    return url_fail


def get_article_REST(legiartID, codeID, blockID, url_fail, Gov_id):
    """ detect, select and copy all articles from a Legifrance page """
    # If block is none --> direct in law code without block
    try:
        with timeout(30):
            response = client.post(get_article_url, json={
                                   "id": legiartID}, headers=headers)
        resp = response.json()
    except:
        log.info(("Wait for refresh token ", token))
        time.sleep(30)
        CreateGlobalOAuth2Values()
        log.info(("token refresh! ", token))
        try:
            with timeout(30):
                response = client.post(get_article_url, json={
                                   "id": legiartID}, headers=headers)
            resp = response.json()
        except:
            log.error("Article load from DILA error or Refresh token error")
            raise ValueError
    json_art = resp["article"]
    if json_art is None:
        url_fail.append(["article", legiartID, codeID, blockID ])
        return url_fail
    Save_Article_REST(json_art, codeID, blockID, url_fail, Gov_id)
    return url_fail

def Save_Article_REST(json_art, codeID, blockID, url_fail, Gov_id):        
    log.debug((
        "Receive: Article ",
        json_art["num"],
        " and it is verion: ",
        json_art["versionArticle"]
    ))
    art, created = LawArticle.objects.get_or_create(
        author_id=Gov_id,
        title="Article "+json_art["num"],
        law_code_id=codeID,
        block_id=blockID,
        legifranceID=json_art["id"],
        text_law=cleanArtText(json_art["texteHtml"]),
        details_law=cleanArtText(json_art['nota']),
    )
    if json_art["etat"] == "ABROGE":
        art.abrogated = datetime.fromtimestamp(json_art["dateFin"]//1000,
            tz=current_tz,
        )
    else:
        art.abrogated = None
    if len(json_art["articleVersions"]) > 1:
        art.update = datetime.fromtimestamp(json_art["dateDebut"]//1000,
            tz=current_tz,
        )
    else:
        art.update = None
    art.posted = datetime.fromtimestamp(
        json_art["articleVersions"][0]["dateDebut"]//1000,
        tz=current_tz,
    )
    art.updated = True
    art.save()
    arttype = ContentType.objects.get_for_model(art)
    # Get commits
    for i, comt in enumerate(json_art["articleVersions"]):
        if i < len(json_art["articleVersions"])-1:
            try:
                with timeout(30):
                    resp = client.post(get_article_url, json={
                                   "id": comt["id"]}, headers=headers).json()
                com = resp["article"]
            except:
                log.info(("Wait for refresh token ", token))
                time.sleep(30)
                CreateGlobalOAuth2Values()
                log.info(("token refresh! ", token))
                try:
                    with timeout(30):
                        response = client.post(get_article_url, json={
                                           "id": comt["id"]}, headers=headers)
                    resp = response.json()
                    com = resp["article"]
                except:
                    log.error("Commit load from DILA error or Refresh token error")
                    raise ValueError
            try:
                commit, created = Commit.objects.get_or_create(
                    content_type=arttype,
                    object_id=art.id,
                    commit_title="Article "+com["num"],
                    commit_txt=cleanArtText(com["texteHtml"]),
                    commit_details=com["nota"],
                    legifranceID=com["id"],
                    comments=str(com['lienModifications']),
                )
                commit.posted = datetime.fromtimestamp(
                    com["dateFin"]//1000,
                    tz=current_tz
                    )
                commit.save()
                log.debug(
                    ("Receive: Article ", json_art["num"], "  commit for version: ", com["versionArticle"]))
            except:
                log.error(("Error in DILA Database for commits from Article: ", json_art["num"])) # DILA does not return a correct LEGIFRANCE_ID or the commit does not exist


def cleanArtText(text):
    """
    Remove the useless tags and text formating\
    from the text recovered from DILA

    :param text: text to clean
    :type text: sting

    :return: cleaned text
    :rtype: string
    """
    text = cleanTextFromLineBreakAndTab(text)
    for x in ['div', 'a']:
        text = remove_piece_of_text(text, '<' + x, '>')
        text = text.replace('</' + x + '>', '')
    removeUselessPTags(text)
    return text


def cleanTextFromLineBreakAndTab(text):
    """
    Remove the useless tab and linebreak\
    from the text recovered from DILA

    :param text: text to clean
    :type text: sting

    :return: cleaned text
    :rtype: string
    """
    for el in ["\n", "\t"]:
        text = text.replace(el, "")
    return text


def removeUselessPTags(text):
    """
    Remove the useless '<p></p>' from the text recovered from DILA

    :param text: text to clean
    :type text: sting

    :return: cleaned text
    :rtype: string
    """
    occur = text.count("<p></p>")
    if occur % 2 == 0:
        for x in range(occur):
            if x % 2 == 0:
                text = text.replace("<p></p>", "<p>", 1)
            else:
                text = text.replace("<p></p>", "</p>", 1)
    return text



def Get_Constitution_REST(url_fail, Gov_id):
    """ recover the constitution """
    url = URL_HTML_BASE + "loda/texte_lc/JORFTEXT000000571356/" +\
        datetime.now().strftime("%Y-%m-%d") + "/" +\
        "?etatTexte=VIGUEUR&etatTexte=VIGUEUR_DIFF" +\
        "&etatTexte=ABROGE&page=1"
    try:
        driver.get(url)
        soup = BS4(driver.page_source, "html.parser")
    except:
        url_fail.append(("constitution", url))
        log.error("URL CONSTITUTION NOT FOUND!")
        return url_fail
    constitution, created = LawCode.objects.get_or_create(
        title="Constitution de la Ve Republique Française")
    content = soup.find("div", attrs={"class": "page-content"})
    for pos, el in enumerate(content.find_all("label", attrs={"class": "title-link"})):
        try:
            if "class" in el.attrs and "abrogated" in el["class"]:
                abrogated = True
            else:
                abrogated = False
            new_block, created = CodeBlock.objects.get_or_create(
                title=getblockname(el),
                position=pos,
                rank=1,
                law_code=constitution,
            )
            new_block.abrogated = abrogated
            new_block.updated = True
            new_block.save()
        except:
            log.error(("Impossible to get/create the block code",
                       " title: ", title.name,
                       "from: ", code.name))
    for el in soup.find_all("p", {"class": "name-article"}):
        container = get_block(
            el.find_previous("label", attrs={"class": "title-link"}),
            constitution
        )
        try:
            container_id = container.id
        except:
            container_id=None
        try:
            legiartID = el.a["id"][4:].split("-")[0]
        except:
            legiartID = el["id"][4:].split("-")[0]
        url_fail = get_article_REST(
            legiartID,
            constitution.id,
            container_id,
            url_fail,
            Gov_id,
        )
    constitution.updated = True
    constitution.save()
    log.info("constitution terminée!")
    return url_fail

def getblockname(htmlblockname):
    name = ""
    for i, el in enumerate(htmlblockname.contents):
        if i>0:
            name += " "
        name += str(el.string)
    return name

def get_block(htmlblocktitle, law_code):
    """ Look for the previous block name to recover
    the parent block for DB"""
    if htmlblocktitle is None:
        block = None
    else:
        name = getblockname(htmlblocktitle)
        block = CodeBlock.objects.get(
            title=name,
            law_code=law_code,
        )
    return block


def _Main_(url=URL_LEGIFRANCE_REF_CODES):
    """
    Main that will pilot and run the download functions:
    - Get the codes reference
    - download the constitution
    - download each codes
    - delete obsolete Law codes, articles and commits
    - 
    """
    # INITIALISATION
    LawCode.objects.all().updated = False
    CodeBlock.objects.all().updated = False
    LawArticle.objects.all().updated = False
    Gov = CYL_user.objects.get(id=1, username="government")
    url_fail = []

    # FIRST TRY TO UPDATE EVERYTHING
    list_codes = Get_codes_ref(URL_LEGIFRANCE_REF_CODES)
    log.info(("Go!\n", list_codes))
    url_fail = Get_Constitution_REST(url_fail, Gov.id)
    for i, code in enumerate(list_codes):
        get_code_data_REST(code, url_fail, Gov.id)

    # RECOVER ALL THE DATA FAILLED BY REQUEST ERROR
    log.debug(url_fail)
    nb_fail = 0
    while url_fail and nb_fail>3:
        for el in url_fail:
            url_fail.remove(el)
            if el[0] == "constitution":
                url_fail = Get_Constitution_REST(url_fail, Gov.id)
            elif el[0] == "code":
                url_fail = get_code_data(el[1], url_fail, Gov.id)
            elif el[0] == "article":
                url_fail = get_article_REST(el[1], el[2].id, el[3].id, url_fail, Gov.id)
            else:
                log.error("url_fail type does not exist")
        log.info(("url_fail:\n", url_fail))
        log.info((" length url_fail: ", len(url_fail)))
        nb_fail += 1

    # DELETE OBSOLETE
    list_del_LC = LawCode.objects.filter(updated=False)
    for el in list_del_LC:
        list_el_del = el.CodeBlock_set.all()
        for el2 in list_el_del:
            el2.LawArticle_set.all().delete()
        el.delete()
    LawCode.objects.filter(updated=False).delete()
    CodeBlock.objects.filter(updated=False).delete()
    LawArticle.objects.filter(updated=False).delete()

    # END
    log.info("DB updated!")
    driver.close()


if __name__ == "__main__":
    _Main_()

"""
TEST GET CODE
url = 'https://sandbox-api.piste.gouv.fr/dila/legifrance-beta/lf-engine-app/consult/code'
resp = client.post(url, json={"date":"2022-09-27","textid":"LEGITEXT000006075116"},headers=headers)
r = resp.json()
print(r)

"""