from __future__ import absolute_import, unicode_literals
from democratos.celery import app
from CreateYourLaws.models import LawArticle
from democratos.settings import BASE_DIR
from DLcodes.functions import get_something,remove_piece_of_text
from datetime import datetime, timedelta

import django
import sys
import os
from django.utils import timezone
from CommitApp.models import Commit
from django.contrib.contenttypes.models import ContentType

current_tz = timezone.get_current_timezone()

from democratos import log

# timeout function
from contextlib import contextmanager
import signal
import time

@contextmanager
def timeout(duration):
    def timeout_handler(signum, frame):
        raise TimeoutError(f'block timedout after {duration} seconds')
    signal.signal(signal.SIGALRM, timeout_handler)
    signal.alarm(duration)
    try:
        yield
    finally:
        signal.alarm(0)


from authlib.integrations.requests_client import OAuth2Session
import json

# This information is obtained upon registration of a new GitHub
API_key = "6f52f04e-ac0e-479d-a73a-1579d3389ef3"
client_id = "dd7868b0-704e-4783-affb-4d361ffb9e1f"
client_secret = "69922f28-07c2-4d1f-ba25-8aecc9698824"
Authorization_URL = 'https://oauth.aife.economie.gouv.fr/api/oauth/authorize'
token_url = 'https://oauth.aife.economie.gouv.fr/api/oauth/token'
base_url = "https://api.aife.economie.gouv.fr"
get_article_url = base_url + "/dila/legifrance-beta/lf-engine-app/consult/getArticle"
headers = {"Content-Type": "application/json"}


def CreateGlobalOAuth2Values():
    """
    Get client and token for Oauth connection
    """
    global client
    global token
    if 'client' in globals():
        del client
    if 'token' in globals():
        del token
    client = OAuth2Session(client_id, client_secret, scope="openid")
    authorization_url, state = client.create_authorization_url(
        Authorization_URL)
    token = client.fetch_token(
        token_url, authorization_response=authorization_url)


CreateGlobalOAuth2Values()

@app.task
def Save_Article_REST(json_art, codeID, blockID, url_fail, Gov_id):        
    log.debug((
        "Receive: Article ",
        json_art["num"],
        " and it is verion: ",
        json_art["versionArticle"]
    ))
    art, created = LawArticle.objects.get_or_create(
        author_id=Gov_id,
        title="Article "+json_art["num"],
        law_code_id=codeID,
        block_id=blockID,
        legifranceID=json_art["id"],
        text_law=cleanArtText(json_art["texteHtml"]),
        details_law=cleanArtText(json_art['nota']),
    )
    if json_art["etat"] == "ABROGE":
        art.abrogated = datetime.fromtimestamp(json_art["dateFin"]//1000,
            tz=current_tz,
        )
    else:
        art.abrogated = None
    if len(json_art["articleVersions"]) > 1:
        art.update = datetime.fromtimestamp(json_art["dateDebut"]//1000,
            tz=current_tz,
        )
    else:
        art.update = None
    art.posted = datetime.fromtimestamp(
        json_art["articleVersions"][0]["dateDebut"]//1000,
        tz=current_tz,
    )
    art.updated = True
    art.save()
    arttype = ContentType.objects.get_for_model(art)
    # Get commits
    for i, comt in enumerate(json_art["articleVersions"]):
        if i < len(json_art["articleVersions"])-1:
            try:
                with timeout(30):
                    resp = client.post(get_article_url, json={
                                   "id": comt["id"]}, headers=headers).json()
                com = resp["article"]
            except:
                log.info(("Wait for refresh token ", token))
                time.sleep(30)
                CreateGlobalOAuth2Values()
                log.info(("token refresh! ", token))
                try:
                    with timeout(30):
                        response = client.post(get_article_url, json={
                                           "id": comt["id"]}, headers=headers)
                    resp = response.json()
                except:
                    log.error("Commit load from DILA error or Refresh token error")
                    raise ValueError
            commit, created = Commit.objects.get_or_create(
                content_type=arttype,
                object_id=art.id,
                commit_title="Article "+com["num"],
                commit_txt=cleanArtText(com["texteHtml"]),
                commit_details=com["nota"],
                legifranceID=com["id"],
                comments=str(com['lienModifications']),
            )
            commit.posted = datetime.fromtimestamp(
                com["dateFin"]//1000,
                tz=current_tz
                )
            commit.save()
            log.debug(
                ("Receive: Article ", json_art["num"], "  commit for version: ", com["versionArticle"]))


