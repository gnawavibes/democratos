# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django import forms
from CommonsJustice.models import (
    Repport, JugeDecision, REASON_CHOICES, MessageRepport
)
from django.utils.translation import gettext as _
from ckeditor.widgets import CKEditorWidget


class JugeDecisionForm(forms.ModelForm):
    """docstring for RepportForm"""

    def __init__(self, *args, **kwargs):
        super(JugeDecisionForm, self).__init__(*args, **kwargs)

    def clean(self):
        if (self.cleaned_data["verdict"] is None or
                self.cleaned_data["repport_abuse"] is None):
            message = (
                "Vous devez impérativement vous prononcer sur la " +
                "recevabilité ET l'hypothétique abus de ce repport."
            )
            self.add_error('verdict', message)
        return self.cleaned_data

    class Meta:
        model = JugeDecision
        fields = ('verdict', 'repport_abuse', 'comments')
        labels = {'verdict': _('Pensez vous que ce signalement soit recevable?'),
                  'repport_abuse': _('Pensez vous que ce signalement soit abusif?'),
                  'comments': _("Commentez vos décisions (optionnel):")}
        widgets = {
            'verdict': forms.RadioSelect(
                choices=((True, 'Oui'), (False, 'Non')),
                attrs={"class": "custom-control-input",
                       "type": "radio", "name": "customRadio"}
            ),
            'repport_abuse': forms.RadioSelect(
                choices=((True, 'Oui'), (False, 'Non')),
                attrs={"class": "custom-control-input"},
            ),
            'comments': CKEditorWidget(config_name='Dialogbox'),
        }


class RepportForm(forms.ModelForm):
    """docstring for RepportForm"""

    def __init__(self, *args, **kwargs):
        super(RepportForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Repport
        fields = ('reason', 'comments')
        labels = {'reason': _('Motifs du signalement... :'),
                  'comments': _("Développez votre question")}
        widgets = {
            'comments': CKEditorWidget(config_name='Dialogbox'),
        }


class MessageForm(forms.ModelForm):
    """docstring for MessageForm"""

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)

    class Meta:
        model = MessageRepport
        fields = ('message',)
        labels = {'message': _(''),
                  }
        widgets = {
            'message': CKEditorWidget(config_name='Dialogbox'),
        }
