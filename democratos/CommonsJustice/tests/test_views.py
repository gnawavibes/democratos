from django.urls import reverse
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase
from django.http import (
    JsonResponse, HttpResponse, HttpResponseNotFound,
    HttpResponseNotAllowed
)
from django.test.client import Client
from mixer.backend.django import mixer
import pytest
import pytz
from unittest import mock

from CreateYourLaws.models import LawArticle, Posopinion
from UserManager.models import CYL_user
from CommonsJustice.models import (
    JusticeTask, JugeDecision, Repport, MessageRepport, Jury
)
from CommonsJustice.views import *
from django.utils.timezone import now
from datetime import timedelta, datetime


@pytest.mark.django_db
class TestLoadRepportFormView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestLoadRepportFormView, cls).setUpClass()
        cls.path = reverse('LoadRepportForm')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()

    def setUp(self):
        self.client.force_login(self.user)

    def test_LoadRepportForm_authenticated_has_no_job_notpunished(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('CreateRepport.html')

    def test_LoadRepportForm_unauthenticated(self):
        client = Client()
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_LoadRepportForm_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = LoadRepportForm(request)

    def test_LoadRepportForm_POST_error(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_LoadRepportForm_hasjobtodo(self):
        task = mixer.blend(JusticeTask,
                           worker=self.user,
                           type_task=1)
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn('/CommonsJustice/takedecisions/1', response.url)

    def test_LoadRepportForm_ispunished(self):
        user = mixer.blend(
            CYL_user,
            end_of_sanction=now()+timedelta(seconds=180)
        )
        client = Client()
        client.force_login(user)
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn('/CommonsJustice/YouArePunished', response.url)

    @classmethod
    def tearDownClass(cls):
        super(TestLoadRepportFormView, cls).tearDownClass()


@pytest.mark.django_db
class TestCreateRepportView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestCreateRepportView, cls).setUpClass()
        cls.path = reverse('CreateRepport')
        cls.lawart = mixer.blend(LawArticle)
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()

    def setUp(self):
        self.client.force_login(self.user)

    def test_CreateRepport_authenticated(self):
        response = self.client.post(
            self.path,
            {'typeref': 'law',
             'idref': self.lawart.id,
             'comments': "blabla",
             "reason": 1,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)

    def test_CreateRepport_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = CreateRepport(request)

    def test_CreateRepport_GET_error(self):
        response = self.client.get(
            self.path,
            {'typeref': 'law',
             'idref': self.lawart.id,
             'comments': "blabla",
             "reason": 1,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_CreateRepport_unauthenticated(self):
        client = Client()
        response = client.post(
            self.path,
            {'typeref': 'law',
             'idref': self.lawart.id,
             'comments': "blabla",
             "reason": 1,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    @mock.patch(
        "CommonsJustice.models.Repport.save",
        mock.MagicMock(name="save"),
    )
    @mock.patch(
        "CommonsJustice.models.JusticeTask.save",
        mock.MagicMock(name="save"),
    )
    def test_CreateRepport_repport_created(self):
        response = self.client.post(
            self.path,
            {'typeref': 'law',
             'idref': self.lawart.id,
             'comments': "blabla",
             "reason": 1,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(Repport.save.called)
        self.assertContains(response, "message")
        self.assertEqual(JusticeTask.save.call_count, 2)

    @mock.patch(
        "CommonsJustice.models.JusticeTask.save",
        mock.MagicMock(name="save"),
    )
    def test_CreateRepport_unvalid_form(self):
        response = self.client.post(
            self.path,
            {'typeref': 'law',
             'idref': self.lawart.id,
             'comments': "blabla",
             "reason": 0,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertContains(response, "message")
        self.assertContains(response, "errors")
        self.assertEqual(
            content["message"],
            "form error"
        )

    @classmethod
    def tearDownClass(cls):
        super(TestCreateRepportView, cls).tearDownClass()


@pytest.mark.django_db
class TestTakeDecisionsView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestTakeDecisionsView, cls).setUpClass()
        cls.path = reverse('TakeDecisions')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()

    def setUp(self):
        self.client.force_login(self.user)

    def test_TakeDecisions_authenticated(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)

    def test_TakeDecisions_GET_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = TakeDecisions(request)

    def test_TakeDecisions_unauthenticated(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertIn('UserManager/accounts/login/', response.url)

    def test_TakeDecisions_GET_ajax(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_TakeDecisions_GET_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = TakeDecisions(request)
        self.assertTemplateUsed('TakeDecisions.html')
        self.assertTrue(isinstance(response, HttpResponse))

    @mock.patch(
        "CommonsJustice.models.JusticeTask.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_TakeDecisions_LoadAllTasks_success(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTemplateUsed('TakeDecisions.html')
        self.assertEqual(JusticeTask.objects.filter.call_count, 3)
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "redirect")
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "script2")

    def test_TakeDecisions_redirectIsTrue_GET(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = TakeDecisions(request, redirect=True)
        self.assertTemplateUsed('TakeDecisions.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "redirect")
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "script2")

    @classmethod
    def tearDownClass(cls):
        super(TestTakeDecisionsView, cls).tearDownClass()


@pytest.mark.django_db
class TestLoadTaskView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestLoadTaskView, cls).setUpClass()
        cls.path = reverse('LoadTask')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.ct_obj = mixer.blend(Posopinion)
        cls.repport = mixer.blend(
            Repport,
            content_object=cls.ct_obj
        )

    def setUp(self):
        self.client.force_login(self.user)

    @mock.patch(
        "CommonsJustice.models.MessageRepport.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_LoadTask_authenticated_and_works(self):
        task = mixer.blend(
            JusticeTask,
            id=5,
            type_task=3,
            content_object=self.repport
        )
        response = self.client.get(
            self.path,
            {"slug": "task:" + str(task.id), },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('JusticeTask.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "main")

    def test_LoadTask_POST_error(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_LoadTask_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = LoadTask(request)

    def test_LoadTask_unauthenticated(self):
        client = Client()
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    @mock.patch(
        "CommonsJustice.models.MessageRepport.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_LoadTask_As_Jury(self):
        jury = mixer.blend(
            Jury,
            repport=self.repport
        )
        decision = mixer.blend(
            JugeDecision,
            jury=jury
        )
        task = mixer.blend(
            JusticeTask,
            id=5,
            type_task=1,
            content_object=decision
        )
        response = self.client.get(
            self.path,
            {"slug": "task:" + str(task.id), },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTemplateUsed('JusticeTask.html')
        self.assertTrue(MessageRepport.objects.filter.called)
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "main")

    @classmethod
    def tearDownClass(cls):
        super(TestLoadTaskView, cls).tearDownClass()


@pytest.mark.django_db
class TestPostRepportMessageView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestPostRepportMessageView, cls).setUpClass()
        cls.path = reverse('PostRepportMessage')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.ct_obj = mixer.blend(Posopinion)
        cls.repport = mixer.blend(
            Repport,
            content_object=cls.ct_obj
        )

    def setUp(self):
        self.client.force_login(self.user)

    def test_PostRepportMessage_unauthenticated(self):
        task = mixer.blend(
            JusticeTask,
            type_task=3,
            content_object=self.repport
        )
        client = Client()
        response = client.post(
            self.path,
            {"isjury": False,
             "taskid": task.id,
             "message": "blablabla",
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_PostRepportMessage_GET_error(self):
        task = mixer.blend(
            JusticeTask,
            type_task=3,
            content_object=self.repport
        )
        response = self.client.get(
            self.path,
            {"isjury": False,
             "taskid": task.id,
             "message": "blablabla",
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_PostRepportMessage_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = PostRepportMessage(request)

    @mock.patch(
        "CommonsJustice.models.MessageRepport.objects.create",
        mock.MagicMock(name="create"),
    )
    @mock.patch(
        "CommonsJustice.models.MessageRepport.objects.filter",
        mock.MagicMock(name="create"),
    )
    def test_PostRepportMessage_valid_and_IsNotJury(self):
        task = mixer.blend(
            JusticeTask,
            type_task=3,
            content_object=self.repport
        )
        response = self.client.post(
            self.path,
            {"isjury": False,
             "taskid": task.id,
             "message": "blablabla",
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('JusticeTask.html')
        self.assertContains(response, "messagepart")
        self.assertContains(response, "message")
        self.assertTrue(MessageRepport.objects.create.called)
        self.assertTrue(MessageRepport.objects.filter.called)
        self.assertTrue(isinstance(response, JsonResponse))

    @mock.patch(
        "CommonsJustice.models.MessageRepport.objects.create",
        mock.MagicMock(name="create"),
    )
    @mock.patch(
        "CommonsJustice.models.MessageRepport.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_PostRepportMessage__valid_and_IsJury(self):
        decision = mixer.blend(JugeDecision)
        jury = mixer.blend(Jury)
        jury.repport = self.repport
        decision.jury = jury
        task = mixer.blend(
            JusticeTask,
            type_task=1,
            content_object=decision
        )
        response = self.client.post(
            self.path,
            {"isjury": True,
             "taskid": task.id,
             "message": "blablabla",
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTemplateUsed('JusticeTask.html')
        self.assertTrue(MessageRepport.objects.create.called)
        self.assertTrue(MessageRepport.objects.filter.called)
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "messagepart")
        self.assertContains(response, "message")

    def test_PostRepportMessage_unvalid_form(self):
        decision = mixer.blend(JugeDecision)
        jury = mixer.blend(Jury)
        jury.repport = self.repport
        decision.jury = jury
        task = mixer.blend(
            JusticeTask,
            type_task=1,
            content_object=decision
        )
        response = self.client.post(
            self.path,
            {"isjury": False,
             "taskid": task.id,
             "message": "",
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertContains(response, "message")
        self.assertContains(response, "errors")
        self.assertEqual(
            content["message"],
            "form error"
        )

    @classmethod
    def tearDownClass(cls):
        super(TestPostRepportMessageView, cls).tearDownClass()


@pytest.mark.django_db
class TestAcceptRepportView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestAcceptRepportView, cls).setUpClass()
        cls.path = reverse('AcceptRepport')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.ct_obj = mixer.blend(Posopinion)

    def setUp(self):
        self.client.force_login(self.user)

    @mock.patch(
        "CommonsJustice.models.Repport.delete",
        mock.MagicMock(name="delete"),
    )
    def test_AcceptRepport_valid(self):
        repport = mixer.blend(
            Repport,
            content_object=self.ct_obj,
            delivered_to_jury=False,
        )
        response = self.client.post(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(Repport.delete.called)
        self.assertTemplateUsed('TakeDecisions.html')
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "message")

    def test_AcceptRepport_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = AcceptRepport(request)

    def test_AcceptRepport_unauthenticated(self):
        repport = mixer.blend(
            Repport,
            content_object=self.ct_obj,
            delivered_to_jury=False,
        )
        client = Client()
        response = client.post(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_AcceptRepport_GET_error(self):
        repport = mixer.blend(
            Repport,
            content_object=self.ct_obj,
            delivered_to_jury=False,
        )
        response = self.client.get(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    @mock.patch(
        "CommonsJustice.models.Repport.delete",
        mock.MagicMock(name="delete"),
    )
    def test_AcceptRepport_delivered_to_jury(self):
        repport = mixer.blend(
            Repport,
            content_object=self.ct_obj,
            delivered_to_jury=True,
        )
        response = self.client.post(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(Repport.delete.call_count, 0)
        self.assertTemplateUsed('TakeDecisions.html')
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "message")

    @classmethod
    def tearDownClass(cls):
        super(TestAcceptRepportView, cls).tearDownClass()


@pytest.mark.django_db
class TestDeleteRepportView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestDeleteRepportView, cls).setUpClass()
        cls.path = reverse('DeleteRepport')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.ct_obj = mixer.blend(Posopinion)

    def setUp(self):
        self.client.force_login(self.user)

    @mock.patch(
        "CommonsJustice.models.Repport.delete",
        mock.MagicMock(name="delete"),
    )
    @mock.patch(
        "CreateYourLaws.models.Posopinion.save",
        mock.MagicMock(name="save"),
    )
    def test_DeleteRepport_valid_delivered_to_jury_False(self):
        repport = mixer.blend(
            Repport,
            content_object=self.ct_obj,
            delivered_to_jury=False,
        )
        response = self.client.post(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(Posopinion.save.called)
        self.assertTrue(Repport.delete.called)
        self.assertTemplateUsed('TakeDecisions.html')
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "message")

    def test_DeleteRepport_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = DeleteRepport(request)

    def test_DeleteRepport_GET_error(self):
        repport = mixer.blend(
            Repport,
            content_object=self.ct_obj,
            delivered_to_jury=True,
        )
        response = self.client.get(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_DeleteRepport_unauthenticated_user(self):
        repport = mixer.blend(
            Repport,
            content_object=self.ct_obj,
            delivered_to_jury=True,
        )
        client = Client()
        response = client.post(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    @mock.patch(
        "CommonsJustice.models.Repport.delete",
        mock.MagicMock(name="delete"),
    )
    def test_DeleteRepport_delivered_to_jury(self):
        repport = mixer.blend(
            Repport,
            content_object=self.ct_obj,
            delivered_to_jury=True,
        )
        response = self.client.post(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(Repport.delete.call_count, 0)
        self.assertTemplateUsed('TakeDecisions.html')
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "message")

    @classmethod
    def tearDownClass(cls):
        super(TestDeleteRepportView, cls).tearDownClass()


@pytest.mark.django_db
class TestGetModifRepportView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGetModifRepportView, cls).setUpClass()
        cls.path = reverse('GetModifRepport')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()

    def setUp(self):
        self.client.force_login(self.user)

    def test_GetModifRepport_authenticated_user_success(self):
        repport = mixer.blend(
            Repport,
            comments="blablabla",
            reason=3,
        )
        response = self.client.get(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('CreateRepport.html')
        self.assertContains(response, "template")

    def test_GetModifRepport_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetModifRepport(request)

    def test_GetModifRepport_POST_error(self):
        repport = mixer.blend(
            Repport,
            comments="blablabla",
            reason=3,
        )
        response = self.client.post(
            self.path,
            {"slug": "repport:" + str(repport.id)},
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_GetModifRepport_unauthenticated_user(self):
        client = Client()
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    @classmethod
    def tearDownClass(cls):
        super(TestGetModifRepportView, cls).tearDownClass()


@pytest.mark.django_db
class TestPostModifRepportView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestPostModifRepportView, cls).setUpClass()
        cls.path = reverse('PostModifRepport')
        cls.ct_obj = mixer.blend(Posopinion)
        cls.repport = mixer.blend(
            Repport,
            content_object=cls.ct_obj
        )
        cls.task = mixer.blend(
            JusticeTask,
            content_object=cls.repport,
            type_task=3,
        )
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()

    def setUp(self):
        self.client.force_login(self.user)

    def test_PostModifRepport_authenticated(self):
        response = self.client.post(
            self.path,
            {'repportid': str(self.repport.id),
             'comments': "blabla",
             "reason": 1,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)

    def test_GetModifRepport_GET_error(self):
        response = self.client.get(
            self.path,
            {'repportid': str(self.repport.id),
             'comments': "blabla",
             "reason": 1,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_PostModifRepport_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = PostModifRepport(request)

    def test_PostModifRepport_unauthenticated_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = AnonymousUser()
        with self.assertRaises(Http404):
            response = PostModifRepport(request)

    def test_PostModifRepport_unauthenticated_ajax(self):
        client = Client()
        response = client.post(
            self.path,
            {'repportid': str(self.repport.id),
             'comments': "blabla",
             "reason": 1,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    @mock.patch(
        "CommonsJustice.models.Repport.save",
        mock.MagicMock(name="save"),
    )
    @mock.patch(
        "CommonsJustice.models.JusticeTask.objects.get",
        mock.MagicMock(name="get"),
    )
    @mock.patch(
        "CommonsJustice.models.MessageRepport.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_PostModifRepport_repport_modified(self):
        response = self.client.post(
            self.path,
            {'repportid': self.repport.id,
             'comments': "blabla",
             "reason": 1,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(Repport.save.called)
        self.assertTrue(JusticeTask.objects.get.called)
        self.assertContains(response, "message")
        self.assertTrue(MessageRepport.objects.filter.called)

    @mock.patch(
        "CommonsJustice.models.JusticeTask.save",
        mock.MagicMock(name="save"),
    )
    def test_PostModifRepport_unvalid_form(self):
        response = self.client.post(
            self.path,
            {'repportid': self.repport.id,
             'comments': "blabla",
             "reason": 0,
             },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        self.assertContains(response, "message")
        self.assertContains(response, "errors")
        self.assertEqual(
            content["message"],
            "form error"
        )

    @classmethod
    def tearDownClass(cls):
        super(TestPostModifRepportView, cls).tearDownClass()


@pytest.mark.django_db
class TestGoJuryView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGoJuryView, cls).setUpClass()
        cls.path = reverse('GoJury')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.DummyUsers = []
        for x in range(10):
            if x == 1:
                dummy = mixer.blend(
                    CYL_user,
                    username="government",
                )
            elif x in [2, 6]:
                dummy = mixer.blend(
                    CYL_user,
                    end_of_section=datetime.now(
                        tz=pytz.UTC)+timedelta(hours=180),
                )
            elif x in [3, 4, 5]:
                dummy = mixer.blend(
                    CYL_user,
                    end_of_section=datetime.now(
                        tz=pytz.UTC)-timedelta(hours=180),
                )
            else:
                dummy = mixer.blend(
                    CYL_user,
                    end_of_section=None,
                )
            cls.DummyUsers.append(dummy)
        ct_obj = mixer.blend(Posopinion, author=cls.DummyUsers[6])
        cls.repport = mixer.blend(
            Repport,
            content_object=ct_obj,
            delivered_to_jury=False,
        )
        ct = ContentType.objects.get_for_model(cls.repport)
        cls.task = mixer.blend(
            JusticeTask,
            content_type=ct,
            object_id=cls.repport.id,
            type_task=3,
        )

    def setUp(self):
        self.client.force_login(self.user)

    def test_GoJury_authenticated_user(self):
        response = self.client.get(
            self.path,
            {'slug': "repport:"+str(self.repport.id), },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "message")
        self.assertTrue(isinstance(response, JsonResponse))

    def test_GoJury_not_ajax(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GoJury(request)

    def test_GoJury_POST_error(self):
        response = self.client.post(
            self.path,
            {'slug': "repport:"+str(self.repport.id), },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_GoJury_unauthenticated(self):
        response = Client().get(
            self.path,
            {'slug': "repport:"+str(self.repport.id), },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    @classmethod
    def tearDownClass(cls):
        super(TestGoJuryView, cls).tearDownClass()


@pytest.mark.django_db
class TestPostJugeDecisionView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestPostJugeDecisionView, cls).setUpClass()
        cls.path = reverse('PostJugeDecision')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.JD = mixer.blend(JugeDecision)
        cls.task = mixer.blend(
            JusticeTask,
            content_object=cls.JD
        )

    def setUp(self):
        self.client.force_login(self.user)

    @mock.patch(
        "CommonsJustice.models.JugeDecision.save",
        mock.MagicMock(name="save"),
    )
    @mock.patch(
        "CommonsJustice.models.JusticeTask.delete",
        mock.MagicMock(name="delete"),
    )
    @mock.patch(
        "CommonsJustice.models.JusticeTask.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_PostJugeDecision_authenticated(self):
        response = self.client.post(
            self.path,
            {
                "taskid": self.task.id,
                "verdict": "True",
                "repport_abuse": "False",
                "comments": "blablabla",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)

    def test_PostJugeDecision_not_ajax(self):
        request = RequestFactory().post(
            self.path,
            {
                "taskid": self.task.id,
                "verdict": "True",
                "repport_abuse": "False",
                "comments": "blablabla",
            },
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = PostJugeDecision(request)

    def test_PostJugeDecision_GET_error(self):
        response = self.client.get(
            self.path,
            {
                "taskid": self.task.id,
                "verdict": "True",
                "repport_abuse": "False",
                "comments": "blablabla",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_PostJugeDecision_unauthenticated(self):
        client = Client()
        response = client.post(
            self.path,
            {
                "taskid": self.task.id,
                "verdict": "True",
                "repport_abuse": "False",
                "comments": "blablabla",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    @mock.patch(
        "CommonsJustice.models.JugeDecision.save",
        mock.MagicMock(name="save"),
    )
    @mock.patch(
        "CommonsJustice.models.JusticeTask.delete",
        mock.MagicMock(name="delete"),
    )
    @mock.patch(
        "CommonsJustice.models.JusticeTask.objects.filter",
        mock.MagicMock(name="filter"),
    )
    def test_PostJugeDecision_formValid(self):
        response = self.client.post(
            self.path,
            {
                "taskid": self.task.id,
                "verdict": "True",
                "repport_abuse": "False",
                "comments": "blablabla",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTemplateUsed('TakeDecisions.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "message")
        self.assertTrue(JugeDecision.save.called)
        self.assertTrue(JusticeTask.delete.called)
        self.assertEqual(JusticeTask.objects.filter.call_count, 3)

    def test_PostJugeDecision_formNotValid(self):
        response = self.client.post(
            self.path,
            {
                "taskid": "",
                "verdict": "",
                "repport_abuse": "",
                "comments": "",
            },
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertTemplateNotUsed('TakeDecisions.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertNotContains(response, "nav")
        self.assertNotContains(response, "main")
        self.assertContains(response, "message")
        self.assertContains(response, "errors")

    @classmethod
    def tearDownClass(cls):
        super(TestPostJugeDecisionView, cls).tearDownClass()


@pytest.mark.django_db
class TestYouArePunishedView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestYouArePunishedView, cls).setUpClass()
        cls.path = reverse('YouArePunished')
        cls.user = mixer.blend(
            CYL_user,
            end_of_sanction=datetime.now(tz=pytz.UTC) + timedelta(days=5)
        )
        cls.client = Client()

    def setUp(self):
        self.client.force_login(self.user)

    def test_YouArePunished_authenticated_and_Valid(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        content = eval(response.content)
        print(content["test"])
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "message")
        self.assertTrue(
            isinstance(
                datetime.strptime(
                    content["test"],
                    "%Y-%m-%d %H:%M:%S"),
                datetime
            )
        )

    def test_YouArePunished_unauthenticated(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')

    def test_YouArePunished_not_ajax(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = YouArePunished(request)

    @classmethod
    def tearDownClass(cls):
        super(TestYouArePunishedView, cls).tearDownClass()
