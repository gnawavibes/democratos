from django.urls import reverse, resolve


class TestUrls:

    def test_TakeDecisions_url(self):
        path = reverse('TakeDecisions')
        assert resolve(path).view_name == "TakeDecisions"

    def test_CreateRepport_url(self):
        path = reverse('CreateRepport')
        assert resolve(path).view_name == "CreateRepport"

    def test_takedecisions_url(self):
        path = reverse('takedecisions', kwargs={"redirect": 1})
        assert resolve(path).view_name == "takedecisions"

    def test_LoadRepportForm_url(self):
        path = reverse('LoadRepportForm')
        assert resolve(path).view_name == "LoadRepportForm"

    def test_LoadTask_url(self):
        path = reverse('LoadTask')
        assert resolve(path).view_name == "LoadTask"

    def test_PostRepportMessage_url(self):
        path = reverse('PostRepportMessage')
        assert resolve(path).view_name == "PostRepportMessage"

    def test_PostJugeDecision_url(self):
        path = reverse('PostJugeDecision')
        assert resolve(path).view_name == "PostJugeDecision"

    def test_AcceptRepport_url(self):
        path = reverse('AcceptRepport')
        assert resolve(path).view_name == "AcceptRepport"

    def test_DeleteRepport_url(self):
        path = reverse('DeleteRepport')
        assert resolve(path).view_name == "DeleteRepport"

    def test_GetModifRepport_url(self):
        path = reverse('GetModifRepport')
        assert resolve(path).view_name == "GetModifRepport"

    def test_PostModifRepport_url(self):
        path = reverse('PostModifRepport')
        assert resolve(path).view_name == "PostModifRepport"

    def test_YouArePunished_url(self):
        path = reverse('YouArePunished')
        assert resolve(path).view_name == "YouArePunished"

    def test_GoJury_url(self):
        path = reverse('GoJury')
        assert resolve(path).view_name == "GoJury"
