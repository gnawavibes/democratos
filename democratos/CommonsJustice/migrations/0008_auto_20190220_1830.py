# Generated by Django 2.1.7 on 2019-02-20 18:30

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('CommonsJustice', '0007_auto_20190220_1811'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jugedecision',
            name='jury',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to='CommonsJustice.Jury'),
        ),
        migrations.AlterField(
            model_name='jury',
            name='repport',
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE, to='CommonsJustice.Repport'),
        ),
        migrations.AlterField(
            model_name='justicetask',
            name='object_id',
            field=models.PositiveIntegerField(),
        ),
        migrations.AlterField(
            model_name='justicetask',
            name='worker',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
