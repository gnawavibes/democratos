from django.http import JsonResponse, HttpResponseRedirect
from UserManager.models import CYL_user
from CommonsJustice.models import JusticeTask


def user_has_no_job_to_do(function):
    def wrap(request, *args, **kwargs):
        u = request.user
        if not JusticeTask.objects.filter(worker=u, type_task=1):
            return function(request, *args, **kwargs)
        else:
            if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                return JsonResponse({'message': "user_has_job"})
            else:
                return HttpResponseRedirect('/CommonsJustice/TakeDecisions')
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
