from django.http import (
    HttpResponseRedirect, JsonResponse, HttpResponse, Http404
)
from django.urls import reverse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.template.loader import render_to_string
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q

from CreateYourLaws.views import home
from CommonsJustice.models import (
    Repport, Jury, JugeDecision, JusticeTask, MessageRepport,
    REASON_CHOICES,
)
from UserManager.models import CYL_user
from CommonsJustice.forms import (
    RepportForm, MessageForm, JugeDecisionForm
)
from democratos.decorators import ajax_login_required
from democratos.settings import NB_JURIES_PER_REPPORT
from django.contrib.contenttypes.models import ContentType
from CreateYourLaws.views_functions import (
    get_the_instance, get_model_type_in_str
)
from django.template.loader import render_to_string
from render_block import render_block_to_string
from django.utils.timezone import now
from datetime import datetime
import random
from django.views.decorators.http import require_POST, require_GET
from democratos import log
import json


@require_GET
@ajax_login_required
@user_passes_test(
    lambda u: not JusticeTask.objects.filter(worker=u, type_task=1),
    login_url='/CommonsJustice/takedecisions/1',
)
@user_passes_test(
    lambda u: u.end_of_sanction is None or now() > u.end_of_sanction,
    login_url='/CommonsJustice/YouArePunished',
)
def LoadRepportForm(request):
    """ View to load the repport Form template """
    log.info("Start LoadRepportForm")
    form = RepportForm()
    log.info("Stop LoadRepportForm")
    return JsonResponse(
        {"template": render_to_string('CreateRepport.html',
                                      locals(),
                                      request=request)}
    )


"""
@user_passes_test(
    lambda u: not JusticeTask.objects.filter(worker=u, type_task=1),
    login_url='/CommonsJustice/takedecisions/1',
)"""


@ajax_login_required
@require_POST
def CreateRepport(request):
    """ View to create a repport for an
    abusive behavior, create Jury instance and decision 
    instances to deliberate """
    log.info("Start CreateRepport")
    log.debug(request)
    user = request.user
    typeref = request.POST.get('typeref', None)
    idref = int(request.POST.get('idref', None))
    obj = get_the_instance(typeref, idref)
    ct = ContentType.objects.get_for_model(obj)
    form = RepportForm(request.POST)
    log.debug(form)
    if form.is_valid():
        # Creation of the Repport
        com = form.cleaned_data['comments']
        repport = Repport.objects.create(
            repporter=request.user,
            object_id=obj.id,
            content_type=ct,
            reason=form.cleaned_data['reason'],
            comments=form.cleaned_data['comments'],
        )
        # Creation of the Sender JusticeTask
        task_sender = JusticeTask.objects.create(
            worker=request.user,
            type_task=3,
            content_object=repport,
        )
        # Creation of the Reciever JusticeTask
        task_reciever = JusticeTask.objects.create(
            worker=obj.author,
            type_task=2,
            content_object=repport,
        )
        # Set the object as censured
        obj.is_censured = True
        obj.save()
        message = "Votre Signalement a bien été enregistré"
        ctx = {'message': message}
    else:
        log.error(form.errors)
        ctx = {'message': "form error",
               'errors': json.dumps(form.errors),
               }

    log.info("Stop CreateRepport")
    return JsonResponse(ctx)


@login_required
def TakeDecisions(request, redirect=False):
    """ Create a form for juges to take decisions
    considering a Repport """
    redirect = bool(redirect)
    log.debug(("request:", request, " redirect: ", redirect))
    user = request.user
    tasks1 = JusticeTask.objects.filter(worker=user, type_task=1)
    tasks2 = JusticeTask.objects.filter(worker=user, type_task=2)
    tasks3 = JusticeTask.objects.filter(worker=user, type_task=3)
    if request.method == "POST" or redirect:
        nav = render_block_to_string('TakeDecisions.html',
                                     "nav",
                                     locals())
        main = render_block_to_string('TakeDecisions.html',
                                      "Main",
                                      locals())
        script2 = render_block_to_string('TakeDecisions.html',
                                         "script2",
                                         locals())
        ctx = {'redirect': redirect,
               'nav': nav,
               'main': main,
               'script2': script2}
        return JsonResponse(ctx)
    else:
        if request.headers.get('x-requested-with') == 'XMLHttpRequest':
            raise Http404
        template = render_to_string('TakeDecisions.html', locals())
        index = template.find('id="GoCommonJustice"')
        template = template[:index-2] + " active" + template[index-2:]
        return HttpResponse(template)


@ajax_login_required
@require_GET
def LoadTask(request):
    log.debug(request.GET)
    slug = request.GET.get('slug', None)
    taskid = int(slug[5:])
    Task = JusticeTask.objects.get(id=taskid)
    log.debug(Task.content_object, Task.type_task)
    if Task.type_task in [2, 3]:
        repport = Task.content_object
        messages = MessageRepport.objects.filter(repport=repport,
                                                 is_jury=False)
        isjury = False
    else:
        DecisionForm = JugeDecisionForm()
        repport = Task.content_object.jury.repport
        messages = MessageRepport.objects.filter(repport=repport)
        isjury = True
    typeref = get_model_type_in_str(repport.content_object)
    reason = REASON_CHOICES[repport.reason-1][1]
    messageform = MessageForm()
    main = render_block_to_string('JusticeTask.html',
                                  "Main",
                                  locals())
    ctx = {'main': main}
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
def PostRepportMessage(request):
    try:
        isjury = request.POST.get("isjury")
        if isjury == "False":
            isjury = False
        else:
            isjury = True
        taskid = int(request.POST.get("taskid"))
        log.debug(isjury, type(isjury), taskid)
        messageform = MessageForm(request.POST)
        if messageform.is_valid():
            msg = messageform.cleaned_data["message"]
            if msg != "":
                Task = JusticeTask.objects.get(id=taskid)
                if isjury:
                    msgtosave = MessageRepport.objects.create(
                        repport=Task.content_object.jury.repport,
                        author=request.user,
                        message=msg,
                        is_jury=True,
                    )
                else:
                    msgtosave = MessageRepport.objects.create(
                        repport=Task.content_object,
                        author=request.user,
                        message=msg,
                        is_jury=False,
                    )
                message = ""
            else:
                message = "message vide"
        else:
            log.error(messageform.errors)
            ctx = {'message': "form error",
                   'errors': json.dumps(messageform.errors),
                   }
            return JsonResponse(ctx)
    except:
        message = "Erreur lors de l'enregistrement du message"
    messageform = MessageForm()
    try:
        messages = MessageRepport.objects.filter(repport=Task.content_object)
    except:
        messages = MessageRepport.objects.filter(
            repport=Task.content_object.jury.repport)
    messagepart = render_block_to_string('JusticeTask.html',
                                         "message",
                                         locals())
    ctx = {'messagepart': messagepart,
           'message': message,
           }
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
def AcceptRepport(request):
    try:
        slug = request.POST.get('slug', None)
        repport_id = slug[8:]
        repport = Repport.objects.get(id=repport_id)
        if repport.delivered_to_jury:
            message = "Le signalement a été soumis au jury." +\
                "</br> impossible de l'accepter à présent'."
        else:
            obj = repport.content_object
            obj.delete()
            repport.delete()
            message = "Votre réflection et le signalement " +\
                      "correspondant ont bien été supprimés."
    except:
        message = "Echec de la suppression de la réflection " +\
                  "et du signalement."
    user = request.user
    tasks1 = JusticeTask.objects.filter(worker=user, type_task=1)
    tasks2 = JusticeTask.objects.filter(worker=user, type_task=2)
    tasks3 = JusticeTask.objects.filter(worker=user, type_task=3)
    nav = render_block_to_string('TakeDecisions.html',
                                 "nav",
                                 locals())
    main = render_block_to_string('TakeDecisions.html',
                                  "Main",
                                  locals())
    ctx = {'nav': nav,
           'main': main,
           'message': message,
           }
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
def DeleteRepport(request):
    try:
        slug = request.POST.get('slug', None)
        repport_id = slug[8:]
        repport = Repport.objects.get(id=repport_id)
        if repport.delivered_to_jury:
            message = "Le signalement a été soumis au jury." +\
                "<br> impossible de le supprimer."
        else:
            obj = repport.content_object
            obj.is_censured = False
            obj.save()
            repport.delete()
            message = "Votre signalement a bien été supprimé."
    except:
        message = "Echec de la suppression du signalement."
    user = request.user
    tasks1 = JusticeTask.objects.filter(worker=user, type_task=1)
    tasks2 = JusticeTask.objects.filter(worker=user, type_task=2)
    tasks3 = JusticeTask.objects.filter(worker=user, type_task=3)
    nav = render_block_to_string('TakeDecisions.html',
                                 "nav",
                                 locals())
    main = render_block_to_string('TakeDecisions.html',
                                  "Main",
                                  locals())
    ctx = {'nav': nav,
           'main': main,
           'message': message,
           }
    return JsonResponse(ctx)


@ajax_login_required
@require_GET
def GetModifRepport(request):
    slug = request.GET.get('slug', None)
    repport_id = slug[8:]
    repport = Repport.objects.get(id=repport_id)
    form = RepportForm(initial={
        "reason": repport.reason,
        "comments": repport.comments,
    })
    return JsonResponse(
        {"template": render_to_string('CreateRepport.html',
                                      locals(),
                                      request=request)}
    )


@ajax_login_required
@require_POST
def PostModifRepport(request):
    repportid = int(request.POST.get("repportid"))
    repport = Repport.objects.get(id=repportid)
    repportform = RepportForm(request.POST)
    ct = ContentType.objects.get_for_model(repport)
    Task = JusticeTask.objects.get(content_type=ct,
                                   object_id=repportid,
                                   type_task=3,
                                   )
    if repportform.is_valid():
        repport.reason = repportform.cleaned_data["reason"]
        repport.comments = repportform.cleaned_data["comments"]
        repport.save()
        reason = repport.reason
        messages = MessageRepport.objects.filter(repport=repport)
        isjury = False
        typeref = get_model_type_in_str(repport.content_object)
        reason = REASON_CHOICES[repport.reason-1][1]
        messageform = MessageForm()
        main = render_block_to_string('JusticeTask.html',
                                      "Main",
                                      locals())
        ctx = {'message': "Ok", 'main': main}

    else:
        ctx = {'message': "form error",
               'errors': json.dumps(repportform.errors),
               }
    return JsonResponse(ctx)


@ajax_login_required
@require_GET
def GoJury(request):
    log.debug(request)
    slug = request.GET.get('slug', None)
    log.debug(slug)
    repport_id = slug[8:]
    repport = Repport.objects.get(id=repport_id)
    # créer un jury
    jury = Jury.objects.create(
        repport=repport,
    )
    # sélectionner des juges, créer et attribuer des décisions
    nb_clean_user = CYL_user.objects.filter(
        Q(end_of_sanction=None) | Q(end_of_sanction__lt=now())
    ).count()
    JuryMembers = []
    for x in range(NB_JURIES_PER_REPPORT):
        winer = random.randint(0, nb_clean_user-1)
        winer = CYL_user.objects.filter(
            Q(end_of_sanction=None) | Q(end_of_sanction__lt=now())
        )[winer]
        if (winer in JuryMembers or
            winer is repport.repporter or
            winer is repport.content_object.author or
            winer.is_superuser or
                winer.username == "government"):
            x -= 1
        else:
            JuryMembers.append(winer)
            new_decision = JugeDecision.objects.create(
                jury=jury,
            )
            task = JusticeTask.objects.create(
                worker=winer,
                type_task=1,
                content_object=new_decision,
            )
    repport.delivered_to_jury = True
    repport.save()
    message = "Le signalement suivant a bien été " +\
        "enregistré <br> et a été soumis à un jury."
    ctx = {"message": message}
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
def PostJugeDecision(request):
    log.debug(("PostJugeDecision", request.POST))
    JDform = JugeDecisionForm(request.POST)
    if JDform.is_valid():
        taskid = int(request.POST.get("taskid"))
        Task = JusticeTask.objects.get(id=taskid)
        JD = Task.content_object
        log.debug(JD)
        JD.verdict = bool(JDform.cleaned_data["verdict"])
        JD.repport_abuse = bool(JDform.cleaned_data["repport_abuse"])
        JD.comments = str(JDform.cleaned_data["comments"])
        JD.decision_made = now()
        JD.save()
        Task.delete()
        user = request.user
        tasks1 = JusticeTask.objects.filter(worker=user, type_task=1)
        tasks2 = JusticeTask.objects.filter(worker=user, type_task=2)
        tasks3 = JusticeTask.objects.filter(worker=user, type_task=3)
        nav = render_block_to_string('TakeDecisions.html',
                                     "nav",
                                     locals())
        main = render_block_to_string('TakeDecisions.html',
                                      "Main",
                                      locals())
        ctx = {'nav': nav,
               'main': main,
               "message": "Merci d'avoir délibéré."}
    else:
        log.error("Erreur dans le Formulaire!")
        ctx = {
            "message": "form error",
            'errors': json.dumps(JDform.errors),
        }
    log.info("PostJugeDecision Terminé")
    return JsonResponse(ctx)


@ajax_login_required
def YouArePunished(request):
    """ Display a message telling that the User is 
    punished, why and for how long"""
    date = datetime.strftime(
        request.user.end_of_sanction,
        "%Y-%m-%d %H:%M:%S",
    )
    message = ("vous avez été sanctionné pour votre comportement" +
               " et ce jusqu'au " + date)
    log.debug(message)
    ctx = {"message": message, "test": date}
    return JsonResponse(ctx)
