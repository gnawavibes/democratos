from django.apps import AppConfig


class CommonsJusticeConfig(AppConfig):
    name = 'CommonsJustice'

    def ready(self):
        from CommonsJustice import signals
