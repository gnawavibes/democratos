# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import CreateYourLaws
from django import template

register = template.Library()


@register.filter
def CreateJusticeTitle(obj):
    print(obj)
    if type(obj) is CreateYourLaws.models.Question:
        Csstype = "Question"
    elif type(obj) is CreateYourLaws.models.Explaination:
        Csstype = "Commentaire"
    elif type(obj) is CreateYourLaws.models.Posopinion:
        Csstype = "Point positif"
    elif type(obj) is CreateYourLaws.models.Negopinion:
        Csstype = "Point négatif"
    elif type(obj) is CreateYourLaws.models.Proposition:
        Csstype = "Proposition de loi"
    elif type(obj) is CreateYourLaws.models.LawArticle:
        Csstype = "Texte de loi"
    elif type(obj) is CreateYourLaws.models.Commit:
        Csstype = "Commit"
    elif type(obj) is CreateYourLaws.models.SourceLink:
        Csstype = "Lien"
    return Csstype + " n° " + str(obj.id)
