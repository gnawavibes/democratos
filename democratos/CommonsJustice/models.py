from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from UserManager.models import CYL_user
from ckeditor.fields import RichTextField
from django.utils.translation import gettext as _


REASON_CHOICES = (
    (1, _("Discrimination (sexisme,racisme,dsicrimination religieuse...)")),
    (2, _("Diffamation, calomnie envers un individu ou un groupe d'individu")),
    (3, _("Injures")),
    (4, _("Atteinte à la vie privée d'un individu")),
    (5, _("Autres (précisez en commentaire...)"))
)


class Repport(models.Model):
    date_open = models.DateTimeField(auto_now_add=True)
    date_close = models.DateTimeField(null=True)
    repporter = models.ForeignKey(
        CYL_user, null=True, on_delete=models.SET_NULL)
    reason = models.IntegerField(choices=REASON_CHOICES)
    comments = RichTextField(null=True)
    delivered_to_jury = models.BooleanField(default=False)
    repporter_task = GenericRelation("JusticeTask")
    reciever_task = GenericRelation("JusticeTask")
    content_type = models.ForeignKey(ContentType,
                                     null=True,
                                     on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class Jury(models.Model):
    repport = models.OneToOneField(Repport, on_delete=models.CASCADE)
    verdict = models.BooleanField(null=True, default=None)
    repport_abuse = models.BooleanField(default=False)
    creation_date = models.DateTimeField(auto_now_add=True)
    decision_date = models.DateTimeField(null=True)


class JugeDecision(models.Model):
    juge_task = GenericRelation("JusticeTask")
    jury = models.ForeignKey(Jury, on_delete=models.CASCADE)
    decision_asked = models.DateTimeField(auto_now_add=True)
    decision_made = models.DateTimeField(null=True)
    verdict = models.BooleanField(null=True)
    repport_abuse = models.BooleanField(null=True)
    comments = RichTextField(null=True, blank=True)


TYPE_JUSTICE_TASK = (
    (1, _("JugeDecision to make")),
    (2, _("Repport reciever")),
    (3, _("Repport Send")),
)


class JusticeTask(models.Model):
    worker = models.ForeignKey(CYL_user, on_delete=models.CASCADE)
    type_task = models.IntegerField(choices=TYPE_JUSTICE_TASK)
    content_type = models.ForeignKey(ContentType,
                                     null=True,
                                     on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


class MessageRepport(models.Model):
    repport = models.ForeignKey(Repport, on_delete=models.CASCADE)
    author = models.ForeignKey(CYL_user, on_delete=models.CASCADE)
    is_jury = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    message = RichTextField(null=False, blank=False)
