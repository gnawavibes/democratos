from django.db.models.signals import post_save, post_delete, post_init
from django.dispatch import receiver
from CommonsJustice.models import (
    JusticeTask, JugeDecision, Jury, Repport,
)
from UserManager.models import CYL_user
from democratos.settings import NB_JURIES_PER_REPPORT
from django.utils.timezone import now
from random import Random
from datetime import timedelta


@receiver(post_save, sender=JugeDecision)
def CheckAndValidJuryDecision(sender, instance, **kwargs):
    """ Check if all members of the Jury have taken 
    their decisions and then  execute global verdict """
    print("Signal capté!")
    Jury = instance.jury
    repport = Jury.repport
    JDs = JugeDecision.objects.filter(jury=Jury)
    print(JDs,
          "\n\n filtré décision non rendue:\n",
          JDs.filter(decision_made=None))
    if not JDs.filter(decision_made=None):
        if JDs.filter(verdict=True).count() > NB_JURIES_PER_REPPORT*2/3:
            Jury.verdict = True
        else:
            Jury.verdict = False
        if JDs.filter(repport_abuse=True).count() > NB_JURIES_PER_REPPORT*2/3:
            Jury.repport_abuse = True
        else:
            Jury.repport_abuse = False
        Jury.decision_date = now()
        Jury.save()
        print("Jury: ", Jury.verdict, Jury.repport_abuse)
        if Jury.verdict:
            PunishUser(Jury.repport.content_object.author)
        if Jury.repport_abuse:
            PunishUser(Jury.repport.repporter)


def PunishUser(user):
    """ Recover the number of convictions of the user
    during the last year and adapt the sentence"""
    print("in PunishUser!")
    rank = 0
    # here we recover conviction for bad behaviour
    nb_ref = list(user.explaination_set.all()) + \
        list(user.question_set.all()) + \
        list(user.proposition_set.all()) + \
        list(user.posopinion_set.all()) + \
        list(user.negopinion_set.all()) + \
        list(user.lawarticle_set.all())
    for el in nb_ref:
        for rep in el.repportjobs.all():
            try:
                if (rep.jury and
                    rep.jury.verdict and
                        rep.jury.decision_date-now() < timedelta(365, 0, 0)):
                    rank += 1
            except:
                print(rep, " has not been submited to jury yet")
    # here we recover conviction for abusive repport
    for rep in Repport.objects.filter(repporter=user):
        try:
            if (rep.jury and
                rep.jury.repport_abuse and
                    rep.jury.decision_date-now() < timedelta(365, 0, 0)):
                rank += 1
        except:
            print(rep, " has not been submited to jury yet")
    # the rank (number of bad behaviour + abusive repport during
    # last year) give the sanction
    if rank == 1:
        penalty = timedelta(1, 0, 0)
    elif rank == 2:
        penalty = timedelta(3, 0, 0)
    elif rank == 3:
        penalty = timedelta(7, 0, 0)
    elif rank == 4:
        penalty = timedelta(30, 0, 0)
    elif rank == 5:
        penalty = timedelta(90, 0, 0)
    elif rank == 6:
        penalty = timedelta(183, 0, 0)
    # The sanction: if the user is already sanctioned,
    # the penalty is added to the previous sanction
    if user.end_of_sanction == None:
        user.end_of_sanction = now()+penalty
    else:
        user.end_of_sanction += penalty
    user.save()
    print("the user ", user, " has been punished")
