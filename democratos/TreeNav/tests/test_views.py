from django.urls import reverse
from django.test import RequestFactory, TestCase
from django.http import JsonResponse, HttpResponseNotAllowed
from django.contrib.auth.models import AnonymousUser
from django.http.response import Http404
from mixer.backend.django import mixer
import pytest
from unittest import mock

from TreeNav.views import *
from CreateYourLaws.models import (
    LawCode, LawArticle, Explaination, CodeBlock,
    Posopinion, Negopinion, Proposition, Question,
)
from CreateYourLaws.views_functions import get_the_instance
from UserManager.models import CYL_user
import string
from random import choice
from datetime import datetime


@pytest.mark.django_db
class Testget_refView(TestCase):
    def setUp(self):
        self.lawart = mixer.blend(LawArticle)

    def test_get_ref(self):
        returned = get_ref("C", self.lawart.id)
        self.assertTrue(isinstance(returned, LawArticle))


@pytest.mark.django_db
class Testnav_upView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(Testnav_upView, cls).setUpClass()
        cls.law = mixer.blend(LawArticle, id=1, abrogated=None)
        cls.law2 = mixer.blend(LawArticle, id=1, abrogated=datetime.now())
        cls.lawcode = mixer.blend(LawCode, id=1)
        cls.exp = mixer.blend(Explaination, id=1)
        cls.block = mixer.blend(CodeBlock, id=1)
        cls.opp = mixer.blend(Posopinion, id=1)
        cls.opn = mixer.blend(Negopinion, id=1)
        cls.prp = mixer.blend(Proposition, id=1)
        cls.qst = mixer.blend(Question, id=1)

    def test_nav_up_success_all_cases(self):
        for el in list(string.ascii_uppercase[:21]):
            if el in ['H', 'I', 'K']:  # A modifier lorsque K sera codé <======================
                continue
            else:
                idbox = el+'1'
                if el in ['L', 'M', 'N']:
                    idbox += choice(['C', 'Q'])
                elif el in ['O', 'P']:
                    idbox += choice(['C', 'Q', 'R', 'S', 'T', 'U'])
                path = reverse('nav_up', args=(idbox,))
                print(path)
                request = RequestFactory().get(path)
                for user in [AnonymousUser(), mixer.blend(CYL_user)]:
                    request.user = user
                    response = nav_up(request, idbox)
                    print(response, response.content,
                          user, user.is_authenticated)
                    self.assertTrue(isinstance(response, JsonResponse))
                    if (el in ['L', 'M', 'N', 'O', 'P'] or
                            (not user.is_authenticated and
                             el in ['D', 'E', 'F', 'G'])):
                        self.assertEqual(response.content, b'[]')
                    else:
                        self.assertContains(response, "id")
                        self.assertContains(response, "text")
                        self.assertContains(response, "icon")
                        self.assertContains(response, "a_attr")
                        self.assertContains(response, "children")
        path = reverse('nav_up', args=("root",))
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "id")
        self.assertContains(response, "text")
        self.assertContains(response, "icon")
        self.assertContains(response, "a_attr")
        self.assertContains(response, "children")

    def test_nav_up_bad_idbox(self):
        for idbox in ['AX', '1', 'BB1', 'mojo', 'M1G7', 'A1C', 'N1', 'O1Z']:
            path = reverse('nav_up', args=(idbox,))
            print(path)
            request = RequestFactory().get(path)
            with self.assertRaises(Http404):
                response = nav_up(request, idbox)

    def test_nav_up_POST_request(self):
        path = reverse('nav_up', args=("C1",))
        request = RequestFactory().post(path)
        request.user = AnonymousUser()
        response = nav_up(request)
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    @classmethod
    def tearDownClass(cls):
        super(Testnav_upView, cls).tearDownClass()
