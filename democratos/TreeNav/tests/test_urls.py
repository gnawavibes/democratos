from django.urls import reverse, resolve


class TestUrls:

    def test_nav_up_url(self):
        path = reverse('nav_up', kwargs={"idbox": "A1"})
        assert resolve(path).view_name == "nav_up"
