// -------------- Secondary JStree settings ---------------------
function CreateTree(name, firstnode="#"){
    $("#" + name).jstree({
        'core' : {
            'data' : {
                'url' : function (node, fisrtnode) {
                    if (node.id === "#"){
                        //console.log("Create Tree ", firstnode, node.id);
                        var url = firstnode === "#" ?
                            '/Tree/nav_up/root' :   
                            '/Tree/nav_up/' + firstnode;
                        return url;
                    }
                    else{
                        return '/Tree/nav_up/' + node.id;
                    }
                },
                'data' : function (node) {
                    return { "id" : node.id,
                             "text": node.text,
                            };
                },
                "plugins":[
                    "wholerow",
                ],
            }
        }
    });
    $("#" + name).on("loaded.jstree after_open.jstree", function(e,data){
        $(this).find("li").each(function(){
            //console.log($(this))
            var li_id = $(this).attr("id");
            if(["A","B","C","D","E","F","G","J","K","L","M","N","O","P","Q","R","S","T","U"].includes(li_id[0])){
                if (!($(this).find(".totabico").length)){
                    var totabico = $("#totabcopy").clone()
                    totabico.appendTo($(this))
                };
                $(this).mouseenter(function(e){
                    $(this).find(".totabico").first().css("visibility","visible");
                });
                $(this).mouseleave(function(e){
                    $(this).find(".totabico").first().css("visibility","hidden");
                });
            };
        });
        SetAddTab();
        SetSubTab();
        manageDisplayAbrogated()
    });
    $("#" + name).height($(window).height()-$("#jstree_CYL").position().top-70);  
    console.log("jstree " + name + " loaded");
};

function SetupTab(idname){
    $('#' + idname).click(function (e) {
        e.preventDefault();
        $(this).tab('show')
    })
};


function SetAddTab(){ // Add a new tree in a tab
    $('#addtab').each(function(){
        $(this).off("click").on("click",function (e) {
            e.preventDefault();
            console.log($(this));
            var num = 1;
            while ($("#tab"+ num).length != 0){
                num++;
            };
            var divtemplate = `<div id="jstree${num}" class="tab-pane fade active show"><div id="jstree_CYL${num}"  class="TreeNav ui-widget-content" style="padding:1px;"></div>`;
            var tabtemplate = `<li role="presentation" class="nav-item"><a id="tab${num}" class="nav-link deletabletab" href="#jstree${num}"><span style="padding-right: 7px;">Tab ${num}</span></a></li>`;
            $(tabtemplate).insertBefore($("#addtab").parent());
            var closeico = $("#closecopy").clone()
            closeico.appendTo($("#tab"+num))
            $("#treediv").append(divtemplate);
            $(".closeico").each(function(){
                $(this).click(function(e){
                    e.preventDefault();
                    console.log("on go close:", $("#hometab"))
                    //$('a[href="#tree_main"]').tab('show');
                    $("#hometab").tab('show');
                    var treetodel = $(this).parent().attr('href');
                    console.log("tree to del: " + treetodel);
                    $(treetodel).remove();
                    $(this).parent().parent().remove();
                });
            });
            CreateTree("jstree_CYL"+num);
            SetupTab("tab"+num);
            $("#tab"+num).tab('show');
        })
    });
};

function SetSubTab(){ // Create Subtab
    $('.totabico').each(function(){
        $(this).off("click").on("click",function (e) {
            e.preventDefault();
            var num = 1;
            while ($("#sub"+ num).length != 0){
                num++;
            };
            var divtemplate = `<div id="subjstree${num}" class="tab-pane fade active show"><div id="subjstree_CYL${num}"  class="TreeNav ui-widget-content" style="padding:1px;"></div>`;
            var tabtemplate = `<li role="presentation" class="nav-item"><a id="sub${num}" class="nav-link deletabletab" href="#subjstree${num}"><span style="padding-right: 7px;">Sub ${num}</span></a></li>`;
             $(tabtemplate).insertBefore($("#addtab").parent());
            var closeico = $("#closecopy").clone()
            closeico.appendTo($("#sub"+num))
            $("#treediv").append(divtemplate);
            $(".closeico").each(function(){
                $(this).click(function(e){
                    e.preventDefault();
                    console.log("on go close:", $("#hometab"))
                    //$('a[href="#tree_main"]').tab('show');
                    $("#hometab").tab('show');
                    var treetodel = $(this).parent().attr('href');
                    console.log("tree to del: " + treetodel);
                    $(treetodel).remove();
                    $(this).parent().parent().remove();
                });
            });
            var parenttree = $(this).parents(".TreeNav").jstree(true); 
            var nodetocreate = parenttree.get_node($(this).parents("li").first());
            CreateTree("subjstree_CYL"+num, firstnode=nodetocreate.id);
            var treehead = $(this).prev().clone();
            // finally we add the subtree head:
            $("#subjstree_CYL"+num).on('loaded.jstree',function(){$("#subjstree_CYL"+num).prepend(treehead)});
            SetupTab("sub"+num);
            $("#sub"+num).tab('show');
        })
    });
};

function setCYLtreesOnNavWidth(){
    $("#CYLtrees").css("display","block");
    $("#CYLtrees").width($("nav").width())
    $("#CYLtrees").css("display","fixed");
}

function manageDisplayAbrogated(){
    if ($("#CheckboxAbrogated").is(':checked')){
        $(".abrogated").parent("li").css("display","block");
    }
    else{
        $(".abrogated").parent("li").css("display","none");
    }
}

$(document).ready(function() {
    // --------------- resizable nav -----------------------
    var container = $("body");
    var numberOfCol = 2;
    var sibTotalWidth;
    var MxWdth = $("body").width() * 0.58;
    var MnWdth = $("body").width() * 0.15;
    setCYLtreesOnNavWidth()
    $("nav").resizable({
        maxWidth: MxWdth,
        minWidth: MnWdth,
        handles: 'e',
        start: function(event, ui){
            sibTotalWidth = ui.originalSize.width + $("#main").outerWidth();
            setCYLtreesOnNavWidth();
        },
        stop: function(event, ui){    
            var cellPercentWidth=100 * ui.originalElement.outerWidth()/ container.innerWidth();
            ui.originalElement.css('width', cellPercentWidth + '%'); 
            var nextPercentWidth=100 * $("#main").outerWidth()/container.innerWidth();
            $("#main").css('width', nextPercentWidth + '%');
            setCYLtreesOnNavWidth();
        },
        resize: function(event, ui){
            ui.originalElement.next().width(sibTotalWidth - ui.size.width);
            setCYLtreesOnNavWidth();
        }
    });
    $("#CYLtrees").ready(function(){ //<--------- A TERMINER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // In order to fix footer covering nav issue 
        var iniHeight = $("#CYLtrees").height();
        $(window).scroll(function(){
            //console.log($("#CYLtrees").offset().top + iniHeight, $("footer").offset().top ,$("#CYLtrees").height());
            if ($("#CYLtrees").offset().top + iniHeight > ($("footer").offset().top)){
                var delta = $("#CYLtrees").offset().top + iniHeight - ($("footer").offset().top);
                //console.log("Delta", delta);
                $("#CYLtrees").css("display","inline-block");
                $("#CYLtrees").height(Math.round(iniHeight - delta));
                //console.log("CYL height", $("#CYLtrees").height());
                //$("#CYLtrees").css("display","fixed");
            };
        });
    });
    // -------------- Main JStree settings ---------------------
    //ENABLE TREES
    CreateTree("jstree_CYL"); 
    // ENABLE THE TABS
    SetupTab("hometab");
    // ADD a New Tree
    console.log("jstree.js loaded");
    // -------------------- Enable/Disable Abrogated --------------
    $("#CheckboxAbrogated").on("change",function(e){
        manageDisplayAbrogated()
    });
});

