# -*-coding: utf-8 -*-

from django.urls import path
from . import views


urlpatterns = [
    path('nav_up/<str:idbox>',
         views.nav_up,
         name="nav_up"),
]
