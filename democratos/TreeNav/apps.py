from django.apps import AppConfig


class TreenavConfig(AppConfig):
    name = 'TreeNav'
