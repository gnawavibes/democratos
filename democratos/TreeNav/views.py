from django.http import JsonResponse, Http404
from CreateYourLaws.models import (
    LawCode, LawArticle, Explaination, CodeBlock,
    Posopinion, Negopinion, Proposition, Question,
)
from CreateYourLaws.views_functions import get_the_instance
from bs4 import BeautifulSoup
from django.views.decorators.http import require_GET


"""ajax for CYL nav: update the tree
note:
- A: the node is a Law Code
- B: the node is a code bloc
- C: the node is a Law Article
- D: the node is to see New Law proposition directly for a Law Code
- E: the node is to see New Law proposition for a specific Codeblock
- F: the node is to see code block proposition directly for a Law Code
- G: the node is to code block proposition for a specific Codeblock
- H: Create New Law
- I: Create New Boxes
- J: New Law proposition
- K: New Bloc Prop
- L: Prop folder
- M: Positive point folder
- N: Negative point folder
- O: Explaination folder
- P: Question folder
- Q: Prop
- R: Positive point
- S: Negative point
- T: Explaination
- U: Question
"""


REFTYPE = {
    'C': 'law',
    'J': 'law',
    'L': 'prp',
    'M': 'opp',
    'N': 'opn',
    'O': 'exp',
    'P': 'qst',
    'Q': 'prp',
    'R': 'opp',
    'S': 'opn',
    'T': 'exp',
    'U': 'qst',
}

###########################################################################
# ############################## main tree ################################
###########################################################################


def get_ref(box, id):
    typeref = REFTYPE[box]
    return get_the_instance(typeref, id)


@require_GET
def nav_up(request, idbox):
    print(idbox)
    if idbox != 'root':
        try:
            if idbox[0] in ['L', 'M', 'N', 'O', 'P']:
                id_box = int(idbox[1:len(idbox)-1])
            else:
                id_box = int(idbox[1:len(idbox)])
        except:
            raise Http404("bad idbox encription (TreeNav view)")
    children = []
    if idbox == "root":
        law_codes = list(LawCode.objects.all().order_by("id"))
        for i, el in enumerate(law_codes):
            # 'A' in the 'id' param inform that this is a Law code
            htmlclass = "InDatBox"
            title = el.title
            if el.abrogated:
                htmlclass += " abrogated"
                if "(abrogé)" not in title:
                    title += " (abrogé)"
            children.append(('A' + str(el.id),
                             title,
                             "/static/icons/code.png",  # law-book.png",
                             htmlclass,
                             "1:" + str(el.id),
                             True))

    elif idbox[0] == 'A' or idbox[0] == 'B':
        # Get all the current legislation and add link to prop
        if idbox[0] == 'A':  # If we are directly in a Lawcode not Codeblock
            listArticle = list(
                LawArticle.objects.filter(law_code=id_box,
                                          block_id__isnull=True,
                                          is_lwp=False).order_by('id'))
            typeboxnewlaw = 'D'
            typeboxnewbox = 'F'
        elif idbox[0] == 'B':
            listArticle = list(
                LawArticle.objects.filter(block=id_box,
                                          is_lwp=False).order_by('id'))
            typeboxnewlaw = 'E'
            typeboxnewbox = 'G'
        if listArticle:
            for el in listArticle:
                htmlclass = "GetReflection"
                title = el.title
                if el.abrogated:
                    htmlclass += " abrogated"
                    if "(abrogé)" not in title:
                        title += " (abrogé)"
                children.append(('C' + str(el.id),
                                 title,
                                 "/static/icons/article.png",
                                 htmlclass,
                                 'law:' + str(el.id),
                                 True))
        if idbox[0] == 'A':
            listBlock = list(
                CodeBlock.objects.filter(rank=1,
                                         law_code=id_box).order_by('id'))
        elif idbox[0] == 'B':
            listBlock = list(
                CodeBlock.objects.filter(block=id_box).order_by('id'))
        if listBlock:
            for el in listBlock:
                htmlclass = "InDatBox"
                title = el.title
                if el.abrogated:
                    htmlclass += " abrogated"
                    if "(abrogé)" not in title:
                        title += " (abrogé)"
                children.append(('B' + str(el.id),
                                 title,
                                 "/static/icons/chapitre.png",
                                 htmlclass,
                                 '2:' + str(el.id),
                                 True))
        children.append((typeboxnewlaw + str(id_box),
                         'Voir les propositions de nouvelles lois ' +
                         'pour cet emplacement',
                         True,
                         'GetNewLaws',
                         idbox[0] + ':' + str(id_box),
                         True))
        children.append((typeboxnewbox + str(id_box),
                         'Voir les propositions sous-groupement de loi ' +
                         'pour cet emplacement',
                         True,
                         'GetNewBoxes',
                         idbox[0] + ':' + str(id_box),
                         True))
    elif idbox[0] == 'D' or idbox[0] == 'E':
        # Get the New law propotions
        if idbox[0] == 'D':  # If we are directly in a Lawcode not Codeblock
            listArticle = list(
                LawArticle.objects.filter(law_code=id_box,
                                          block_id__isnull=True,
                                          is_lwp=True).order_by('id'))
        elif idbox[0] == 'E':
            listArticle = list(
                LawArticle.objects.filter(block=id_box,
                                          is_lwp=True).order_by('id'))
        if listArticle:
            for el in listArticle:
                htmlclass = "GetReflection"
                title = el.title
                if el.abrogated:
                    htmlclass += " abrogated"
                    if "(abrogé)" not in title:
                        title += " (abrogé)"
                children.append(('J' + str(el.id),
                                 title,
                                 "/static/icons/NewLawPrp.png",
                                 htmlclass,
                                 'law:' + str(el.id),
                                 True))
        if request.user.is_authenticated:
            children.append((
                'H'+str(id_box),
                'Créer une loi à cet emplacement',
                "/static/icons/AddNewLaw.png",
                'CreateNewLaw',
                idbox[0] + ':' + str(id_box),
                False)
            )
    elif idbox[0] == 'F' or idbox[0] == 'G':
        # + open a Newbox to add <--------------------------------
        if idbox[0] == 'F':
            listBlock = list(
                CodeBlock.objects.filter(rank=1,
                                         law_code=id_box,
                                         is_cbp=True).order_by('id'))
        elif idbox[0] == 'G':
            listBlock = list(
                CodeBlock.objects.filter(block=id_box,
                                         is_cbp=True).order_by('id'))
        if listBlock:
            for el in listBlock:
                children.append(('G' + str(el.id),
                                 el.title,
                                 True,
                                 "InDatBox",
                                 '2:' + str(el.id),
                                 True))
        if request.user.is_authenticated:
            children.append(('I'+str(id_box),
                             'Proposer un un sous-groupement de loi à' +
                             ' cet emplacement',
                             True,
                             'CreateNewBox',
                             idbox[0] + ':' + str(id_box),
                             False)
                            )
    elif idbox[0] in ['C', 'J', 'Q', 'R', 'S', 'T', 'U']:
        if idbox[0] in ['C', 'J', 'Q']:
            children.extend([(
                'L'+str(id_box)+idbox[0],
                'liste des contre-propositions/amendements',
                "/static/icons/prop-folder.png",
                'GetListRef',
                REFTYPE[idbox[0]] + ':' + str(id_box) +
                ':prp',
                True,
            ),
                (
                    'M'+str(id_box)+idbox[0],
                    'liste des points positifs',
                    "/static/icons/posop-folder.png",
                    'GetListRef',
                    REFTYPE[idbox[0]] + ':' + str(id_box) +
                    ':opp',
                    True,
            ),
                (
                    'N' + str(id_box) + idbox[0],
                    'liste des points négatifs',
                    "/static/icons/negop-folder.png",
                    'GetListRef',
                    REFTYPE[idbox[0]] + ':' + str(id_box) +
                    ':opn',
                    True,
            ),
            ])
        children.extend([(
            'O' + str(id_box) + idbox[0],
            'liste des commentaires',
            "/static/icons/exp-folder.png",
            'GetListRef',
            REFTYPE[idbox[0]] + ':' + str(id_box) +
            ':exp',
            True,
        ),
            (
                'P'+str(id_box)+idbox[0],
                'liste des questions',
                "/static/icons/qst-folder.png",
                'GetListRef',
                REFTYPE[idbox[0]] + ':' + str(id_box) +
                ':qst',
                True,
        ),
        ])
    elif idbox[0] in ['L', 'M', 'N', 'O', 'P']:
        try:
            box = idbox[-1]
            ref = get_ref(box, id_box)
        except:
            raise Http404
        if idbox[0] == "L":
            props = ref.propositions.all()
            for prop in props:
                if prop.title:
                    title = prop.title
                else:
                    title = BeautifulSoup(prop.text_prp)
                    title = title.get_text()
                    title = title.replace("\n", " ")
                if len(title) > 50:
                    title = title[:47] + "..."
                children.append((
                    'Q' + str(prop.id),
                    title,
                    "/static/icons/prop.png",
                    "GetReflection",
                    'prp:' + str(prop.id),
                    True)
                )
        elif idbox[0] == "M":
            posops = ref.posopinions.all()
            for posop in posops:
                if posop.title:
                    title = posop.title
                else:
                    title = BeautifulSoup(posop.text_opp)
                    title = title.get_text()
                    title = title.replace("\n", " ")
                if len(title) > 50:
                    title = title[:47] + "..."
                children.append((
                    'R' + str(posop.id),
                    title,
                    "/static/icons/posop.png",
                    "GetReflection",
                    'opp:' + str(posop.id),
                    True)
                )
        elif idbox[0] == "N":
            negops = ref.negopinions.all()
            for negop in negops:
                if negop.title:
                    title = negop.title
                else:
                    title = BeautifulSoup(negop.text_opn)
                    title = title.get_text()
                    title = title.replace("\n", " ")
                if len(title) > 50:
                    title = title[:47] + "..."
                children.append((
                    'S' + str(negop.id),
                    title,
                    "/static/icons/negop.png",
                    "GetReflection",
                    'opn:' + str(negop.id),
                    True)
                )
        elif idbox[0] == "O":
            exps = ref.explainations.all()
            for exp in exps:
                if exp.title:
                    title = exp.title
                else:
                    title = BeautifulSoup(exp.text_exp)
                    title = title.get_text()
                    title = title.replace("\n", " ")
                if len(title) > 50:
                    title = title[:47] + "..."
                children.append((
                    'T' + str(exp.id),
                    title,
                    "/static/icons/exp.png",
                    "GetReflection",
                    'exp:' + str(exp.id),
                    True)
                )
        elif idbox[0] == "P":
            qsts = ref.questions.all()
            for qst in qsts:
                if qst.title:
                    title = qst.title
                else:
                    title = BeautifulSoup(qst.text_q)
                    title = title.get_text()
                    title = title.replace("\n", " ")
                if len(title) > 50:
                    title = title[:47] + "..."
                children.append((
                    'U' + str(qst.id),
                    title,
                    "/static/icons/qst.png",
                    "GetReflection",
                    'qst:' + str(qst.id),
                    True)
                )
    else:
        return Http404
    JSON_obj = []
    for elem in children:
        # 'B' in the 'id' param inform that this is a Code BLock
        JSON_obj.append({'id': elem[0],
                         'text': elem[1],
                         'icon': elem[2],
                         'a_attr': {'class': elem[3],
                                    'name': elem[4],
                                    },
                         'children': elem[5]})
    return JsonResponse(JSON_obj, safe=False)
