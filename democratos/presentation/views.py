from presentation.models import MyPrez
from django.template.loader import render_to_string
from render_block import render_block_to_string
from django.http import (
    JsonResponse, HttpResponse
)

# Create your views here.


def Get_Prez(request):
    try:
        user = request.user
    except:
        print("user not connected")
    prez = MyPrez.objects.all()
    if not prez:
        the_prez = ""
        prez_nav = ""
    else:
        the_prez = prez[0].text
        prez_nav = prez[0].contents_table
    print("in Get_Prez ", request)
    if request.headers.get('x-requested-with') == 'XMLHttpRequest':
        nav = render_block_to_string('prez.html',
                                     "nav",
                                     locals())
        main = render_block_to_string('prez.html',
                                      "Main",
                                      locals())
        ctx = {'nav': nav,
               'main': main,
               'static2': ""}
        print("no error in Get_Prez")
        return JsonResponse(ctx)
    else:
        template = render_to_string('prez.html', locals())
        index = template.find('id="GoToPrez"')
        template = template[:index-2] + " active" + template[index-2:]
        return HttpResponse(template)
