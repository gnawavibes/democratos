# -*- coding: utf-8 -*-

from django.db import models
import os
from democratos.settings import BASE_DIR
from ckeditor.fields import RichTextField
from bs4 import BeautifulSoup as BS4


# Create your models here.
class MyPrez(models.Model):
    """Web site presentation ediatble from Admin"""
    text = RichTextField()
    contents_table = models.TextField(default="",)

    def save(self, *args, **kwargs):
        soup = BS4(self.text, "html.parser")
        soup.prettify()
        all_elements = soup.find_all(True)
        prev_el_rank = 0
        self.contents_table = "<div class='prez_nav'>"
        for el in all_elements:
            if el.name in ["h1", "h2", "h3", "h4", "h5", "h6"]:
                try:
                    anchor = '<a href="#' + el.a["id"] + '">'
                except:
                    anchor = ""
                if int(el.name[1]) > prev_el_rank:
                    self.contents_table += "<ul><li>" + anchor + el.text
                elif int(el.name[1]) == prev_el_rank:
                    self.contents_table += "</li><li>" + anchor + el.text
                else:
                    for x in range(prev_el_rank-int(el.name[1])):
                        self.contents_table += "</li></ul>"
                    self.contents_table += "<li>" + anchor + el.text
                if anchor != "":
                    self.contents_table += "</a>"
                prev_el_rank = int(el.name[1])
        for x in range(prev_el_rank):
            self.contents_table += "</li></ul>"
        self.contents_table += "</div>"
        os.chdir(BASE_DIR + "/presentation/templates")
        Text_Backup = open("prez.txt", "w")
        Text_Backup.write(self.text)
        Text_Backup.close()
        super(MyPrez, self).save(*args, **kwargs)
