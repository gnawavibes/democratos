# -*- coding: utf-8 -*-

from django.contrib import admin
from django import forms
from presentation.models import MyPrez
from ckeditor_uploader.widgets import CKEditorUploadingWidget
# Register your models here.


class MyPrezAdminForm(forms.ModelForm):
    text = forms.CharField(
        widget=CKEditorUploadingWidget(config_name="prez_config"))

    class Meta:
        model = MyPrez
        fields = ['text', ]


class MyPrezAdmin(admin.ModelAdmin):
    form = MyPrezAdminForm


admin.site.register(MyPrez, MyPrezAdmin)
