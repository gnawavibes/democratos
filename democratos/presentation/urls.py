from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.Get_Prez, name='Get_Prez'),
    path('ckeditor/', include('ckeditor_uploader.urls')),
]
