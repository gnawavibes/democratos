from django.urls import reverse
from django.test import RequestFactory, TestCase, Client
from django.http import JsonResponse, HttpResponse
from mixer.backend.django import mixer
from UserManager.models import CYL_user
from presentation.views import *
import pytest
from unittest import mock


@pytest.mark.django_db
class TestGetPrezView(TestCase):
    def setUp(self):
        self.path = reverse('Get_Prez')
        self.user = mixer.blend(CYL_user)

    def test_Get_prez_ajax(self):
        client = Client()
        client.force_login(self.user)
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('Get_Prez.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "nav")
        self.assertContains(response, "main")
        self.assertContains(response, "static2")

    def test_Get_prez_default(self):
        request = RequestFactory().post(
            self.path,
        )
        request.user = self.user
        response = Get_Prez(request)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('Get_Prez.html')
        self.assertTrue(isinstance(response, HttpResponse))
        self.assertContains(response, "title")
