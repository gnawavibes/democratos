from django.urls import reverse, resolve


class TestUrls:

    def test_Get_Prez_url(self):
        path = reverse('Get_Prez')
        assert resolve(path).view_name == "Get_Prez"
