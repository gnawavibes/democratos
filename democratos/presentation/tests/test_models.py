from presentation.models import MyPrez
from mixer.backend.django import mixer
from django.test import TestCase
import pytest
from unittest import mock

TEST_PREZ = """
<h1>Introduction<a id="anch1" name="anch1"></a></h1>
<p>&nbsp;</p>
<h1>Cr&eacute;ons nos lois<a id="anch12" name="anch12"></a></h1>
<h2>le principe<a id="anch2" name="anch2"></a></h2>
<h3>le texte<a id="anch3" name="anch3"></a></h3>
<p>Le but est dans un premier temps d&#39;&eacute;crire une grosse connerie. Et pour cela je vais mettre une photo de Kikey</p>
<h3>l&#39;ilustration<a id="anch4" name="anch4"></a></h3>
<h4>titre de l&#39;illustration<a id="anch5" name="anch5"></a></h4>
<p>un beau kiki!</p>
<h4>L&#39;illustration<a id="anch6" name="anch6"></a></h4>
<h1>Justice des Communs<a id="anch10" name="anch10"></a></h1>
<p>&nbsp;</p>
<h1>Notre budget<a id="anch11" name="anch11"></a></h1>
<p>&nbsp;</p>
<h1>Le Forum<a id="anch15" name="anch15"></a></h1>
<p>&nbsp;</p>
<h1>La Messagerie<a id="anch13" name="anch13"></a></h1>
<p>&nbsp;</p>
<h1>Conclusion<a id="anch7" name="anch7"></a></h1>
<h3>un premier point <a id="anch8" name="anch8"></a></h3>
<p>&ccedil;a d&eacute;conne quand m&ecirc;me beaucoup moins l&agrave;. Kikey IL A DIT!!!!!</p>
"""


@pytest.mark.django_db
class TestMyPrez(TestCase):

    @mock.patch(
        "builtins.open",
        new_callable=mock.mock_open,
        read_data="prez.txt"
    )
    def test_save_MyPrez(self, mock_file):
        TestPrez = mixer.blend(
            MyPrez,
            text=TEST_PREZ,
            contents_table="",
        )
        TestPrez.save()
        self.assertNotEqual(TestPrez.contents_table, "")
        self.assertEqual(TestPrez.contents_table.count("anch"), 13)
        self.assertEqual(TestPrez.contents_table.count("<li>"), 13)
        self.assertEqual(TestPrez.contents_table.count("</div>"), 1)
        self.assertEqual(TestPrez.contents_table.count("</ul>"), 6)
        assert open("/presentation/templates").read() == "prez.txt"
        mock_file.assert_called_with("/presentation/templates")
