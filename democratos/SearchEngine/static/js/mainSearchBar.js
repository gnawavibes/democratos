function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

/*
The functions below will create a header with csrftoken
*/
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function MainSearch(){
    var input = $("#MainSearchBarInput").val()
    console.log(input);
    if (input!=""){
        $.ajax({
            type: 'POST',
            url: '/Search/search_reflection',
            data: {'input': input, csrfmiddlewaretoken: csrftoken},
            dataType: "json",
            success: function(res) {
                /*


                */
            },
        });
    };
}

$(document).ready(function() {
    $("#MainSearchBarInput" ).keyup(function (e) {
        if (e.keyCode==13){
            console.log("Nip Alert!");
            MainSearch();
        };
    });
});