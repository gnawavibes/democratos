from elasticsearch_dsl.query import Q, MultiMatch, SF
from SearchEngine.documents import QuestionDocument, LawArticleDocument
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from django.http import JsonResponse
from django.views.decorators.http import require_POST, require_GET
from democratos import log
from democratos.decorators import ajax_login_required
import sys
if not 'pytest' in sys.argv[0]:
    from democratos.local_settings import ElasticHost
else:
    ElasticHost = ["127.0.0.1:9201",
    "http://elasticsearch_test:9201","localhost:9201","elasticsearch:9200"]
from datetime import datetime

client = Elasticsearch(ElasticHost)

@require_POST
def search_reflection(request):
    searchtxt = request.POST.get('input', None)
    print("Nip HERE!")
    s_ref= Search(using=client)
    q_law = s_ref.index(
        "law_article"
    ).query(
        size=1,
        query=Q('multi_match',
            query=searchtxt,
            fields=['title','text_law'],
            fuzziness="AUTO",
        ),
        
    )
    r_law= q_law.execute()
    max_score = r_law.hits.max_score
    q_law = s_ref.index(
        "law_article"
    ).query(
        "function_score",
        query=Q('multi_match',
            query=searchtxt,
            fields=['title','text_law'],
            fuzziness="AUTO",
        ),
        min_score=max_score*0.95,
    )
    """
    .query(
        Q('match_phrase',
            text_law=searchtxt,
        )|Q('match_phrase',
            title=searchtxt,
        )
    )
    """
    r_law= q_law.execute()
    log.debug(r_law)
    import pdb; pdb.set_trace()
    """
    user = request.user
    query = s_user.index(
        "cyl_user"
    ).query(
        'regexp',
        username=name+'.*',
    )
    print(user.id)
    response = query.execute()
    if len(response) < 10:
        query = s_user.index("cyl_user").query(
            'regexp',
            username='.*'+name+'.*',
        )
        response2 = query.execute()
        toremove = []
        for hit in response2.hits:
            if hit in response.hits:
                toremove.append(hit)
        for hit in toremove:
            response2.hits.remove(hit)
        response.hits.extend(response2.hits[:10-len(response)])
    else:
        response = response[:10]
    if response:
        res = []
        for hit in response.hits:
            print(hit)
            res.append({"username": hit.username, 'id': hit.id})
        ctx = {"results": str(res)}
        print(name, "\n\n", ctx)
    else:
        query = s_user.suggest(
            'suggestion',
            name,
            completion={'field': 'username.suggest'}
        )
        response = query.execute()
        results = str(response.suggest['suggestion'][0]['options'])
        ctx = {
            "results": results.replace("text", "username")
        }
        """
    ctx = {
        "r_law": r_law,
    }
    log.debug(ctx)
    return JsonResponse(ctx)
