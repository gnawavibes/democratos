from django_elasticsearch_dsl import Document, fields
from elasticsearch_dsl import analyzer, Nested
from django_elasticsearch_dsl.registries import registry
from CreateYourLaws.models import (
    Question, Explaination, LawArticle, Proposition,
    Posopinion, Negopinion, SourceLink, LawCode,
    CodeBlock
)
from UserManager.models import CYL_user
from elasticsearch_dsl.analysis import token_filter

from django.utils.encoding import force_str
from django.utils.html import strip_tags
from html import unescape

"""
def get_searchable_content(self, value):
    # Strip HTML tags to prevent search backend to index them
    source = force_str(value)
    return [unescape(strip_tags(source))]
"""

edge_ngram_completion_filter = token_filter(
    'edge_ngram_completion_filter',
    type="edge_ngram",
    min_gram=1,
    max_gram=20
)


edge_ngram_completion = analyzer(
    "edge_ngram_completion",
    tokenizer="standard",
    filter=["lowercase", edge_ngram_completion_filter]
)

# @registry.register_document
class QuestionDocument(Document):
    """
    title = fields.TextField(
        fields={
            'raw': fields.TextField(analyzer='standard'),
            'suggest': fields.CompletionField(),
            'edge_ngram_completion': fields.TextField(
                analyzer=edge_ngram_completion
            ),
        }
    )
    """
    text_qst = fields.TextField()

    def prepare_text_qst(self,instance):
        source = force_str(instance.text_qst)
        return [unescape(strip_tags(source))]
    

    class Index:
        # Name of the Elasticsearch index
        name = 'question'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = Question # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
            'title',
            'approval_factor',
        ]
        
        # related_models=[CYL_user]

    """
    def get_instances_from_related(self, related_instance):
        ""If related_models is set, define how to retrieve the Car instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        ""
        if isinstance(related_instance, Message):
            return related_instance.message

    def get_instances_from_related(self, related_instance):
        ""If related_models is set, define how to retrieve the Book instance(s) from the related model.""
        if isinstance(related_instance, CYL_user):
            return related_instance.conversation_set.all()
    """

# @registry.register_document
class LawArticleDocument(Document):
    text_law = fields.TextField()

    def prepare_text_qst(self,instance):
        source = force_str(instance.text_law)
        return [unescape(strip_tags(source))]

    class Index:
        # Name of the Elasticsearch index
        name = 'law_article'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = LawArticle # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
            'title',
            'approval_factor',
        ]


"""
@registry.register_document
class MessageDocument(Document):
    message = fields.TextField(
        fields={
            'raw': fields.TextField(analyzer='standard'),
            'suggest': fields.CompletionField(),
            'edge_ngram_completion': fields.TextField(
                analyzer=edge_ngram_completion
            ),
        }
    )
    conversation = fields.NestedField(properties={
        'name': fields.TextField(),
        'id': fields.IntegerField(),
        'date': fields.DateField(index=False),
        'participants':fields.NestedField(properties={
            'username': fields.TextField(),
            'id': fields.IntegerField(),
        },include_in_root=True)
    }, include_in_root=True)

    class Index:
        # Name of the Elasticsearch index
        name = 'message'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = Message # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
        ]
        related_models=[Conversation]
"""
