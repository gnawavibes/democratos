from django.urls import path
from . import search
from . import views

urlpatterns = [
    path('search_reflection',
        search.search_reflection,
        name="search_reflection"),
]

