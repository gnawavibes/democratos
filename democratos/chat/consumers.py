# Chat/consumers.py
import json
from channels.generic.websocket import AsyncWebsocketConsumer
from django.utils import timezone
from chat.tasks import SaveMessage
from democratos import log

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        username = text_data_json['userName']
        userid = text_data_json['userId']
        convid = text_data_json['convId']
        date = timezone.now()

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'username': username,
                'userid': userid,
                'date': date.strftime("%d/%m/%Y, %H:%M:%S")
            }
        )

        #save message in server
        log.debug((text_data_json))
        log.debug(("In the conversation ",
            text_data_json['convName'],
            " the message received from ",
            text_data_json['userName'],
            ' is: ',
            message
            ))
        SaveMessage.delay(message, userid, convid, date)

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        username = event['username']
        userid = event['userid']
        date = event['date']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'username': username,
            'userid': userid,
            'date': date
        }))
       