from django.urls import reverse, resolve
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase
from django.test.client import Client
from django.http import (
    JsonResponse, HttpResponse, Http404, HttpResponseNotFound,
    HttpResponseNotAllowed
)
from mixer.backend.django import mixer
import pytest
from unittest import mock

from chat.models import *
from chat.views import *
from UserManager.models import CYL_user



@pytest.mark.django_db
class TestHomeChatView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestHomeChatView, cls).setUpClass()
        cls.path = reverse('homeChat')
        cls.user = mixer.blend(CYL_user)

    def test_homeChat_user_logged(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        response = homeChat(request)
        self.assertTemplateUsed('homeChat.html')
        self.assertTrue(isinstance(response, HttpResponse))

    def test_homeChat_user_unautenthicated_HTTP_request(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = AnonymousUser()
        response = homeChat(request)
        self.assertIn('UserManager/accounts/login/', response.url)

    def test_homeChat_user_unautenthicated_AJAX_GET_request(self):
        response = Client().get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertIn('UserManager/accounts/login/', response.url)

    def test_homeChat_user_unautenthicated_AJAX_POST_request(self):
        response = Client().post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertIn('UserManager/accounts/login/', response.url)

    @classmethod
    def tearDownClass(cls):
        super(TestHomeChatView, cls).tearDownClass()


@pytest.mark.django_db
class TestGetConversationView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestGetConversationView, cls).setUpClass()
        cls.path = reverse('GetConversation')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.conv = mixer.blend(Conversation)
        cls.conv.participants.add(cls.user)
        for i in range(1,3):
            setattr(cls, "p"+str(i), mixer.blend(CYL_user))
            cls.conv.participants.add(getattr(cls,"p"+str(i)))

    def setUp(self):
        self.client.force_login(self.user)

    def test_GetConversation_HTTP_request(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetConversation(request)

    def test_GetConversation_unauthenticated_user(self):
        client = Client()
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')
    
    def test_GetConversation_AJAX_POST_request(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    @mock.patch(
        "UserManager.models.CYL_user.is_blocked_by",
        mock.MagicMock(name="is_blocked_by"),
    )
    def test_GetConversation_working_case(self):
        response = self.client.get(
            self.path,
            {
                'idConv':self.conv.id,
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTemplateUsed('room.html')
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "block")
        self.assertContains(response, "content")
        self.assertContains(response, "conv_name")
        content = eval(response.content)
        self.assertEqual(content["conv_id"], str(self.conv.id))


    def test_GetConversation_conv_id_error(self):
        response = self.client.get(
            self.path,
            {
                'idConv':str(-1),
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        print(response)
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    @classmethod
    def tearDownClass(cls):
        super(TestGetConversationView, cls).tearDownClass()

@pytest.mark.django_db
class TestcreateNewConversationView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestcreateNewConversationView, cls).setUpClass()
        cls.path = reverse('createNewConversation')
        cls.user = mixer.blend(CYL_user)
        cls.client = Client()
        cls.listparticipants = []
        for i in range(1,3):
            setattr(cls, "p"+str(i), mixer.blend(CYL_user))
            cls.listparticipants.append(
                getattr(cls,"p"+str(i)).username)

    def setUp(self):
        self.client.force_login(self.user)        

    def test_createNewConversation_HTTP_request(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetConversation(request)

    def test_createNewConversation_unauthenticated_user(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')
    
    def test_createNewConversation_AJAX_GET_request(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )
         
    @mock.patch(
        "chat.models.Conversation.objects.create",
    )
    def test_createNewConversation_working_case(self, mock_create):
        mock_create.return_value=mixer.blend(Conversation)
        response = self.client.post(
            self.path,
            {
                'nameConv':"test_new_conv",
                "listParticipant": str(self.listparticipants)
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTemplateUsed('homeChat.html')
        self.assertContains(response, "idConv")
        self.assertContains(response, "nav")
        self.assertTrue(isinstance(response, HttpResponse))

    @mock.patch(
        "chat.models.Conversation.objects.create",
    )
    def test_createNewConversation_user_has_blocked(self, mock_create):
        mock_create.return_value=mixer.blend(Conversation)
        block = mixer.blend(BlockedContact,
            user=self.user,
            blocked=self.p1,
        )
        response = self.client.post(
            self.path,
            {
                'nameConv':"test_new_conv",
                "listParticipant": str(self.listparticipants)
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, "block_error")
        self.assertTrue(isinstance(response, HttpResponse))

    @mock.patch(
        "chat.models.Conversation.objects.create",
    )
    def test_createNewConversation_user_is_blocked(self, mock_create):
        mock_create.return_value=mixer.blend(Conversation)
        block = mixer.blend(BlockedContact,
            user=self.p1,
            blocked=self.user,
        )
        response = self.client.post(
            self.path,
            {
                'nameConv':"test_new_conv",
                "listParticipant": str(self.listparticipants)
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, "block_error")
        self.assertTrue(isinstance(response, HttpResponse))

    @classmethod
    def tearDownClass(cls):
        super(TestcreateNewConversationView, cls).tearDownClass()

@pytest.mark.django_db
class TestContactSomeoneView(TestCase):
    def setUp(self):
        self.path = reverse('ContactSomeone')
        self.user = mixer.blend(CYL_user)
        self.client = Client()
        self.client.force_login(self.user)
        self.otheruser = mixer.blend(CYL_user)

    def test_ContactSomeone_HTTP_request(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetConversation(request)

    def test_ContactSomeone_unauthenticated_user(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')
    
    def test_ContactSomeone_AJAX_GET_request(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_ContactSomeone_wrong_contact_id(self):
        conv = mixer.blend
        response = self.client.post(
            self.path,
            {
                'contact_id': 'fgniofhj',
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_ContactSomeone_create_conv_working_case(self):
        response = self.client.post(
            self.path,
            {
                'contact_id':self.otheruser.id
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, JsonResponse)
        )
        self.assertTemplateUsed('HomeChat.html')
        self.assertContains(response, "nav")
        self.assertContains(response, "idConv")

    def test_ContactSomeone_get_conv_working_case(self):
        conv = mixer.blend(Conversation)
        if self.user.id > self.otheruser.id:
            NameConv = str(self.otheruser.id)+"AND"+str(self.user.id)
        else:
            NameConv = str(self.user.id)+"AND"+str(self.otheruser.id)
        conv.name=NameConv
        conv.participants.add(self.user)
        conv.participants.add(self.otheruser)
        response = self.client.post(
            self.path,
            {
                'contact_id':self.otheruser.id
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, JsonResponse)
        )
        self.assertTemplateUsed('HomeChat.html')
        self.assertContains(response, "nav")
        self.assertContains(response, "idConv")

    def test_ContactSomeone_user_has_blocked(self):
        block = mixer.blend(
            BlockedContact,
            user=self.user,
            blocked=self.otheruser
        )
        response = self.client.post(
            self.path,
            {
                'contact_id':self.otheruser.id
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        print(response)
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )

    def test_ContactSomeone_user_is_blocked(self):
        block = mixer.blend(
            BlockedContact,
            user=self.otheruser,
            blocked=self.user
        )
        response = self.client.post(
            self.path,
            {
                'contact_id':self.otheruser.id
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, JsonResponse)
        )
        self.assertContains(response, "block_error")


@pytest.mark.django_db
class TestBlockUnblockContactView(TestCase):
    def setUp(self):
        self.path = reverse('BlockUnblockContact')
        self.user = mixer.blend(CYL_user)
        self.client = Client()
        self.client.force_login(self.user)
        self.otheruser = mixer.blend(CYL_user)

    def test_BlockUnblockContact_HTTP_request(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetConversation(request)

    def test_BlockUnblockContact_unauthenticated_user(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')
    
    def test_BlockUnblockContact_AJAX_GET_request(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_BlockUnblockContact_block_working_case(self):
        response = self.client.post(
            self.path,
            {
                'contact_id':self.otheruser.id
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, JsonResponse)
        )
        self.assertTemplateUsed('ContactsList.html')
        self.assertContains(response, "html")
        content = eval(response.content)
        self.assertEqual(content["contact_id"], self.otheruser.id)
        self.assertEqual(content["contact_name"], self.otheruser.username)
        self.assertEqual(content["blocked"], 'true')

    def test_BlockUnblockContact_unblock_working_case(self):
        block = mixer.blend(
            BlockedContact,
            user=self.user,
            blocked=self.otheruser
        )
        response = self.client.post(
            self.path,
            {
                'contact_id':self.otheruser.id
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, JsonResponse)
        )
        self.assertTemplateUsed('ContactsList.html')
        self.assertContains(response, "html")
        content = eval(response.content)
        self.assertEqual(content["contact_id"], self.otheruser.id)
        self.assertEqual(content["contact_name"], self.otheruser.username)
        self.assertEqual(content["blocked"], 'false')

    def test_BlockUnblockContact_wrong_contact_id(self):
        response = self.client.post(
            self.path,
            {
                'contact_id': 'fgniofhj',
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotFound)
        )


@pytest.mark.django_db
class TestGetManageContactsView(TestCase):
    def setUp(self):
        self.path = reverse('GetManageContacts')
        self.user = mixer.blend(CYL_user)
        self.client = Client()
        self.client.force_login(self.user)


    def test_GetManageContacts_HTTP_request(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = GetConversation(request)

    def test_GetManageContacts_unauthenticated_user(self):
        client = Client()
        response = client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')
    
    def test_GetManageContacts_AJAX_POST_request(self):
        response = self.client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )
         
    def test_GetManageContacts_working_case(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTemplateUsed('ContactsList.html')
        self.assertContains(response, "content")
        self.assertTrue(isinstance(response, HttpResponse))
