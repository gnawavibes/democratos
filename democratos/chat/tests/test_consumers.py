"""
from django.urls import reverse, resolve
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase, override_settings
from django.test.client import Client
from django.http import (
    JsonResponse, HttpResponse, Http404, HttpResponseNotFound,
    HttpResponseNotAllowed
)
from mixer.backend.django import mixer
from datetime import datetime, timedelta
import pytest
from unittest import mock

from chat.models import *
from chat.consumers import *
from UserManager.models import CYL_user
from channels.sessions import SessionMiddlewareStack
from channels.testing import WebsocketCommunicator

import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf.urls import re_path
from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")

app = ProtocolTypeRouter({
    # Django's ASGI application to handle traditional HTTP requests
    "http": get_asgi_application(),

    # WebSocket chat handler
    "websocket": AuthMiddlewareStack(
        URLRouter([
            re_path(r'ws/chat/(?P<room_name>\w+)/$', ChatConsumer.as_asgi()),
        ])
    ),
})


""
@pytest.mark.django_db
class TestChatConsumer(TestCase):
    def setUp(self):
        self.app = SessionMiddlewareStack(ChatConsumer.as_asgi())
        self.communicator = WebsocketCommunicator(
            ChatConsumer(),
            '/ws/chat/room_test/'
        )

    async def test_connnect(self):
        connected, _ = await self.communicator.connect()
        import pdb; pdb.set_trace()
        assert connected
""

@pytest.mark.asyncio
async def test_async_websocket_consumer():
    ""
    Tests that AsyncWebsocketConsumer is implemented correctly.
    ""
    # Test a normal connection
    communicator = WebsocketCommunicator(app, "ws/chat/testws/")
    
    channel_layers_setting = {
        "default": {"BACKEND": "channels.layers.InMemoryChannelLayer"}
    }
    with override_settings(CHANNEL_LAYERS=channel_layers_setting):
        connected, _ = await communicator.connect()
        # Test sending text
        await communicator.send_to(text_data="hello")
        response = await communicator.receive_from()
        assert response == "hello"
        # Close
        await communicator.disconnect()
"""