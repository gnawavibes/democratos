import pytest

from django.urls import reverse, resolve
from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory, TestCase
from django.test.client import Client
from django.http import (
    JsonResponse, HttpResponse, Http404, HttpResponseNotFound,
    HttpResponseNotAllowed
)
from mixer.backend.django import mixer
from datetime import datetime, timedelta

from unittest import mock

from chat.models import *
from chat.search import *
from elasticsearch import Elasticsearch
from chat.documents import *
from UserManager.documents import CYL_userDocument
from elasticsearch_dsl import Index
from elasticsearch_dsl.analysis import token_filter

"""
def index_test_fixtures(es, index_name, mixer_elmt):
    data = {}
    for el in mixer_elmt.__dict__:
        data[el]=mixer_elmt.__dict__[el]
    data.pop("_state", None)
    created = es.index(index=index_name,id=data["id"], body=data)
    es.indices.refresh(index_name)

def my_blender(Model, ES, *args, **kwargs):
    the_blend = mixer.blend(Model, *args, **kwargs)
    index_test_fixtures(ES, Model.__name__.lower(), the_blend)
    return the_blend


@pytest.fixture
def testcf():
    return "yo"
"""

schemas={}
documents = [
    ConversationDocument,
    CYL_userDocument,
    MessageDocument
]

for doc in documents:
    i = Index(doc._index._name)
    i.document(doc)
    i.analyzer(edge_ngram_completion)
    schemas[doc._index._name] = i.to_dict()["mappings"]


ELASTICSEARCH_TEST_HOST = [
    "127.0.0.1:9201",
    "elasticsearch:9200"
]

class Esearch_class(Elasticsearch):
    def __init__(self, HOST, schemas=schemas, edge_ngram_completion=edge_ngram_completion):
        super().__init__(HOST)
        self.schemas = schemas
        for index_name, schema in self.schemas.items():
            if self.indices.exists(index=index_name):
                self.indices.delete(index=index_name)
            body = {
                "settings": {
                    "number_of_shards": 1,
                    "number_of_replicas": 1,
                    "index.store.type": "mmapfs",
                    "analysis":{
                        "analyzer":{
                            "edge_ngram_completion":edge_ngram_completion.get_definition(),
                        },
                        "filter":{
                            "edge_ngram_completion_filter":edge_ngram_completion_filter.get_definition(),
                        },
                    }
                },
                "mappings": schema,
            }
            self.indices.create(index=index_name, body=body)

    def clearES(self):
        for index_name in self.schemas.keys():
            self.indices.flush()

    def __del__(self):
        for index_name in self.schemas.keys():
            self.indices.delete(index=index_name)


@pytest.mark.django_db
class Testsearch_convView(TestCase):
    @classmethod
    def setUpClass(cls):
        super(Testsearch_convView, cls).setUpClass()
        cls.esearch = Esearch_class(ELASTICSEARCH_TEST_HOST)
        cls.path = reverse('search_conv')
        cls.user = mixer.blend(CYL_user, username="main_user")
        cls.client = Client()
        cls.client.force_login(cls.user)
        cls.conv = mixer.blend(Conversation)
        cls.conv.participants.add(cls.user)
        cls.conv.save()
        for i in range(1,4):
            setattr(cls, "p"+str(i), mixer.blend(
                CYL_user, username="p"+str(i)))
            getattr(cls, "p"+str(i)).save()
            setattr(cls, "conv_"+str(i), mixer.blend(
                Conversation,
                name="convers_test "+str(i),
            ))
            cls.conv.participants.add(getattr(cls,"p"+str(i)))
            getattr(cls,"conv_"+str(i)).participants.add(
                getattr(cls,"p"+str(i)))
            getattr(cls,"conv_"+str(i)).participants.add(
                cls.user)
            getattr(cls,"conv_"+str(i)).save()
            setattr(cls, "mess_from_p"+str(i), mixer.blend(
                Message,
                conversation=cls.conv,
                author=getattr(cls,"p"+str(i))
            ))
            getattr(cls, "mess_from_p"+str(i)).save()
            for j in range(1,6):
                setattr(cls, "m_p"+str(i)+"_"+str(j), mixer.blend(
                    Message,
                    conversation=getattr(cls,"conv_"+str(i)),
                    author=getattr(cls,"p"+str(i)),
                    message= "conv_{0}: test {1} from p{0} to user".format(i,j)
                    )
                )
                getattr(cls, "m_p"+str(i)+"_"+str(j)).save()
                setattr(cls, "m_user_"+str(j), mixer.blend(
                    Message,
                    conversation=getattr(cls,"conv_"+str(i)),
                    author=cls.user,
                    message= "conv_{0}: test {1} from user to p{0}".format(i,j)
                    )
                )
                getattr(cls, "m_user_"+str(j)).save()

    @classmethod
    def tearDownClass(cls):
        del(cls.esearch)
        super(Testsearch_convView, cls).tearDownClass()

    def setUp(self):
        self.client.force_login(self.user)

    def test_search_conv_HTTP_request(self):
        request = RequestFactory().get(
            self.path,
        )
        request.user = self.user
        with self.assertRaises(Http404):
            response = search_conv(request)

    def test_search_conv_unauthenticated_user(self):
        client = Client()
        response = client.post(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertContains(response, 'not_authenticated')
    
    def test_search_conv_AJAX_GET_request(self):
        response = self.client.get(
            self.path,
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(
            isinstance(response, HttpResponseNotAllowed)
        )

    def test_search_conv_all_search_type_works(self):
        client = self.client
        response = client.post(
            self.path,
            {
                'input':'test',
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "results")
        content = eval(response.content)
        hits_len = len(eval(content["results"]))
        print(content["results"], "    ", hits_len)
        self.assertTrue(hits_len==3)

    def test_search_conv_conversation_search_type_works(self):
        client = self.client
        response = client.post(
            self.path,
            {
                'input':'convers_test',
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "results")
        content = eval(response.content)
        hits_len = len(eval(content["results"]))
        print(content["results"], "    ", hits_len)
        self.assertTrue(hits_len==3)

    def test_search_conv_message_search_type_works(self):
        client = self.client
        response = client.post(
            self.path,
            {
                'input':'from p2',
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "results")
        content = eval(response.content)
        hits_len = len(eval(content["results"]))
        self.assertTrue(hits_len==1)

    def test_search_conv_participant_search_type_works(self):
        client = self.client
        response = client.post(
            self.path,
            {
                'input':self.p1.username,
            },
            **{'HTTP_X_REQUESTED_WITH':'XMLHttpRequest'}
        )
        self.assertTrue(isinstance(response, JsonResponse))
        self.assertContains(response, "results")
        content = eval(response.content)
        hits_len = len(eval(content["results"]))
        print(content["results"], "    ", hits_len)
        self.assertTrue(hits_len==2)
