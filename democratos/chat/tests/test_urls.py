from django.urls import reverse, resolve

class TestUrls:
    def test_homeChat_url(self):
        path = reverse('homeChat')
        assert resolve(path).view_name == "homeChat"

    """ ===========   OBSOLETE!   ==================
    def test_index_url(self):
        path = reverse('index')
        assert resolve(path).view_name == "index"

    def test_room_url(self):
        path = reverse("room",kwargs={"room_name":"test"})
        assert resolve(path).view_name == "room"
        ============================================
    """

    def test_createNewConversation_url(self):
        path = reverse('createNewConversation')
        assert resolve(path).view_name == "createNewConversation"

    def test_GetConversation_url(self):
        path = reverse('GetConversation')
        assert resolve(path).view_name == "GetConversation"

    def test_ContactSomeone_url(self):
        path = reverse('ContactSomeone')
        assert resolve(path).view_name == "ContactSomeone"

    def test_GetManageContacts_url(self):
        path = reverse('GetManageContacts')
        assert resolve(path).view_name == "GetManageContacts"

    def test_BlockUnblockContact_url(self):
        path = reverse('BlockUnblockContact')
        assert resolve(path).view_name == "BlockUnblockContact"

    def test_search_conv_url(self):
        path = reverse('search_conv')
        assert resolve(path).view_name == "search_conv"

