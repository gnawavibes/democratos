//setup JQuery's AJAX methods to setup CSRF token in the request before sending it off.

// This function gets cookie with a given name
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

/*
The functions below will create a header with csrftoken
*/
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    };
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    };
    function closeList(elmnt=null) { // <------------------ REVOIR ICI
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        if (!elmnt) {
             $("#SearchUserResultsDiv").html("");
        }
        else{
            var x = $("#SearchUserResultsDiv");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i]["username"] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
    };
    /*execute a function when someone writes in the text field:*/
    // inp.change(function(e) {
    var a, b, i, val = inp.val();
    /*close any already open lists of autocompleted values*/
    closeList();
    if (!val) { return false;}
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.getElementById("SearchUserResultsDiv");
    //a.setAttribute("id", inp.attr("id")+ "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    /*append the DIV element as a child of the autocomplete container:*/
    //inp.parentNode.appendChild(a);
    /*for each item in the array...*/
    for (i = 0; i < arr.length; i++) {
        /*create a DIV element for each matching element:*/
        b = document.createElement("DIV");
        /*make the matching letters bold:*/
        index = arr[i]["username"].toLowerCase().indexOf(val);
        b.innerHTML = arr[i]["username"].substr(0, index);
        b.innerHTML += "<strong>" + val + "</strong>";
        b.innerHTML += arr[i]["username"].substr(index + val.length);
        /*insert a input field that will hold the current array item's value:*/
        b.innerHTML += "<input type='hidden' value='" + arr[i]["username"]  + "'>";
        /*execute a function when someone clicks on the item value (DIV element):*/
        b.addEventListener("click", function(e) {
            /*insert the value for the autocomplete text field:*/
            e.preventDefault();
            var selected = inp.val(this.getElementsByTagName("input")[0].value);
            if (!selected) { return false;}
            /*close the list of autocompleted values,
            (or any other open lists of autocompleted values:*/
            closeList();
        });
        a.appendChild(b);
    }
    //});
    /*execute a function presses a key on the keyboard:*/
    inp.on("keydown", function(e) {
        var x = document.getElementById("SearchUserResultsDiv");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
              /*and simulate a click on the "active" item:*/
              if (x) x[currentFocus].click();
            }
        }
    });
};

function ValidNewConversation(){
    /* Check if the name of the conversations and at least 
    one participantis correct to start a new conversation
    return bool if valid or not*/
    // clean the form from previous alerts
    $(".alert").remove();
    // check the conversation name
    if ($("#ConversationNameInput").val() ==""){
        var msg = `
        <div class="alert alert-danger" role="alert">
            Un nom de conversation est nécessaire.
        </div>`;
        $("#ConversationNameInput").before(msg);
        return false
    }
    // check the list of participants
    var divs = $("#ListParticipant").children(".AddedParticipant");
    var valid = false;
    var i = 0;
    if (!divs.length<=0){
        while (!valid || i<divs.length){
            // console.log(divs[i]);
            $.ajax({
                type: 'POST',
                url: '/UserManager/ValidUser',
                data: {'val': divs[i].textContent.slice(0, -1), 
                        csrfmiddlewaretoken: csrftoken},
                dataType: "json",
                async:false,
                success: function(res) {
                    valid = eval(res.valid);
                },
                error: function(rs, e) {
                    alert(rs.responseText);
                }
            });
            i++;
        }
    }
    if (!valid){
        var msg = `<div class="alert alert-danger" role="alert">
            La conversation nécessite au moins un interlocuteur valide.
        </div>`;
        $("#SearchUserInput").before(msg);
    }
    return valid;
};

function CreateConversation(){
    /* Create a Conversation from a Conversation form in the dialog box*/
    var convName = $("#ConversationNameInput").val();
    var listParticipant=[];
    var divs = $("#ListParticipant").children(".AddedParticipant");
    for (i=0; i<divs.length;i++){
        listParticipant[i]=divs[i].textContent.slice(0, -1);
    }
    $.ajax({
        type: 'POST',
        url: '/Chat/createNewConversation',
        data: {'listParticipant': JSON.stringify(listParticipant),
                'convName':convName,
                csrfmiddlewaretoken: csrftoken},
        dataType: "json",
        success: function(res) {
            if (res.block_error){
                alert(res.block_error);
                return
            }
            $('nav').html(res.nav);
            DisplayConv(res.idConv);
            SetAddConvForm();
            SetUpConvNav();
        },
        error: function(rs, e) {
            alert(rs.responseText);
        }
    });
}

function ContactSomeone(contact_id){
    $.ajax({
        type: 'POST',
        url: '/Chat/ContactSomeone',
        data: {'contact_id':contact_id,
                csrfmiddlewaretoken: csrftoken},
        dataType: "json",
        success: function(res) {
            if (res.block_error){
                alert(res.block_error);
                return
            }
            $('nav').html(res.nav);
            DisplayConv(res.idConv);
            SetUpConvNav();
        },
        error: function(rs, e) {
            alert(rs.responseText);
        }
    });
}

function BlockUnblockContact(contact_id){
    $.ajax({
        type: 'POST',
        url: '/Chat/BlockUnblockContact',
        data: {'contact_id':contact_id,
                csrfmiddlewaretoken: csrftoken},
        dataType: "json",
        success: function(res) {
            var block = eval(res.blocked);
            var TR = "#TR"+res.contact_name+res.contact_id;
            console.log(res.contact_name, "is now blocked: ", block);
            if (block){
                $(TR).addClass("table-warning");
            }else{
                $(TR).removeClass("table-warning");
            }
            $(TR).html(res.html)
            $(TR).find(".BlockContact, .UnBlockContact").each(function(e){
                $(this).on('click',function(e){
                    var contact = $(this).attr("name").split(":");
                    BlockUnblockContact(contact[1])
                })
            })
        },
        error: function(rs, e) {
            alert(rs.responseText);
        }
    });
}

function DisplayConv(idConv){
    /* Get the conversation from its id and 
    display it in '#content' section*/
    console.log("looking for conv "+idConv);
    $.ajax({
        type: 'GET',
        url: '/Chat/GetConversation',
        data: {'idConv':idConv,
                csrfmiddlewaretoken: csrftoken},
        dataType: "json",
        success: function(res) {
            $('#content').html(res.content);
            $(".chat-log").height($("#main").outerHeight()- 170);
            SetUpRoom();
            if(res.block.length!=0){
                var input_chat = "#chat-message-input-" + res.conv_name + "-" +res.conv_id;
                var submit_chat = "#chat-message-submit-" + res.conv_name + "-" +res.conv_id;
                console.log("block! ", input_chat, submit_chat)
                var text = "";
                for (i=0; i<res.block.length;i++){
                    text += res.block[i] + ";  ";
                }
                $(input_chat).val(text);
                $(input_chat).prop('disabled', true);
                $(submit_chat).prop('disabled', true);
            }
        },
        error: function(rs, e) {
            alert(rs.responseText);
        }
    });
    $(".ConvItem").removeClass("active");
    $("#itemConv"+idConv).addClass("active");
}

function SetAddConvForm(){
    // Initializes search overlay plugin.
    // Replace onSearchSubmit() and onKeyEnter() with
    // your logic to perform a search and display results
    $("#dialogdiv").css('display','none')
    $("#AddConv").on("click",function(){
        $("#SearchUserInput").val("")
        var dialogbox = $("#dialogdiv").css('display','block')
            .dialog({
                modal: true, title: "Choisir ses correspondants", zIndex: 3, autoOpen: true,
                width: '35%', resizable: false, height:300,
                close: function (event, ui) {
                    $(this).css('display','none');
                },
                buttons: {
                    Valider: function () {
                        var valid = ValidNewConversation();
                        if (valid){
                            $(this).dialog("close");
                            CreateConversation();
                            // clean dialogbox
                            $("#SearchUserInput").val("");
                            $("#ListParticipant").html("");
                        }
                    }
                },
        }); 
        $("#SearchUserInput").keyup(function (e) {
            if ([13,38,40].includes(e.keyCode)){
                return
            }
            var input = $(this).val()
            if (input!=""){
                $.ajax({
                    type: 'POST',
                    url: '/UserManager/search_user',
                    data: {'input': input, csrfmiddlewaretoken: csrftoken},
                    dataType: "json",
                    success: function(res) {
                        var r = eval(res.results);
                        if (r.length!=0){
                            $('#SearchUserResultsDiv').html('');
                            autocomplete($("#SearchUserInput"),r);
                        }
                        else{
                            var html = "Pas d'utilisateurs pour ce(s) mot(s) clé";
                            $('#SearchUserResultsDiv').html(html);
                        }
                    },
                });
            }
            else{
                $('#SearchUserResultsDiv').html("");
            }
        });
        $("#ValidUserButton").on("click", function(e){
            e.preventDefault();
            var val = $("#SearchUserInput").val()
            if ($("#ListParticipant").find("#"+val+"div").length || val==""){
                return;
            }
            var newParticipant = '<div id="'+val+'div" name="'+val+'" class="badge badge-pill';
            newParticipant +=' badge-primary AddedParticipant">' +val +" </div>";
            $("#ListParticipant").append(newParticipant);
            var closeico = $("#removecopy").clone();
            closeico.appendTo($("#"+val+"div"));
            $(".removeico").on("click", function(e){
                $(this).parent().remove();
            });
            $("#SearchUserInput").val("");
        });
    });
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        $("#SearchUserResultsDiv").html("");
    });
}

function SetUpRoom(){
    // retrieve conversation data
    const roomName = $("#convName").html();
    const convName = roomName.split(":")[0];
    const convId = roomName.split(":")[1];

    // retrieve user data
    const userData = $("#userData").html();
    const userName = userData.split(":")[0];
    const userId = userData.split(":")[1];

    console.log("Create room "+roomName);
    const chatSocket = new WebSocket(
        'ws://'
        + window.location.host
        + '/ws/chat/'
        + roomName.replace(/:/g,"").replace(/ /g, "_")
        + '/'
    );

    chatSocket.onmessage = function(e) {
        const data = JSON.parse(e.data);
        console.log(data.message, "recieved")
        var html = '<div class="MessageWrapper">';
        if(data.userid == userId){
            html += '<div class="JusticeMessage MyMessage card text-white bg-primary mb-3">'
        }
        else{
            html += '<div class="JusticeMessage OthersMessage card bg-light mb-3">'
        }
        html += `<div class="card-body MessageCard">
                        <span><b>${data.username}&nbsp;&nbsp;&nbsp;</b></span>
                        <small class="MessageDate">${data.date}</small><br>
                        <span class="card-text">${data.message}</span>
                    </div>
                </div>
            </div>`
        $('#chat-log-'+convName+'-'+convId).append(html);
        $('#chat-log-'+convName+'-'+convId).animate(
            { scrollTop: $('#chat-log-'+convName+'-'+convId).height() }, 1000);
    };

    chatSocket.onclose = function(e) {
        console.error('Chat socket closed unexpectedly');
    };

    document.querySelector('#chat-message-input-'+convName+'-'+convId).focus();
    document.querySelector('#chat-message-input-'+convName+'-'+convId).onkeyup = function(e) {
        if (e.keyCode === 13) {  // enter, return
            document.querySelector('#chat-message-submit-'+convName+'-'+convId).click();
        }
    };

    document.querySelector('#chat-message-submit-'+convName+'-'+convId).onclick = function(e) {
        const messageInputDom = document.querySelector('#chat-message-input-'+convName+'-'+convId);
        const message = messageInputDom.value;
        console.log("Sending message... " + message + "  for conv: "+ convName)
        chatSocket.send(JSON.stringify({
            message:message,
            convName:convName,
            convId:convId,
            userName:userName,
            userId:userId
        }));
        messageInputDom.value = '';
    };
    $("#listconversations").on("click", function(e){
        chatSocket.close();
        delete chatSocket;
    });
}

function SetManageContacts(){
    $("#ManageContacts").on("click",function(){
        $.ajax({
            type: 'GET',
            url: '/Chat/ManageContacts',
            data: {csrfmiddlewaretoken: csrftoken},
            dataType: "json",
            success: function(res) {
                console.log("Success, ", res)
                $('#content').html(res.content);
                $(".DiscussWith").each(function(e){
                    $(this).on('click',function(e){
                        var contact = $(this).attr("name").split(":");
                        ContactSomeone(contact[1])
                    })
                });
                $(".BlockContact, .UnBlockContact").each(function(e){
                    $(this).on('click',function(e){
                        var contact = $(this).attr("name").split(":");
                        BlockUnblockContact(contact[1])
                    })
                })
                SetManageContacts();
            },
            error: function(rs, e) {
                console.log("error", rs)
                alert(rs.responseText);
            },
        });    
    });
}

function SetUpSearchConv(){
    var BackUpListConv = $('#listconversations').html()
    $("#SearchConvInput").keyup(function (e) {
        if ([13,38,40].includes(e.keyCode)){
            return
        }
        var input = $(this).val()
        if (input!=""){
            $.ajax({
                type: 'POST',
                url: '/Chat/search_conv',
                data: {'input': input, csrfmiddlewaretoken: csrftoken},
                dataType: "json",
                success: function(res) {
                    var r = eval(res.results);
                    if (r.length!=0){
                        var html = ""
                        for (i=0; i<r.length;i++){
                            html += `
                                <div id="itemConv${r[i].id}" class="list-group-item list-group-item-action flex-column align-items-start ConvItem"> <!-- active"> -->
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-2 h6">${r[i].name}</h5>
                                        <small>${r[i].date}</small>
                                    </div>
                                    ${r[i].message}
                                </div>
                            `
                        } 
                        $('#listconversations').html(html);
                    }
                    else{
                        var html = "Pas de conversation pour ce(s) mot(s) clé";
                        $('#listconversations').html(html);
                    }
                    ActiveConvItems();
                },
            });
        }
        else{
            $('#listconversations').html(BackUpListConv);
        }
        ActiveConvItems();
    });
}

function ActiveConvItems(){
    $(".ConvItem").on("click",function(e){
        idConv = $(this).attr("id").slice(8);
        DisplayConv(idConv);
    });
}

function SetUpConvNav(){
    SetManageContacts();
    ActiveConvItems();
    SetUpSearchConv();
}

function SetHomeChatPage(){
    SetAddConvForm();
    SetUpConvNav();
}

$(document).ready(function() {
    SetHomeChatPage();
});
