# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import template

register = template.Library()


@register.filter
def ConvName(conv, user):
    """ build the name of the conversation 'conv' depending on 
    if it has only 2 or more participants"""
    participants = conv.participants.all()
    if len(participants)==2:
        p = list(participants)
        p.remove(user)
        convName = p[0].username
    else:
        convName = conv.name
    return convName


@register.filter
def IsBlockedBy(c, u):
    """ 
    Tell is the contact c is bloced by user u 
    or not.

    :rtype: bool"""
    if c.is_blocked_by.filter(user=u).exists():
        return True
    else:
        return False