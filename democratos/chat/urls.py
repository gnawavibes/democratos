from django.urls import path
from . import search
from . import views

urlpatterns = [
    path('', views.homeChat, name='homeChat'),
    # path('index/', views.index, name='index'),     # Pour TESTER  OBSOLETE
    # path('<str:room_name>/', views.room, name='room'),  # Pour TESTER  OBSOLETE
    path('createNewConversation',
        views.createNewConversation,
        name='createNewConversation'),
    path('GetConversation',
        views.GetConversation,
        name='GetConversation'),
    path('ContactSomeone',
        views.ContactSomeone,
        name='ContactSomeone'),
    path('ManageContacts',
        views.GetManageContacts,
        name='GetManageContacts'),
    path('BlockUnblockContact',
        views.BlockUnblockContact,
        name='BlockUnblockContact'),
    path('search_conv',
        search.search_conv,
        name="search_conv"),
]

