from django.shortcuts import render
from django.http import Http404, JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.decorators.http import require_POST, require_GET
from render_block import render_block_to_string
from UserManager.models import CYL_user
from democratos.decorators import ajax_login_required
from chat.models import *
from django.utils import timezone
from democratos import log


def index(request):
    """OBSOLETE Create a chat room from a name """
    return render(request, 'MyChat.html')


def room(request, room_name):
    """OBSOLETE Display a room from its roomname """
    return render(request, 'room.html', {
        'room_name': room_name
    })


@login_required
def homeChat(request):
    user = request.user
    conversations = user.conversation_set.all().order_by("-date")
    return render(request, 'HomeChat.html', locals())


@ajax_login_required
@require_GET
def GetConversation(request):
    """ Return a conversation from its ID """
    user = request.user
    idConv = int(request.GET.get('idConv'))
    try:
        conv = Conversation.objects.get(id=idConv)
        assert conv is not None
    except:
        raise Http404
    messages = reversed(conv.message_set.all().order_by("-date")[0:6])
    participants = conv.participants.all()
    block = []
    for p in participants:
        if user.is_blocked_by.filter(user=p).exists():
            block.append(p.username + " vous a bloqué, discussion impossible")
        if user.has_blocked.filter(blocked=p).exists():
            block.append("Vous avez bloqué " + p.username +
                         ", discussion impossible")
    log.debug(("converstaion ", conv.id, "found!"))
    content = render_block_to_string(
        'room.html',
        "room",
        locals(),)
    ctx = {
        "content": content,
        "block": block,
        "conv_name": conv.name,
        "conv_id": str(idConv),
    }
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
def createNewConversation(request):
    """Create a conversation from an ajax post conswedishtaining
    a conversation name ("conName") and a list of 
    participant username (listParticipant)"""

    listParticipant = eval(request.POST.get('listParticipant', None))
    user = request.user
    convName = request.POST.get('convName', None)
    conv = Conversation.objects.create(
        name=convName,
        created=timezone.now(),
        date=timezone.now()
    )
    contacts = user.contacts.all()  # all users this user is following
    for username in listParticipant:
        participant = CYL_user.objects.get(username=username)
        if user.is_blocked_by.filter(user=participant).exists():
            conv.delete()
            msg = participant.username +\
                " vous a bloqué, vous ne pouvez le contacter"
            return JsonResponse({"block_error": msg})
        if user.has_blocked.filter(blocked=participant).exists():
            conv.delete()
            msg = " vous avez bloqué " +\
                participant.username +\
                ", vous ne pouvez le contacter"
            return JsonResponse({"block_error": msg})
        if not participant in contacts:
            user.contacts.add(participant)
            user.save()
        conv.participants.add(participant)
    conv.participants.add(user)
    conv.save()
    conversations = list(user.conversation_set.all().order_by("-date"))
    nav = render_block_to_string(
        'HomeChat.html',
        "nav",
        locals(),
    )
    ctx = {
        "nav": nav,
        "idConv": conv.id,
    }
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
def ContactSomeone(request):
    """Create a conversation from an ajax post conswedishtaining
    a conversation name ("conName") and a list of 
    participant username (listParticipant)"""
    try:
        contact_id = eval(request.POST.get('contact_id', None))
        contact = CYL_user.objects.get(id=int(contact_id))
    except:
        raise Http404
    user = request.user
    if user.has_blocked.filter(blocked=contact).exists():
        raise Http404
    if user.is_blocked_by.filter(user=contact).exists():
        msg = " vous a bloqué, vous ne pouvez le contacter"
        return JsonResponse({"block_error": contact.username + msg})
    if user.id > contact.id:
        NameConv = str(contact.id)+"AND"+str(user.id)
    else:
        NameConv = str(user.id)+"AND"+str(contact.id)
    conv, created = Conversation.objects.get_or_create(
        name=NameConv,
    )
    if created:
        conv.participants.add(contact)
        conv.participants.add(user)
        conv.created = timezone.now()
    conv.date = timezone.now()
    conv.save()
    conversations = list(user.conversation_set.all().order_by("-date"))
    nav = render_block_to_string(
        'HomeChat.html',
        "nav",
        locals(),
    )
    ctx = {
        "nav": nav,
        "idConv": conv.id,
    }
    return JsonResponse(ctx)


@ajax_login_required
@require_POST
def BlockUnblockContact(request):
    """Block or Unblock a contact from its id
    contained in the POST request"""
    try:
        contact_id = eval(request.POST.get('contact_id', None))
        c = CYL_user.objects.get(id=int(contact_id))
    except:
        raise Http404
    user = request.user
    block, created = BlockedContact.objects.get_or_create(
        user=user,
        blocked=c
    )
    if created:
        block.save()
        blocked = 'true'
    else:
        block.delete()
        blocked = 'false'
    html = render_block_to_string(
        'ContactsList.html',
        "TRblock",
        locals(),
    )
    ctx = {
        "html": html,
        "contact_id": c.id,
        "contact_name": c.username,
        "blocked": blocked,
    }
    return JsonResponse(ctx)


@ajax_login_required
@require_GET
def GetManageContacts(request):
    user = request.user
    contacts = user.contacts.all()
    content = render_block_to_string(
        'ContactsList.html',
        "contacts",
        locals(),)
    ctx = {
        "content": content,
    }
    return JsonResponse(ctx)
