from democratos.celery import app
from chat.models import Conversation, Message

@app.task
def SaveMessage(message, userid, convid, date):
	""" save message from a chat in DB """
	m = Message.objects.create(
		author_id = int(userid),
		conversation_id = int(convid),
		date = date,
		message = message,
	)
	m.save()
	c = Conversation.objects.get(id=convid)
	c.date = date
	c.save()