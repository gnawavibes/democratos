from django_elasticsearch_dsl import Document, fields
from elasticsearch_dsl import analyzer, Nested
from django_elasticsearch_dsl.registries import registry
from .models import Conversation, Message
from UserManager.models import CYL_user
from elasticsearch_dsl.analysis import token_filter

edge_ngram_completion_filter = token_filter(
    'edge_ngram_completion_filter',
    type="edge_ngram",
    min_gram=1,
    max_gram=20
)


edge_ngram_completion = analyzer(
    "edge_ngram_completion",
    tokenizer="standard",
    filter=["lowercase", edge_ngram_completion_filter]
)

@registry.register_document
class ConversationDocument(Document):
    name = fields.TextField(
        fields={
            'raw': fields.TextField(analyzer='standard'),
            'suggest': fields.CompletionField(),
            'edge_ngram_completion': fields.TextField(
                analyzer=edge_ngram_completion
            ),
        }
    )
    participants = fields.NestedField(
        properties={
            'username': fields.TextField(),
            'id': fields.IntegerField(),
        },
        include_in_root=True
    )

    class Index:
        # Name of the Elasticsearch index
        name = 'conversation'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = Conversation # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
            'date',
        ]
        related_models=[CYL_user]

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Car instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, Message):
            return related_instance.message

        # Ignore auto updating of Elasticsearch when a model is saved
        # or deleted:
        # ignore_signals = True

        # Don't perform an index refresh after every update (overrides global setting):
        # auto_refresh = False

        # Paginate the django queryset used to populate the index with the specified size
        # (by default it uses the database driver's default setting)
        # queryset_pagination = 5000

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Book instance(s) from the related model."""
        if isinstance(related_instance, CYL_user):
            return related_instance.conversation_set.all()

@registry.register_document
class MessageDocument(Document):
    message = fields.TextField(
        fields={
            'raw': fields.TextField(analyzer='standard'),
            'suggest': fields.CompletionField(),
            'edge_ngram_completion': fields.TextField(
                analyzer=edge_ngram_completion
            ),
        }
    )
    conversation = fields.NestedField(properties={
        'name': fields.TextField(),
        'id': fields.IntegerField(),
        'date': fields.DateField(index=False),
        'participants':fields.NestedField(properties={
            'username': fields.TextField(),
            'id': fields.IntegerField(),
        },include_in_root=True)
    }, include_in_root=True)

    class Index:
        # Name of the Elasticsearch index
        name = 'message'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = Message # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
        ]
        related_models=[Conversation]

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Book instance(s) from the related model."""
        if isinstance(related_instance, Conversation):
            return related_instance.message_set.all()