from django.db import models
from UserManager.models import CYL_user

class Conversation(models.Model):
    name = models.CharField(max_length=200)
    participants = models.ManyToManyField(
        CYL_user,
    )
    created = models.DateTimeField(null=True)
    date = models.DateTimeField(null=True)

    class Meta:
        ordering = ['date']
        verbose_name = "Conversation"
        verbose_name_plural = "Conversations"

    def __str__(self):
        pass

class Message(models.Model):
    author = models.ForeignKey(
        CYL_user, null=True, on_delete=models.CASCADE)
    conversation = models.ForeignKey(
        Conversation, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    message = models.TextField(null=False, blank=False)

    class Meta:
        ordering = ['date']
        verbose_name = "Conversation"
        verbose_name_plural = "Conversations"

    def __str__(self):
        return f"{self.author.username} - {self.date}\n{self.message}"

class BlockedContact(models.Model):
    user = models.ForeignKey(
        CYL_user,
        on_delete=models.CASCADE,
        related_name='has_blocked'
    )
    blocked = models.ForeignKey(
        CYL_user,
        on_delete=models.CASCADE,
        related_name='is_blocked_by'
    )

class UnreadMessageNote(models.Model):
    user = models.ForeignKey(
        CYL_user, null=True, on_delete=models.CASCADE)
    conversation = models.ForeignKey(
        Conversation, on_delete=models.CASCADE)
    nb_unread = models.IntegerField()

    def save(self, *args, **kwargs):
        # this will take care of the saving
        if self.nb_unread==0:
            self.delete()
        else:
            super(UnreadMessageNote, self).save(*args, **kwargs)