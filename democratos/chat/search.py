from elasticsearch_dsl.query import Q, MultiMatch, SF
from chat.documents import ConversationDocument, MessageDocument
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from django.http import JsonResponse
from django.views.decorators.http import require_POST, require_GET
from democratos import log
from democratos.decorators import ajax_login_required
import sys
if not 'pytest' in sys.argv[0]:
    from democratos.local_settings import ElasticHost
else:
    ElasticHost = ["127.0.0.1:9201",
    "http://elasticsearch_test:9201","localhost:9201","elasticsearch:9200"]
from datetime import datetime

client = Elasticsearch(ElasticHost)

@ajax_login_required
@require_POST
def search_conv(request):
    input_val = request.POST.get('input', None)
    s_user = Search(using=client)
    user = request.user
    # conversation Name search
    q_conv = s_user.index(
        "conversation"
    ).filter(
        "nested",
        path="participants",
        query=Q("match", participants__id=user.id)
    ).query(
        'regexp',
        name='.*'+input_val+'.*',
    )
    r_conv = q_conv.execute()
    # participant Search
    q_part = s_user.index(
        "conversation"
    ).filter(
        "nested",
        path="participants",
        query=Q(
            "match",
            participants__id=user.id
        )
    ).filter(
        "nested",
        path="participants",
        query=Q(
            'regexp',
            participants__username='.*'+input_val+'.*'
        )
    )
    r_part = q_part.execute()
    # message containing input search
    q_msg = s_user.index(
        "message"
    ).filter(
        "nested",
        path="conversation.participants",
        query=Q("match", conversation__participants__id=user.id),
    ).query(Q(
            'wildcard',
            message='*'+input_val+'*',
        )| Q(
            'match_phrase',
            message=input_val,
        )
    )
    r_msg = q_msg.execute()
    res = []
    for h in r_conv.hits:
        res.append({
            "name": h.name,
            "id": h.id,
            "date": datetime.fromisoformat(
                h.date).strftime("%d/%m/%Y, %H:%M:%S"),
            "message": "",
        })
    for h in r_part.hits:
        name = h.name
        if len(h.participants) == 2:
            p_list = list(h.participants)
            p_list.remove([el for el in p_list if el['id'] == user.id][0])
            name = p_list[0].username
        for el in res:
            if el['id'] == h.id:
                res.remove(el)
        res.append({
            "name": name,
            "id": h.id,
            "date": datetime.fromisoformat(
                h.date).strftime("%d/%m/%Y, %H:%M:%S"),
            "message": "",
        })
    for h in r_msg.hits:
        name = h.conversation.name
        if len(h.conversation.participants) == 2:
            p_list = list(h.conversation.participants)
            p_list.remove([el for el in p_list if el['id'] == user.id][0])
            name = p_list[0].username
        for el in res:
            if el['id'] == h.conversation.id:
                res.remove(el)
        res.append({
            "name": name,
            "id": h.conversation.id,
            "date": datetime.fromisoformat(
                h.conversation.date).strftime("%d/%m/%Y, %H:%M:%S"),
            "message": h.message,
        })
    ctx = {"results": str(res)}
    return JsonResponse(ctx)
