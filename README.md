# democratos

The goal of this Django project is to create an opensource platform on which people can debate and vote about the french laws.
This is still an ongoing project, at the developpment phase.
Apps ready:

* dl_codes: download and stack the french law codes from legifrance - NEED FIX
* CreateYourLaws: allow to vote, create new laws, navigate through codes, debate...
* Tree Nav: use to navigate through law codes and laws.
* Commit: used to trace laws and opinions evolution
* CommonsJustice: App used to create virtual court of users to judge abusive behavior from other users.
* Chat: asynchronous messenger between user


Ongoing Apps:

* Add a search engine (Elasticsearch instance)



Ongoing tasks:

* See if more Tasks could worth being Celery taskable
* fix bugs


And later:

* Add a Forum
* Add a budget vote system

Before doing anything, run "python democratos/dl_codes/DownloadCodes.py" in order to dwl the french laws and create the "government" user.
Then you can create your superuser, add user...


